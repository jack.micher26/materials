# Zoom

In this activity, we will become familiar with Zoom in preparation for future activities.

## Preflight Checklist

This is my preflight checklist.

- [ ] Appropriately dressed

- [ ] Lighting is good (in front of your face, not behind)

- [ ] Distraction free environment

- [ ] Physical background is non-distracting

- [ ] Computer background is non-distracting

- [ ] Headset

- [ ] Beverage

- [ ] Bodily functions have been attended to

- [ ] Notify others you're in a meeting for given duration

- [ ] Animals and small children cared for

- [ ] Paper and pen

- [ ] Other appropriate materials

- [ ] Power

- [ ] Internet

- [ ] Comfortable seat

## What's possible?

All of these may not be possible. Try them, test them, and figure out which
are possible and which are not.

1. Mute/unmute your microphone (audio).

2. Mute/unmute your camera (video).

3. Mute/unmute others.

4. Set/unset a virtual background.

5. Share/unshare your screen.

6. Give/revoke control of mouse and/or keyboard to another.

7. Annotate your screen.

8. Annotate another's screen.

9. View list of participants.

10. Chat

  * Group
  * Private

11. Breakout rooms

  * Creating
  * Entering
  * Sharing
  * Chat
  * Exiting

12. Share whiteboard

  * Draw on it
  * Have others draw on it
  * How many different whiteboard's are there?

13. Use reactions

14. Record (and later post, and later playback).

15. Rearrange thumbnails.

16. Change focus from one person to another, making them large

## How can we best use zoom in team activities?


## Other tips, suggestions, ideas?


---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
