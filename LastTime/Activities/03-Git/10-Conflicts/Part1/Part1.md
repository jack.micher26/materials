# Conflicts Part 1: No Conflicts

1. Part1Project.zip contains a git repository. Unzip it.

2. Use `git branch` to list the branches in this repository.
    * What branches exist?
    * Which branch is the active branch (i.e., which branch is checked out into the working tree? i.e., which branch are you on?)?

3. Use `ls` and `cd` to browse around figure out what files and directories exist. List them here.

4. Use `git log --all --graph` to explore the structure of commits. Summarize your understanding of the graph structure below. Consider drawing a diagram for yourself.

5. What is the ID of the most recent commit that both branches have in common?

6. Use `git diff COMMIT_ID` to view the changes on the current branch since the most recent commit that the branches have in common. Describe the change.

7. Use `git checkout` to checkout the other branch. Use `ls` and `cd` to explore the files that are in that branch, and use `git diff` to view the changes on the other branch since the most recent commit that both branches have in common. Summarize your results here.

8. Use `git diff BRANCH`, where branch is the name of the branch you are not on, to view the difference between the two branches. Summerize the differences.

9. You have decided to merge the non-master branch into `master` such that `master` has the combined results of the two branches (the other branch will remain unchanged). Assuming you do not know which branch you are currently on, write a command sequence  that will merge the non-master branch into `master`.

10. Predict what will happen when you execute your sequence.

    * How will the graph and its labels (branches, HEAD, tags, etc.) change?
    * How will the content of the files change?

11. Execute your sequence and inspect the project again.

12. Did your merge sequence work as expected? How do you know?

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
