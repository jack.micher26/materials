# Part 3: Resolving Lexical Conflicts

1. Part3Project.zip contains a git repository. Unzip it. Inspect the repository as you did in Part1. Summarize your findings below. Predict what you expect to happen when you merge.

2. In Part3Project, merge the non-master branch into master, but **abort** the merge.
   (Just want you to know how to abort a merge.) What command did you use to abort the merge? Inspect your repository, summarize your findings.

3. In Part3Project, merge the non-master branch into master, resolving any conflicts. Inspect your repository. Summarize your findings.

## Practice: Create your own conflict

1. Create a new branch, and make some commits to that branch.
2. Go back to master, and make some commits---some of which that will conflict.
3. Merge the new branch into master and resolve the conflicts.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
