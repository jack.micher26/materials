# Git Graph Structure and Branching

The commit structure of git is represented by a directed acyclic graph.
That graph structure allows non-linear commit structures used for
branches used in software development workflows.

## Content Learning Objectives

*After completing this activity, students should be able to:*

* Identify features of graphs.
* Identify correspondences between git command outputs and the
  underlying directed acyclic graph structure.
* Explain what HEAD, branch names (like master), and tag names
  represent.
* Explain how branching and merging works in git.

## Process Skill Goals

*During the activity, students should make progress toward:*

* Carefully reading Git command messages for understanding.
    (Information Processing)

## Team Roles

*Decide what role each of you will play for today. Choose a role that
you have not played before, or recently. The goal should be to have all
team members rotate through the roles on a regular basis to become
comfortable with all the roles. If you have only three people, one
should have two roles. If you have five people, two may share the same
role. Record role assignments here.*

| Role        | Name |
| ----------- | ---- |
| Manager     |      |
| Presenter   |      |
| Recorder    |      |
| Reflector   |      |

## Model 1: Graphs (4 min)

Graph A

```mermaid
graph BT
    1 --- 2 --- 3 --- 4
    1(( ))
    2(( ))
    3(( ))
    4(( ))
```

Graph B

```mermaid
graph BT
    1 --> 2 --> 3 --> 4
          2 --> 5 --> 4
    1(( ))
    2(( ))
    3(( ))
    4(( ))
    5(( ))
```

Graph C

```mermaid
graph BT
    1 --> 2 --> 3 --> 4
          2 --> 5 --> 4
                5 --> 2
    1(( ))
    2(( ))
    3(( ))
    4(( ))
    5(( ))
```

Graph D

```mermaid
graph BT
    1 --> 2 --> 3 --> 4 --> 5
          2 --> 5
    1(( ))
    2(( ))
    3(( ))
    4(( ))
    5(( ))
```

Graph E

```mermaid
graph BT
    1 --> 2 --> 3 --> 4
          4 --> 5 --> 2
    1(( ))
    2(( ))
    3(( ))
    4(( ))
    5(( ))
```

Graph F

```mermaid
graph BT
    1 --- 2 --- 3 --- 4
          2 --- 5 --- 4
    1(( ))
    2(( ))
    3(( ))
    4(( ))
    5(( ))
```

Graph G

```mermaid
graph BT
    1 --> 2 --> 3 --> 4
          2 --> 5 --> 6
                5 --> 7
    1(( ))
    2(( ))
    3(( ))
    4(( ))
    5(( ))
    6(( ))
    7(( ))
```

### Model 1 Questions

1. Complete the table below

    | Graph | # Nodes | # Edges | Directed? | Acyclic? |
    | ----- | ------- | ------- | --------- | -------- |
    | A     |         |         |           |          |
    | B     |         |         |           |          |
    | C     |         |         |           |          |
    | D     |         |         |           |          |
    | E     |         |         |           |          |
    | F     |         |         |           |          |
    | G     |         |         |           |          |

2. What does it mean for a graph to be acyclic?

3. What does it mean for a graph to be directed?

## Model 2: Git Commits Graph Structure (5 min)

Git commits are represented as a *directed acyclic graph (DAG)*.

In git, each commit contains the commit number (commit id) to its parent commit. The first commit does not have a parent.

Consider the following log and corresponding commit diagram.

```bash
$ git log
commit 685f4abfccda64d68145bae6a6123bf1e8a88021 (HEAD -> master)
Author: Karl R. Wurst <karl@w-sts.com>
Date: Fri Jan 18 16:19:36 2019 -0500

    Add Feature Z.

commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4
Author: Karl R. Wurst <karl@w-sts.com>
Date: Fri Jan 18 16:02:12 2019 -0500

    Add Feature Y.

commit db967767f9ab675dba21e08c5d20627b34cbe133
Author: Karl R. Wurst <karl@w-sts.com>
Date: Fri Jan 18 14:25:44 2019 -0500

    Add Feature X.
```

```mermaid
graph BT
    1 --> 2 --> 3
    1((q))
    2((r))
    3((s))
```

### Model 2 Questions

1. Complete the table by writing the Node name that corresponds to each commit such tha the diagram correctly reflects the relationships between the commits.

    | Commit ID | Commit Message | Node |
    | --- | --- | --- |
    | 685f4abfccda64d68145bae6a6123bf1e8a88021 | Add Feature Z. |  |
    | 63b66cf74632ab3f2ebabb333e71d13938d7f2d4 | Add Feature Y. |  |
    | db967767f9ab675dba21e08c5d20627b34cbe133 | Add Feature X. |  |

2. Why did you choose that ordering?

3. What do the edges represent?

4. Why must the edges be directed?

5. Which node should also be labeled with `HEAD` and `master`?

## Model 3: Starting with Version 1.0

You are developing a piece of software. The software has been released as Version 1.0. A ***release*** is a version of code that has been shipped to clients, or has been marked for use by clients.

Let's see the most recent commits in our repository.

```bash
$ git log
commit 946e142c5638795aa2fd5b9555692ac82dea378c (HEAD -> master, tag: v1.0)
Author: Dorothy Dev <dot@awesomesoft.com>
Date:   Fri Jan 18 16:19:36 2019 -0500

    Add Feature W.
```

Notice this commit has a tag of `v1.0`. This indicates to others that this commit was released as Version 1.0. This tag was added using the following command:

```bash
git tag -a v1.0 -m "Version 1.0 for release"
```

Let's take a closer look at the most recent commit.

```bash
$ git show v1.0
tag v1.0
Tagger: Robert Release <rob@awesomesoft.com>
Date:   Fri Jan 18 17:29:36 2019 -0500

    Version 1.0 for release

commit 946e142c5638795aa2fd5b9555692ac82dea378c (HEAD -> master, tag: v1.0)
Author: Dorothy Dev <dot@awesomesoft.com>
Date:   Fri Jan 18 16:19:36 2019 -0500

    Add Feature W.
```

### Model 3 Questions

1. How did we tell `git show` what commit to show?

2. What other ways do you think can we specify a particular commit to git?

3. Who authored the commit?

4. Who tagged the commit using this command?

5. How did git know what commit to tag?

6. What is the purpose of `git show`?

## Model 4: Work on Version 2.0

Now your team is developing Version 2. Version 2 will have three new features, X, Y, and Z. The new version cannot be released until ***all three*** features are complete.

Your team has completed Feature X and Feature Y, and committed them to your Git repository.

```bash
$ git log
commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4 (HEAD -> master)
Author: Colin Coder <colin@awesomesoft.com>
Date:   Fri Jan 18 17:02:12 2019 -0500

    Add Feature Y.

commit db967767f9ab675dba21e08c5d20627b34cbe133
Author: Dorothy Dev <dot@awesomesoft.com>
Date:   Fri Jan 18 16:25:44 2019 -0500

    Add Feature X.

commit 946e142c5638795aa2fd5b9555692ac82dea378c (tag: v1.0)
Author: Dorothy Dev <dot@awesomesoft.com>
Date:   Fri Jan 18 16:19:36 2019 -0500

    Add Feature W.
```

### Model 4 Questions

1. Which of the following has stayed on the same commit (mark with "same") and which has moved (marke as "moved")?

    * HEAD
    * master
    * v1.0
    * 946e142

2. What does this tell you about the difference between tags and branch names (e.g., `master`), and the relationship between a tag and a commit?

## Model 5: Bug Report Against Version 1

Before Feature Z can be completed a critical bug is reported against version 1.0. The bug must be fixed and Version 1.1 released immediately. All work is stopped on Feature Z, until the bug is fixed and Version 1.1 released.

Once Version 1.1 is released, work can continue on Feature Z. Feature Z will be committed. And then Release 2.0 can be committed incorporating all of the new features.

### Model 5 Questions

1. Assuming Version 1.1 is committed next and Freture Z is committed next and released as Version 2.0, label nodes in the graph below with the commit messages and version tags.

    ```mermaid
    graph BT
        5 --> 4 --> 3 --> 2 --> 1
        1((1))
        2((2))
        3((3))
        4((4))
        5((5))
    ```

    | Node | Message | Tag |
    | --- | --- | --- |
    | 1 |   |   |
    | 2 |   |   |
    | 3 |   |   |
    | 4 |   |   |
    | 5 |   |   |

2. What commits will release Version 1.1 contain?

3. What problem will that cause? (Hint: review Model 4.)

4. To what commit *should* we apply the bug fix?

5. Ideally, if you could rewrite history, what would the commits come in?

    | Node | Message | Tag |
    | --- | --- | --- |
    | 1 |   |   |
    | 2 |   |   |
    | 3 |   |   |
    | 4 |   |   |
    | 5 |   |   |

## Model 6: Let's Start Over: Version 1.0

You are developing a piece of software. The software has been released as Version 1.0.

```bash
$ git log
commit 946e142c5638795aa2fd5b9555692ac82dea378c (HEAD -> master, tag: v1.0)
Author: Dorothy Dev <dot@awesomesoft.com>
Date: Fri Jan 18 16:19:36 2019 -0500

    Add Feature W.

$ git show v1.0
tag v1.0
Tagger: Robert Release <rob@awesomesoft.com>
Date:   Fri Jan 18 17:29:36 2019 -0500

    Version 1.0 for release

commit 946e142c5638795aa2fd5b9555692ac82dea378c (HEAD -> master,
tag: v1.0)
Author: Dorothy Dev <dot@awesomesoft.com>
Date:   Fri Jan 18 16:19:36 2019 -0500

    Add Feature W.
```

```mermaid
graph BT
    HEAD --> master
    master --> 2
    v1.0 --> 2
    2 --> 1
    2((946e14))
    1((...))
```

Your team is currently developing Version 2. Version 2 will have three new features, X, Y, and Z. The new version cannot be released until all three features are complete.

Your team has completed Feature X and Feature Y, and committed them to your Git repository.

```bash
$ git log
commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4 (HEAD -> master)
Author: Colin Coder <colin@awesomesoft.com>
Date:   Fri Jan 18 17:02:12 2019 -0500

    Add Feature Y.

commit db967767f9ab675dba21e08c5d20627b34cbe133
Author: Dorothy Dev <dot@awesomesoft.com>
Date:   Fri Jan 18 16:25:44 2019 -0500

    Add Feature X.

commit 946e142c5638795aa2fd5b9555692ac82dea378c (tag: v1.0)
Author: Dorothy Dev <dot@awesomesoft.com>
Date:   Fri Jan 18 16:19:36 2019 -0500

    Add Feature W.
```

```mermaid
graph BT
    HEAD --> master
    master --> 4
    v1.0 --> 2
    4 --> 3 --> 2 --> 1
    4((63b66c))
    3((db9677))
    2((946e14))
    1((...))
```

## Model 7: Bug Report Against Version 1

Before Feature Z can be completed a critical bug is reported. The bug must be fixed and Version 1.1 released immediately. All work is stopped on Feature Z, until the bug is fixed and Version 1.1 released.

The bug was reported as Issue #1.

### Model 7 Questions

1. Based on the current DAG, to which commit do we want to apply the bug fix?

## Model 8: Fixing the Bug: Creating a Branch

Since we want to apply the bug fix to Version 1.0, rather than the current `HEAD`, we will start a branch. We will name the branch `issue-1`.

```bash
1 |  $ git branch
  |  * master
  |
2 |  $ git branch issue-1 946e14
  |
3 |  $ git branch
  |  issue-1
  |  * master
  |
4 |  $ git checkout issue-1
  |
5 |  $ git branch
  |  * issue-1
  |  master
```

DAG 1

```mermaid
graph BT
    HEAD --> master
    master --> 4
    v1.0 --> 2
    4 --> 3 --> 2 --> 1
    4((63b66c))
    3((db9677))
    2((946e14))
    1((...))
```

DAG 2

```mermaid
graph BT
    HEAD --> master
    master --> 4
    v1.0 --> 2
    issue-1 --> 2
    4 --> 3 --> 2 --> 1
    4((63b66c))
    3((db9677))
    2((946e14))
    1((...))
```

DAG 3

```mermaid
graph BT
    HEAD --> issue-1
    master --> 4
    v1.0 --> 2
    issue-1 --> 2
    4 --> 3 --> 2 --> 1
    4((63b66c))
    3((db9677))
    2((946e14))
    1((...))
```

### Model 8 Questions

There are three DAGs above, representing different states of the git
repository during the commands.

1. After which command does DAG 1 represent our repository?

2. After which command does DAG 2 represent our repository?

3. After which command does DAG 3 represent our repository?

4. In terms of our scenario, what does `issue-1` represent?

5. In terms of our scenario, what does `master` represent?

6. In the output of the `get branch` commands, there is an asterisk (`*`). What cooresponds to this asterisk in the diagrams?

7. What does `HEAD` represent?

8. What new git commands where introduced, and what do they do?

## Model 7: Fixing the Bug: Committing the Fix

Once the bug fix is committed by Dorothy Dev, Robert Release tags it a
v1.1 for release.

```bash
1 |  $ git branch
  |  * issue-1
  |  master
  |
2 |  $ git commit -m "Fix Issue #1"
  |  [issue-1 95cda96] Fix Issue #1
  |  1 file changed, 1 insertion(+), 1 deletion(-)
  |
3 |  $ git log
  |  commit 95cda96507fcc41390a5e172e2e0ac1ce46ab38c (HEAD -> issue-1)
  |  Author: Dorothy Dev <dot@awesomesoft.com>
  |  Date:   Sat Jan 19 9:45:02 2019 -0500
  |
  |      Fix Issue #1
  |
  |  commit 946e142c5638795aa2fd5b9555692ac82dea378c (tag: v1.0)
  |  Author: Dorothy Dev <dot@awesomesoft.com>
  |  Date:   Fri Jan 18 16:19:36 2019 -0500
  |
  |      Add Feature W.
  |
4 |  $ git tag -a v1.1 -m "Version 1.1 for release"
  |
5 |  $ git show v1.0
  |  tag v1.1
  |  Tagger: Robert Release <rob@awesomesoft.com>
  |  Date:   Sat Jan 19 10:29:36 2019 -0500
  |
  |      Version 1.1 for release
  |
  |  commit 95cda96507fcc41390a5e172e2e0ac1ce46ab38c (HEAD -> issue-1)
  |  Author: Dorothy Dev <dot@awesomesoft.com>
  |  Date:   Sat Jan 19 9:45:02 2019 -0500
  |
  |     Fix Issue #1
```

```mermaid
graph BT
    HEAD --> issue-1
    master --> 4
    v1.0 --> 2
    issue-1 --> 5
    5 --> 2
    v1.1 --> 5
    4 --> 3 --> 2 --> 1
    5((95cda9))
    4((63b66c))
    3((db9677))
    2((946e14))
    1((...))
```

### Model 7 Questions (2 min)

1. Why do you think Dorothy Dev issued command #1?

2. Why does commit `95cda96` point to `94e14m` (rather than `63b66cf`)?

3. Why doesn't command 3 show commits `db96776` and `63b66cf`?

4. What is the relationship of `HEAD` and a commit?

## Model 8: Merging the Bug Fix Into Version 2

We are ready to get back to work on Version 2. The last thing to do is add Feature Z.

But, we should be sure to make sure the bug fix code gets into the code before we release Version 2.

We have two choices:

* Merge in the bug fix ***before*** adding Feature Z.

* Merge in the bug fix ***after*** adding Feature Z.

Which one we choose will depend on the impact of the bug fix on the new code.

Let's assume that it's a better idea to merge in the bug fix right away, before adding Feature Z.

```bash
1 | $ git branch
  | * issue-1
  | master
  |
2 | $ git checkout master
  | Switched to branch 'master'
  |
3 | $ git branch
  | issue-1
  | * master
  |
4 | $ git merge issue-1
  | Merge made by the 'recursive' strategy.
  |  a.java | 2 +-
  |  1 file changed, 1 insertion(+), 1 deletion(-)
  |
5 | $ git log
  | commit d05bd5c8760d8c5af94ccb690e9f0d6950bd5ff8 (HEAD -> master)
  | Merge: 95cda96 63b66c
  | Author: Colin Coder <colin@awesomesoft.com>
  | Date:   Mon Jan 21 9:18:30 2019 -0500
  |
  |     Merge branch 'issue-1'
  |
  | commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4
  | Author: Colin Coder <colin@awesomesoft.com>
  | Date:   Fri Jan 18 17:02:12 2019 -0500
  |
  |     Add Feature Y.
  |
  | commit db967767f9ab675dba21e08c5d20627b34cbe133
  | Author: Dorothy Dev <dot@awesomesoft.com>
  | Date: Fri Jan 18 16:25:44 2019 -0500
  |
  |     Add Feature X.
  |
  | commit 946e142c5638795aa2fd5b9555692ac82dea378c (tag: v1.0)
  | Author: Dorothy Dev
  | <dot@awesomesoft.com>
  | Date: Fri Jan 18 16:19:36 2019 -0500
  |
  |     Add Feature W.
```

```mermaid
graph BT
    HEAD --> master
    master --> 6
    v1.0 --> 2
    issue-1 --> 5
    5 --> 2
    v1.1 --> 5
    6 --> 4 --> 3 --> 2 --> 1
    6 --> 5
    6((d05bd5))
    5((95cda9))
    4((63b66c))
    3((db9677))
    2((946e14))
    1((...))
```

### Model 8 Questions (8 min)

1. What is the result of issuing command #2?

2. Why did Colin Coder issue command #2?

3. Which command created commit `d05bd5c`?

4. Where are the two edges coming out of the node `d05bd5c` represented in the log?

5. How would the resulting graph be different if Colin had not issued command 2, and had issued `git merge master` instead of `git merge issue-1`? (Assume that the newly created node would still be `d05bd5c`.)

    * Where would the edges coming out of node `d05bd5c` point? Why?

    * Which node would `HEAD` point to? Why?

    * Which node would `master` point to? Why?

    * Which node would `issue-1` point to? Why?

    * Which node would `v1.1` point to? Why?

    * Why would this have been a bad choice? Explain how this would be different than what we wanted to do.

---
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
