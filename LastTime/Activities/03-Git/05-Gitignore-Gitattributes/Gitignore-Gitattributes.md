# .gitignore and .gitattributes


## Learning Outcomes

By the end of this activity, participants will be able to...

- Explain the purpose of .gitignore.
- Use .gitignore to ignore different groups of files by extension.
- Justify types of files that should and should not be stored in a repository.
- Explain the problem of line-endings and how .gitattributes can help.


## Process Skills Practiced

- Formulating and testing hypotheses.


## Team Roles


| Role      | Name | Responsibility |
| --------- | ---- | -------------- |
| Manager   |      | Manage time. Ensure everyone has a voice. Help solve problems. |
| Recorder  |      | Write answers on behalf of the team. Help solve problems. |
| Presenter and Reflector |      | Speaks for the team. Helps solve problem. Identify what the team is doing well, and suggest how it might improve |


## Model 1: Project A

```
$ ls -a
.       ..      .git    A.class A.java
$ git status
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	A.class
	A.java

nothing added to commit but untracked files present (use "git add" to track)

```

## Model 2: Project B

```
$ ls -a
.          ..         .git       .gitignore A.class    A.java
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	A.java

nothing added to commit but untracked files present (use "git add" to track)
$ cat .gitignore
*.class

```

## Questions: 15 minutes

1. How many files are in Project A?

```




```

2. How many files are in Project B?

```




```

3. What file does Project B have that Project A does not?

```




```

4. What is the contents of that file?

```




```

5. In Project A, how many files are "untracked" by git?

```




```

6. In Project B, how many files are "untracked" by git?

```




```

7. In Project B, which file is being ignored by git?

```




```

8. Why do you think git in Project B is ignoring those files?

```




```

9. What do you need to do to make git ignore that same file in Project B?

```




```

10. Perform that operation and observe the results. Do they confirm or reject your hypothesis?

```




```

11. Suppose you want git to ignore JAR files (files that end in .jar) as well as class files.
    How do you think you would do that?

```




```

12. Confirm your hypothesis.

```




```

13. What kinds of files do you think should and should not be stored in the repository?

```




```

## Model 3

Visit https://github.com/github/gitignore .

### Questions

1. Find and view a gitignore file for a Python-based project. List a few of the files that will be ignored if you adopted this file for a project. Explain why they should be ignored.


2. Find and view a gitignore file for a Java-based project. List a few of the files that will be ignored if you adopted this file for a project. Explain why they should be ignored.


3. Your team will be starting a new Java project. Will you write your own gitignore file, or will you start with one in the above repository? Give the rationale for your decisions.

## Model 4

Different operating systems different character(s) for lind endings for text files.

1. What line ending character is used by the following operating systems
    * Windows?
    * Linux?
    * MacOS?


2. Consider a team of developers working on the same project using different operating systems. What problems might this cause with version control?


3. It's possible to configure your local git to handle line-endings properly so that the above problem doesn't occur. What's the problem with relying on each developer to configure git properly to handle line endings?


4. Skim https://help.github.com/en/github/using-git/configuring-git-to-handle-line-endings . How can .gitattribute help address the line-endings problem, and the developer-git-configuration problem identified in the previous question?


5. Where do you place a .gitattributes file?


6. .gitattributes can be complicated to write, how does the following site help? https://github.com/alexkaratarakis/gitattributes .



---
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
