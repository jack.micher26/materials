# Branching

In this activity you will learn to minualate git's commit graph without worrying about the contents of commits.

## Team Roles

| Role | Name | Purpose |
| ---- | ---- | ------- |
| Pilot |     | Manipulate the system under study on the team's behalf. |
| Co-Pilot |  | Read instructions, record answers, help oritient the pilot. |

If you have a 3-person team, the additional person is the spokesperson and manager.

## Objectives

Upon completion of this activity you will be able to:

* Use `git branch` to create a new branch.
* Use `git checkout` to control HEAD.
* Use `git merge` to merge one branch into another.
* Use `git rebase` to rebase commits onto another branch.
* Explain why `rebase` may be disruptive and when to avoid it.
* Explain the relationship between HEAD and `git commit`.
* Use commit hash values to refer to commits in git commands.
* Use relative references (`^` and `~`) to refer to commits in git commands.
* Use `git reset` to undo a commit on a branch.
* Use `git revert` to create a new commit that undoes a commit on a branch.
* Explain the difference between `git reset` and `git revert`.
* Identify when one should prefer `git reset` over `git revert` and vice-versa.

## Model 1 (10 min)

* Navigate to <http://learngitbranching.js.org/> .
* Read and complete levels ***1 and 2*** of the `Introduction Sequence` in `Main`.

### Model 1 Questions

1. In terms of the graph, explain in detail what happens when you execute `git commit`.

2. What is the result of the following command sequence? Do you think this is what the user intends?

    ```bash
    1 |  git branch issue-1
    2 |  edit
    3 |  git add .
    4 |  git commit -m "issue-1: Write test"
    ```

3. What command could the user add to the sequence above to get their desired results? Between which two instructions should it be inserted? List all possible solutions.

4. Git a single command that creates and checks out a new branch named `issue-2`.

5. How many commits are created or destroyed when you create a new branch?

## Model 2 (5 min)

* Navigate to <http://learngitbranching.js.org/> .
* Read and complete levels ***3*** of the `Introduction Sequence` in `Main`.

### Model 2 Questions

1. True or false; git merge always creates a new commit. Explain.

2. Assuming master is checked out, which branch is ***NOT*** changed by the following command?

    ```bash
    git merge issue-4
    ```

## Model 3 (10 min)

* Navigate to <http://learngitbranching.js.org/> .
* Read and complete levels ***4*** of the `Introduction Sequence` in `Main`.

> WARNING: Rebasing is a disruptive operation. Never rebase a branch that has been published to a remote repository.

### Model 3 Questions

1. Assuming master is checked out, which branch is ***NOT*** changed by the following command?

    ```bash
    git rebase issue-4
    ```

2. Is your answer the same or different from your answer from question 2 in Model 2? 

3. Form a rule based on your previous observation.

4. When commits are copied over using rebase, will the new nodes have the same hash values as the old? Explain.

5. Based on your previous answer, why do you think you should never rebase a published branch?

6. When are the following commands equivalent?

    ```bash
    git rebase bugFix
    ```

    and

    ```bash
    git merge bugFix
    ```

## Model 4 (5 min)

* Navigate to <http://learngitbranching.js.org/> .
* Read and complete levels ***1*** of `Ramping Up` in `Main`.

### Model 4 Questions

1. What kind of thing does HEAD point to in git?

2. What kind of thing does HEAD point to if it has been detached?

3. Explain how you can detach HEAD? Give an example of a command to detach HEAD; you may make up values.

4. How do you attach HEAD to a branch? Give an example. Make up any values you need.

## Model 5 (5 min)

* Navigate to <http://learngitbranching.js.org/> .
* Read and complete levels ***2*** of `Ramping Up` in `Main`.

### Model 5 Questions

1. Write a reference to the commit that is the parent of master.

2. Write a reference to the parent of the current commit.

## Model 6 (10 min)

* Navigate to <http://learngitbranching.js.org/> .
* Read and complete levels ***3*** of `Ramping Up` in `Main`.

### Model 6 Questions

1. Write a reference to the commit that is 3 above master.

2. Write a command that would move master to the commit 2 above the current commit.

3. In your haste to implement a new idea, you forgot to create a new branch to hold your work. You have made three commits to master. Give a command to restore master to its position before you began your work.

4. Now give a command to create a new branch `issue-5` that contains your new work. Assuming you did not move HEAD in your previous command, HEAD points to your most recent commit containing your new work.

5. Do you think your previous two commands may be disruptive like `rebase` is? Explain.

## Model 7 (10 min)

* Navigate to <http://learngitbranching.js.org/>.
* Read and complete levels ***4*** of `Ramping Up` in `Main`.

### Model 7 Questions

1. Explain what `reset` does.

2. Explain what `revert` does.

3. Why might `reset` be disruptive like `rebase`?

4. Why is `revert` ***NOT*** disruptive like `rebase`?

5. When should you prefer `revert` over `reset`?

## Model 8 (3 min)

* Navigate to <http://learngitbranching.js.org/> .
* Read and complete levels ***!!!4!!!*** of `A Mixed Bag` in `Main`. HINT: type `levels` to get back to the main menu.

### Model 8 Questions

1. Write a command to tag the commit before the current commit with `v1.0`.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
