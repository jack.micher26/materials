# Git Remotes

## Model 1: Remote basics (10 min)

### Project: t

```bash
❯ mkdir t
❯ cd t
❯ git init .
giInitialized empty Git repository in /Users/hjackson/Desktop/t/.git/
❯ touch README.md
❯ git add .
❯ git commit -m "Add README.md"
[master (root-commit) 53a300e] Add README.md
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md
❯ git log --all --oneline --graph
* 53a300e (HEAD -> master) Add README.md
❯ git remote -v
❯ git branch -a
* master
❯
```

### Project: s

```bash
❯ git clone https://gitlab.com/StoneyJackson/s.git
Cloning into 's'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 201 bytes | 100.00 KiB/s, done.
❯ cd s
❯ git log --all --oneline --graph
* 3bed730 (HEAD -> master, origin/master, origin/HEAD) Initial commit
❯ git remote -v
origin	https://gitlab.com/StoneyJackson/s.git (fetch)
origin	https://gitlab.com/StoneyJackson/s.git (push)
❯ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
❯
❯
```

### Model 1 Questions

1. Where did project t come from?

2. Where did project s come from?

3. What remotes does t have?

4. What remotes does s have?

5. What branches does t have?

6. What branches does s have?

7. What is a remote?

8. What command automatically creates a remote?

9. What's the name of the remote automatically created?

10. What is the meaning of the remote automatically created?

11. What extra branches does s have that t does not?

12. Why do you think they exist?

## Model 2: Clone, Remote Branches, and Commits (5 min)

- Complete levels 1-2 on the `Remote` tab of LearnGitBranching: https://learngitbranching.js.org/

### Model 2 Questions

1. Explain what happens to `origin/master` if you checkout `master` and make a commit.

2. Explain what happens if you checkout `origin/master` and make a commit.

3. True/false and explain; `origin/master` always points to the same commit locally that `master` points to in the remote repository.

## Model 3: Fetch and Pull (15 min)

- Complete levels 3-5 on the `Remote` tab of LearnGitBranching: https://learngitbranching.js.org/

### Model 3 Questions

1. Does `git fetch` modify any of your local branches: e.g., `master`, `feature-foo`, etc. If so which?

2. Does `git fetch` modify any of your local "remote" branches: e.g., `origin/master`, `origin/feature-foo`, etc.? If so which?

3. Does `git fetch` modify any of the remote repository's branches? If so which?

4. Are merge conflicts possible with `git fetch`?

5. In your own words, what is the purpose of `git fetch`?

6. Does `git pull` modify any of your local branches? If so which?

7. Does `git pull` modify any of your local "remote" branches? If so which?

8. Does `git pull` modify any of the remote repository's branches? If so which?

9. `git pull` is really two commands in one. Assuming you are on the `master` branch in a repository cloned from `origin`, write the two command sequence that is equivalent to `git pull`.

10. Are merge conflicts possible with `git pull`?

11. What should you always do after running `git pull`?

12. In your own words, what is the purpose of `git pull`?

13. Why do you think some developers avoid `git pull` and prefer using `git fetch`?

14. Is `git fakeTeamwork` a real git command? Is it on the exam? (Hint: these questions have the same answer.)

## Model 4: Push (10 min)

- Complete levels 6-7 on the `Remote` tab of LearnGitBranching: https://learngitbranching.js.org/

### Model 4 Questions

1. List all of the branches that are updated as a result of `git push`?

2. In your own words, what is the purpose of `git push`?

3. What does `git push` do if your local "remote" branch (e.g., `origin/master`) does not match `master` in the repository at `origin`?

4. If `origin/master` does not match `master` in `origin`, what sequence of commands will you run next? Briefly explain why you need to run each.

5. What's the next several commands and why?

6. What is the difference between `git pull --rebase` and `git pull`, and which do you think is better and why?

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
