# Remotes Practice

## Part 1

1. In a group on GitLab that your entire team has access to, have ***one*** of your team members
    1. Create a project ***initialized with a README.md*** named `ping-pong-INITIALS` where `INITIALS` are your team members' initials run together (e.g., `ping-pong-sjkw` for Stoney Jackson and Karl Wurst).
    2. Configure the repository so that anyone can push to master.
        1. Open `Settings` --> `Repository`
        2. Expand `Protected Branches`
        3. At the bottom of that section, set it so that `Developers + Maintainers` are allowed to `merge` and `push`.
    3. If you do not have a group that your entire team has access to... create the project as described above and then add your other team members as developers under `Settings` --> `Members`.
2. Have ***each*** team member configure git locally, and clone the repository, and position their terminals in the root of the project.
3. Now, assign a number to each team member: 1st, 2nd, etc.

    | Number | Team member |
    | ------ | ----------- |
    | 1st    |             |
    | 2nd    |             |
    | 3rd    |             |

4. Have the first team member do the following:
    1. Create an empty markdown file whose name is the same as the team member's name. E.g., `StoneyJackson.md`.
    2. Stage it.
    3. Commit it.
    4. Push it.
5. Then have the next team member do the following:
    1. Fetch the changes from origin.
    2. Merge `origin/master` into `master`.
    3. Create an empty markdown file whose name is the same as the team member's name. E.g., `StoneyJackson.md`.
    4. Stage it.
    5. Commit it.
    6. Push it.
6. If you have more than two team members, have each of them complete the previous step, ***one at a time***.
7. Starting with the first team member, ***one at a time***, repeat step 5, but this time each team member edits their file and adds their favorite movie to their file. Remember: fetch, merge, modify, stage, commit, and push.
8. Starting with the first team member, ***one at a time***, have each team member add a favorite book to their file, but this time, use `git pull` instead of `git fetch` and `git merge`. The other steps are the same.
9. Starting with the first team member, ***one at a time***, have each team member add a favorite animal to their file, but this time, use `git pull --rebase` instead of `git pull`.
10. Inspect the log of your repository. Identify where `git fetch`/`git merge`, `git pull`, and `git pull --rebase` where used. Can you tell the difference? What do you notice about the graph?
11. ***all at once***, have each team member add their favorite food to their personal file, using `git fetch`/`git merge`. Note, some members may have to use `git fetch`/`git merge` multiple times (one may not have to use it at all).
12. ***all at once***, have each team member add their favorite song to their personal file, using `git pull`. Note, some members may have to use `git pull` multiple times (one may not have to use it at all).
13. ***all at once***, have each team member add their favorite board game to their personal file, using `git pull --rebase`. Note, some members may have to use `git pull --rebase` multiple times (one may not have to use it at all).
14. Inspect the log of your repository. Identify where `git fetch`/`git merge`, `git pull`, and `git pull --rebase` where used. Can you tell the difference? What do you notice about the graph? Which command do you prefer and why?
15. Did you ever experience a conflict? Why or why not?

## Part 2

The purpose of this part is to experience and resolve conflicts.

1. Select a pull mechanism that your team will use: (e.g., `git fetch`/`git merge`, `git pull`, or `git pull --rebase`).

2. ***all at once***, have each team member rename their personal file as `ours.md`, using any mechanism the team agrees to to pull changes from the shared repository. By the end, `ours.md` should contain everyone's favorites. At least one of your team members should need to resolve a conflict.

3. ***all at once***, have each team member update `ours.md` in one of the following ways (assign each team member one of these tasks):
    * Group favorites by kind.
    * Add another favorite movie.
    * Add another favorite food.
    * Add another favorite animal.

4. ***all at once***, have each team member update `ours.md` in one of the following ways (assign each team member one of these tasks):
    * Sort the favorites within their groups.
    * Add titles for each group.
    * Add another favorite song.
    * Add another favorite book.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
