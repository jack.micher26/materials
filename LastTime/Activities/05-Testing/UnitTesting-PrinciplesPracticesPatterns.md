# Unit Testing - Principles, Practices, and Patterns

## Ch1 Questions

1. What is the goal of unit testing?
2. What does code coverage metrics tell us about tests?
3. What does the testability of code tell us about the code?

## Ch2 Questions

1. What is the definition of a unit test from the London/mockist) point of view?
2. What is the definition of a unit test from the Detroit/classical point of view?
3. What is a shared dependency? Give an example of one.
4. What is a private dependency? Give an example of one.
5. What is an out-of-process dependency? Give an example of one.
6. Explain how a database (which is usually a shared, out-of-process dependency)
  can be made to be a private, out-of-process dependency.
7. Which dependencies are replaced with test-doubles in the classical school of unit testing?
8. Which dependencies are replaced with test-doubles in the London school of unit testing?

## Ch3 Questions

1. What is the AAA pattern and how does it relate to GWT (Given-When-Then)?
2. When should you use GWT and when should you use AAA?
3. How many of each of the AAA sections should a unit test typically have?
4. Which AAA section is typically the largest?
5. How big should the act section typically be?
6. How many asserts statements in the assert section should a unit test typically have?
7. How should unit tests be named?

## Ch4 Questions

1. What are the four pillars of a good unit test?
2. What is a regression?
3. In general, how do we write tests that maximize protection against regressions?
4. What is refactoring?
5. What is meant by "resistance to refactoring"?
6. What is a false positive?
7. What is a false negative?
8. "Protection against regressions" helps address which types of errors:
  false positives or false negatives? Explain.
9. "Resistance to refactoring" helps address which types of errors:
  false positives or false negatives? Explain.
10. What contributes to a test's maintainability?
11. Which two pillars will you "max-out", that is they are non-negotiable?
12. Which two pillars must you strike a balance between?
13. Explain how white-box and black-box testing should be used with unit
  testing?

## Ch5 Questions

1. How is a mock different than a stub?
2. Why do using mocks make your tests more fragile and less resistant to refactoring.
3. Explain how to distinguish between "observable behavior" and "implementation detail".

## Ch6 Questions

1. What is output-based testing, and why is it preferred over other styles for unit tests.
2. What is state-based testing, and why is it worse than output-based for unit tests.
3. What is communication-based testing, and why is it the worst of the three
  styles for use in unit testing. When must you use it?

## Ch7 Questions

1. What are the characteristics of each of the following categories of code:
    * Trivial code
    * Domain model, algorithms
    * Controllers
    * Overcomplicated code
2. What kind of tests are used to focus on domain models and algorithms?
3. What kind of tests are used to test trivial code?
4. Why will tests that test overcomplicated code be less valuable? Consider each of the 4 pillars in your answer.
5. What kind of tests are used to test controllers?

## Ch8 Questions

1. What quadrant of code should integration tests target?
2. What is the longest happy-path?
3. Besides the longest happy-path, what other integration tests should you add?
4. What should be mocked in an integration test?
5. What should not be mocked in an integration test?
6. What is the purpose of, when would you use, end-to-end testing?
7. Why are integration tests better at protecting against regressions than unit tests?
8. Why are integration tests less resistant to refactoring than unit tests?
9. Why are integration tests likely slower than unit tests?
10. Why are integration tests harder to maintain than unit tests?

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
