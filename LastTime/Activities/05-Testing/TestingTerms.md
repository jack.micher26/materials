# Testing Terms

Testing terms you should know.

- Unit testing
- Integration testing
- Functional/End-to-End testing
- Performance testing
- Acceptance testing
- Smoke testing
- Automated testing
- Manual testing
- Black box testing
- White box testing
- Grey box testing
- Line coverage
- Branch coverage
- Static testing
- Dynamic testing
- Exploratory testing
- Test Driven Development
- Behavior Driven Development
- Acceptance Test Driven Development

## Activity

- Instructor adds an issue per term.
- Form teams
  - Manager
  - Recorder
  - Spokesperson (in 2-person team, combine with manager)
- Each team claims an issue, researches it, and provides on the issue
  - A list of references (at least 2)
  - A short definition
  - An example or illustration that goes with the definition
- Go over as a class
  - Spokesperson pressents
  - Class discusses
- Repeat for remainging terms

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
