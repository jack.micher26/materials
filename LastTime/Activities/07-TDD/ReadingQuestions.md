# TDD by Example - Reading Questions

* Manager:
* Recorder:
* Spokesperson:

## Introduction

1. What are the two simple rules that we should follow as developers?


2. The author claims that if you are a genius you don't need to follow these rules. Do you agree with this statement? Why or why not?


3. List any other insights that you took away from this chapter, and/or list any questions or ideas that you find confusing.



## Chapter 1 - Multi-Currency Money

1. List the steps in TDD. (Keep these close at hand, and refer to them often.)


2. What are the first two scenarios identified by the author? And what are the examples (GWT) that go with them?


3. What is the first method written?


4. As the author notices issues/mistakes he doesn't fix them. What does he do with them? Why doesn't he fix them (what's the goal)?


5. How much code does the author write to get the test to compile?


6. Does the tests pass once the test compiles?


7. How much code does the author write to pass the test? Is it "intelligent" code?


8. What is the relationship between "dependency" and "duplication"? Which will we treat in TDD? Why do you think that is?


9. What kind of duplication does the author first identify?


10. As the author refactors his code and test to remove the duplication, how often is he running his tests? What are the results of running the tests?


11. Do you always have to refactor in very tiny steps?


12. Try drawing an analogy between refactoring code and reducing an equation in algebra, especially in terms of the size of the steps you should take.


13. How does the `*=` operator reduce duplication?


14. List any other insights that you took away from this chapter, and/or list any questions or ideas that you find confusing.




## Chapter 2 - Degenerate Objects


1. Kent Beck gives us a new simplified sequence of steps for TDD. What are the new steps.


2. In TDD, when do you start thinking about the public API of the thing you are building?


3. In TDD, when do we do all the good engineering of our implementation?


4. Writing clean code that works is hard. How does TDD help?


5. To work on "Dollar side effects?", does Kent Beck start by writing a ___new___ test? If not, what does he do?


6. What are the two (of three) strategies (presented in this chapter) for getting to a green?


7. How do you decide which strategy to use? (Or how does the author decide?)


8. List any other insights that you took away from this chapter, and/or list any questions or ideas that you find confusing.



## Chapter 3 - Equality for All

1. What new items does Kent Beck add to his to-do list and why?


2. Why do you think Kent Beck decide to go after equals()?


3. Explain triangulation? How does it play out in tests? When should you employ it?


4. What does Kent Beck add to his to-do list?


5. Explain why a to-do list is essential to TDD?


6. List any other insights that you took away from this chapter, and/or list any questions or ideas that you find confusing.


## Chapter 4 - Privacy

1. What new functionality is written in this chapter?


2. How many times did a test fail in this chapter?


3. Overall then, what was done in this chapter?


4. How does TDD help us manage risk?


5. What insights or questions do you have about this chapter?




## Chapter 5 - Franc-ly Speaking

1. Kent Beck is still not confident about tackling `$5 + 10 CHF = $10 if rate is 2:1`. So ***what*** does he do and ***how***?


2. Let's review... after you have a failing test, what is your goal?


3. What new items are added to the list?


4. By the end of the chapter, what phase did we finish?


5. What insights or questions do you have about this chapter?



## Chapter 6 - Equality for All, Redux

1. At the start of the chapter, what phase of TDD does Kent Beck enter?


2. What do you need before you can refactor code, (or else your teeth will go bad)? And what should you do if you don't have what you need?


3. By the end of the chapter, what phase did Kent Beck complete?


4. What insights or questions do you have about this chapter?




## Chapter 7 - Apples and Oranges

1. What was accomplished this chapter? And at a high-level how was it accomplished?


2. Explain why the new item "Currency?" was added to the to-do list.


3. What insights or questions do you have about this chapter?




## Chapter 8 - Makin' Objects

1. How does Kent Beck decouple the tests from concrete classes Dollar and Franc?


2. Explain what "Delete testFrancMultiplication?" is about.


3. What insights or questions do you have about this chapter?



## Chapter 9 - Times We're Livin' In

You should be getting pretty good at analyzing these chapters now.

1. What was accomplished in this chapter.


2. What key insights did you take away from this chapter?


3. What questions do you have?




---
Copyright 2020, Stoney Jackson \<dr.stoney@gmail.com\>;

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
