# TDD Summary

## TDD Cycle (Basic)

```plantuml
[*] --> WriteFailingTest
WriteFailingTest --> MakeCodePass
MakeCodePass --> RemoveDuplication
RemoveDuplication --> [*]

WriteFailingTest: Initial condition: all tests are passing.
WriteFailingTest: Designing public API and observable behavior.
WriteFailingTest: Keep it small, and make sure it fails.

MakeCodePass : Condition: one test is not compiling or is failing.
MakeCodePass: Make code pass the test, and do it quickly!
MakeCodePass: Make note of sins on your to-do list. Don't fix them... yet.
MakeCodePass: Try faking it, obvious implementation, and triangulation.
MakeCodePass: If you get stuck, revert work since and including last test; retry taking smaller steps.

RemoveDuplication : Condition: all tests are passing
RemoveDuplication : Keep tests passing.
RemoveDuplication : Refactor to remove duplicate data, code, and tests
RemoveDuplication : If you break something and get stuck, revert work and retry taking smaller steps.
```

## Red-Green States

```plantuml
[*] --> Green
Green --> Red : write a failing test
Red --> Green : write code to pass test
Red --> Green : revert

Green : Condition: all tests pass.
Green : Never modify public API (write failing test first).
Green : Never modify observable behavior (write failing test first).
Green : Only refactor (e.g., remove duplication (including duplicating tests), improve design, improve clarity).
Green : Write passing tests to cover existing behavior only to support refactoring. (Beware!)

Red : Condition: only one failing/non-compiling test.
Red : Write code to pass test as quickly as possible.
Red : Delay all else by adding them to to-do list.
Red : If you get stuck, revert work since and including last test; retry taking smaller steps.
```


## TDD Cycle (more complete)

The to-do list contains two types of items: 1) items that represent "new" behavior and 2) items that represent an improvement to the design of the tests or system. "New" behavior requires a change to the system's public API or its publicly observable behavior. For these items we use the traditional TDD cycle and start by implementing a failing test.

Items of the second category, that represent improvements to the design of the tests or system, do not modify the public API of the system or its publicly observable behavior. For those, we do not need to implement a failing test, we just need to refactor using our existing tests. However, there may be existing behaviors not covered by tests that we fear we may break when we refactor; we will write new passing tests or these. But We need to be careful that these tests actually test what we think they should test.

```plantuml
[*] --> SelectItem
SelectItem --> ModifyBehavior : Changes publicly observable behavior
SelectItem --> ImproveDesign : Doesn't change publicly observable behavior

state ModifyBehavior {
  [*] --> WriteFailingTest
  WriteFailingTest --> MakeCodePass
  MakeCodePass --> RemoveDuplication
  RemoveDuplication --> [*]
}
ModifyBehavior --> CrossOffItem

state ImproveDesign {
  [*] --> Refactor
  Refactor --> [*]
}
ImproveDesign --> CrossOffItem

CrossOffItem --> SelectItem


SelectItem : Select an item that represents "one step".
SelectItem : Add simpler, related items if none feels like "one step".

WriteFailingTest: Initial condition: all tests are passing.
WriteFailingTest: Designing public API and observable behavior.
WriteFailingTest: Keep it small, and make sure it fails.

MakeCodePass : Condition: one test is not compiling or is failing.
MakeCodePass: Make code pass the test, and do it quickly!
MakeCodePass: Make note of sins on your to-do list. Don't fix them... yet.
MakeCodePass: Try faking it, obvious implementation, and triangulation.
MakeCodePass: If you get stuck, revert work since and including last test; retry taking smaller steps.

RemoveDuplication : Condition: all tests are passing
RemoveDuplication : Keep tests passing.
RemoveDuplication : Refactor to remove duplicate data, code, and tests
RemoveDuplication : If you break something and get stuck, revert work and retry taking smaller steps.

Refactor : Condition: all tests are passing
Refactor : Keep all tests passing
Refactor : Refactor to improve design, clarity, and remove duplication.
Refactor : May implement passing tests for existing behavior to support refactor.

CrossOffItem : Consider rewriting list if it is messy.
```

## TDD with Version Control

I recommend committing after completing one cycle. Each time I cross off an item, I commit. This means that each commit "works", and if I ever get stuck I simply throw away all my changes since the last commit. If throwing away your changes scares you, you are probably trying to make too big of changes.


## Writing a Passing Test

OK, you are in a green state --- all tests are passing. Then you write a test that also passes. This is a strange event that should make you worried. When we write tests in TDD, they should fail. Let's explore why you should be concerned and under what circumstances you may legitimately want to write a passing test.


### It's not worth it

Often writing a passing test is not worth it. Why? The reason the new test is passing is probably because you have one or more other tests that cover the behavior you are testing. Instead, consider updating or combining existing tests to better capture the behavior you were planning to test. Or, write a your new passing test, but remove old ones made obsolete by your new one. But before you do, be sure to read "Risky business" below.

### A time for everything

You might need to write a passing test to cover a behavior that is currently uncovered by any other tests in anticipation of a refactor that may break this behavior. Here is a restatement of this scenario in GWT form.

```
GIVEN an existing behavior B
  AND B is not covered by another test
WHEN you plan a refactoring R
  AND you are concerned that R may break B
THEN write a passing test that covers B.
```

If you must write a passing test, be sure to read "Risky business" below.

### Risky business

When writing a passing test, you risk writing a bad test. You may write a tautology (something that is always true) without realizing it. You think it's testing something, but it's not testing what you think it is testing.

Writing failing tests first, and then writing code to pass the test, helps us ensure the test is correct. Starting with a passing test means that you have not confirmed that it tests what it is intended to test.

One way you can try to confirm that the new passing tests is correct, is to modify the code so that it fails the test (as expected) and then undo your modification so it is passing again.
