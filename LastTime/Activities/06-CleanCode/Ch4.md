

## Explaining yourself in code

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Legal Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Informative Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Explanation of Intent

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Clarification

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Warning of Consequences

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## TODO Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Amplification

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Javadocs in Public APIs

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Mumbling

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Redundant Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Misleading Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Mandated Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Journal Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Noise Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Scary Noise

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Don't Use a comment when you can use a function or or a Variable

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Position Markers

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Closing Brace Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Attributions and Bylines

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Commented-out code

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## HTML Comments

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Nonlocal information

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Too much information

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Inobvious connection

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Function headers

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---

## Javadocs in nonpublic code

### Team answering

* Manager:
* Spokesperson:
* Recorder

### Unclean example

### Clean example

### Other thoughts

---
Copyright 2020, Stoney Jackson \<dr.stoney@gmail.com\>;

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
