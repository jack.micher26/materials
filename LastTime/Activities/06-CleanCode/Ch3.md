# Clean Code - Act02 - Ch3 Functions

| Role           | Name           |
| :------------- | :------------- |
| Manager        |                |
| Recorder       |                |
| Spokesperson   |                |

---

Sections

* **F1: Functions should be small**
* **F2: Functions should do one thing**
* **F3: All statements should be at the same level of abstraction**
* **F4: Blocks and Indenting (nested structures)**
* **F5: Sections within functions**
* **F6: Switch statements**


1. How small should functions be?


2. How many statements should typically be inside the body of an if-statement, while-loop, etc.?


3. Identify two statements in Listing 3-1 on pp 32-33 that have different levels of abstraction.

4. Identify two statements in Listing 3-2 on pg 33 that have different levels of abstraction.

5. Does Listing 3-3 on pg 35 have statements on different levels of abstraction?

6. "Functions should do one thing" doesn't actually mean that they should have only one statement. Based on your analysis in 3, 4, and 5, create a new rule for how small a function should be.

7. How many levels of nested structures (if-statements, while-loops, etc.) should a function typically have?

8. How many "sections" should be in a function?


9. What's the problem with switch statements? What can we do about them?

---

Sections

* **F7: Use Descriptive Names**

1. Are long names acceptable?

2. What can we do about long names?

---

Sections

* **F8: Niladic functions**
* **F9: Monadic functions**
* **F10: Flag argument**
* **F11: Dyadic functions**
* **F12: Triadic functions**
* **F13: Argument Objects**
* **F14: Argument Lists**
* **F15: Verbs and Keywords**


1. How many parameters should a function have?

2. What's wrong with functions with many parameters?

3. How can we use objects to reduce the number of parameters?

4. What's wrong with flag parameters?

5. Under what circumstances are argument lists acceptable?

6. When it comes to function names, what are keywords and what do they help with?

---

Sections

* **F16: Have no side effects**
* **F17: Output arguments**
* **F18: Command query separation**



1. Why does Uncle Bob consider side effects lies?

2. What is the relationship between _this_ in OO-languages like Java and output arguments?

3. What is meant by "command query separation"?

---

Sections

* **F19: Prefer Exceptions to Returning Error Codes**
* **F20: Error Handling is One Thing**

1. What's the trouble with returning error codes?

2. If a function contains a try-catch block, what should probably be the first word in the function?

---

Sections

**F21: Don't Repeat Yourself**
**F22: Structured Programming**

1. DRY - Don't Repeat Yourself - "Duplication may be the root of all evil." Enough said. (No question.)

2. Why practice suggested by structured programming is not as relevant when practicing clean code and why?


---
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
