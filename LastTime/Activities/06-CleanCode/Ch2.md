
## Clean Code - Chapter 2 Meaningful Names

| Role           | Name           |
| :------------- | :------------- |
| Manager        |                |
| Recorder       |                |
| Spokesperson   |                |


For each practice, give one or more "Bad" examples, one or more "Good" examples,
and a tip, memory aide, summary, or a key quote.


### Intention-Revealing

* "Bad" examples: `amount`
* "Good" examples: `priceInDollars`
* Tip, memory aide, summary, or key quote: _Make explicit the implicit._

### Disinformation

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Distinctions

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Pronounceable

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Searchable

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Encodings

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Mental mapping

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Class names

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Method names

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Cute

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### One word per concept

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Puns

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Solution domain

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Problem domain

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Context

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

### Gratuitous Context

* "Bad" examples:
* "Good" examples:
* Tip, memory aide, summary, or key quote:

---
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
