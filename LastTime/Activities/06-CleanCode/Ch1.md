# Clean Code - Chapter 1 Clean Code

Team 1:
- Manager:
- Recorder:
- Spokesperson (same as manager in a 2-person team):

## Chapter 1

1. What is the story the author is telling in this chapter?

2. What are some characteristics of clean code?

3. What is the "Boy Scout Rule" and how does it apply to code?


---
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
