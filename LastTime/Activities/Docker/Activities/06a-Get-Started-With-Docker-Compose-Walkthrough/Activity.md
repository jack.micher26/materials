# Activity - Get Started with Docker Compose Walkthrough

## Overview

Walk through the exmample given in [Get Started with Docker Compose](https://docs.docker.com/compose/gettingstarted/).

There are multiple ways to conduct this activity. One could have the students
work throught the example, and discuss the files and their contents either
before or after the students work through the example. Or one could walk
through the example as the instructor and talk through the files as they go.

## Starter files

To reduce the amount of typing that is required, and to ensure files are put in
the correct locations, starter files are given in ./composetest.

Note, some files have version numbers: e.g., `app1.py` and `app2.py`. For such
files, there is also a corresponding file without the version numer: e.g.,
`app.py`. The versioned files represent the versions of the file as you work
through the example. Whereas the unversioned file should be thought of as the
current state of the file, and initially has the contents of version 1.
To switch between versions of the file, copy one of the versioned files over
the non versioned files. For example

```
cp app2.py app.py
```

To switch back to the first version...

```
cp app1.py app.py
```

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
