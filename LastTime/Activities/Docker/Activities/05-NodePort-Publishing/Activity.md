# Docker - NodePort Publishing

## Model 1

- Goal: Run a webserver on localhost:80.
- Fact: Webservers use port 80/tcp by default (per HTTP).
- Fact: NGINX is a webserver, so it listens on port 80 by default.

```
docker run --rm --detach nginx
docker ps
```

- Open browser to <http://localhost/>.

1. What ports are open on the NGINX server?
2. Was the browser successful?
3. Did we reach our goal?
4. Why do you think we did or did not reach our goal?

```
docker stop $(docker ps -q)
docker rm $(docker ps -a -q)
```

## Model 2

- Goal: Run a webserver on localhost:80.

```
docker run --rm --detach -p 80 nginx
docker ps
```

- Open browser to <http://localhost/>.

>&#9888; WARNING: Your browser caches pages you've previously visited.
> Always refresh the page to ensure you are viewing the most recent version of the page.

1. What's different about the `docker run` command this time?
2. What's different about the output of `docker ps`?
3. Did we reach our goal?
4. Why do you think we did or did not reach our goal?

```
docker stop $(docker ps -q)
docker rm $(docker ps -a -q)
```

## Model 3

- Goal: Run a webserver on localhost:80.

```
docker run --rm --detach -p 80:80 nginx
docker ps
```

- Open browser to <http://localhost/>.

1. What's different about the `docker run` command this time?
2. What's different about the output of `docker ps`?
3. Did we reach our goal?
4. Why do you think we did or did not reach our goal?


```
docker stop $(docker ps -q)
docker rm $(docker ps -a -q)
```

## Model 4

- Goal: Run a webserver on localhost:8888.

```
docker run --rm --detach -p 80:8888 nginx
docker ps
```

- Open browser to <http://localhost/>.
- Open browser to <http://localhost:8888/>.

1. What's different about the `docker run` command this time?
2. What's different about the output of `docker ps`?
3. Did we reach our goal?
4. Why do you think we did or did not reach our goal?

```
docker stop $(docker ps -q)
docker rm $(docker ps -a -q)
```

## Model 5

- Goal: Run a webserver on localhost:8888.

```
docker run --rm --detach -p 8888:8888 nginx
docker ps
```

- Open browser to <http://localhost/>.
- Open browser to <http://localhost:8888/>.

1. What's different about the `docker run` command this time?
2. What's different about the output of `docker ps`?
3. Did we reach our goal?
4. Why do you think we did or did not reach our goal?

```
docker stop $(docker ps -q)
docker rm $(docker ps -a -q)
```

## Model 6

- Goal: Run a webserver on localhost:8888.

```
docker run --rm --detach -p 8888:80 nginx
docker ps
```

- Open browser to <http://localhost/>.
- Open browser to <http://localhost:8888/>.

1. What's different about the `docker run` command this time?
2. What's different about the output of `docker ps`?
3. Did we reach our goal?
4. Why do you think we did or did not reach our goal?

```
docker stop $(docker ps -q)
docker rm $(docker ps -a -q)
```

## Model 7

- Goal: Summarize what we know.

1. By default, can you access a container through the host network?
2. In `docker run`, what does the `-p` option do?
3. What is the meaning of the first number in `-p 80:8888`?
4. What is the meaning of the second number in `-p 80:8888`?

## Model 8

- Goal: Apply what we've learned.

1. Run two NGINX containers at the same time. Make one accessible through localhost:10000,
   the other on localhost:20000.
2. What commands did you use?
3. Stop and remove your containers.


## Model 9

Goal: Reflect on your how well your team is functioning. 

1. What's working?

2. What can be improved?

3. What changes will you make to improve?

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
