# GitLab Introduction

## Learning Objectives

Able to

1. Explain the purpose of GitLab
2. Explain the purpose of a GitLab Group
3. Explain the purpose of a GitLab Project
4. Able to identify GitLab's roles and their relative permissions.

## Team Roles

| Role      | Name | Responsibility |
| --------- | ---- | -------------- |
| Manager   |      | Manage time. Ensure everyone has a voice. Help solve problems. |
| Recorder  |      | Write answers on behalf of the team. Help solve problems. |
| Presenter and Reflector |      | Speaks for the team. Helps solve problem. Identify what the team is doing well, and suggest how it might improve |


## Model - GitLab

https://about.gitlab.com/what-is-gitlab/

### Questions

1. What is GitLab according to GitLab?


2. List any terms from their description that your team has not heard of before or are fuzzy about. Spend a moment and try to find a definition on-line for those terms.



## Model - GitLab Groups

https://docs.gitlab.com/ee/user/group/

### Questions

1. What is the purpose of a GitLab group?



## Model - GitLab Projects

- https://docs.gitlab.com/ee/user/project/


### Questions

1. Which of the following are included with a GitLab Project (select all that apply)
    - [ ] A Git repository
    - [ ] An issue tracker
    - [ ] A Docker container registry
    - [ ] A website for your project
    - [ ] A Wiki for your project
    - [ ] A place to store and share small fragments of example code
    - [ ] A way to distribute releases of the project
    - [ ] A way to automatically test and deploy the project (often known as continuous integration and continuous delivery or deployment).
    - [ ] A way for visitors to donate to your project.
2. What are the possible visibility of projects on GitLab, and what do they mean? (You might have to click a link to find out; if you do, return back to the link in Model 1 before continuing.)
3. What restrictions are there on the number of private projects you can create or work on before you have to start paying for them?


## Model - GitLab Permissions

- https://docs.gitlab.com/ee/user/permissions.html

### Questions

1. List the different roles available in GitLab. Based on their privileges give a one-sentence characterization/description/meaning of that roll.

2. Which roles can create a project within a group?


---
Copyright © 2019 Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
