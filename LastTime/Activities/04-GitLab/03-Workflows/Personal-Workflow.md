# Personal Workflow

## Overview

Your instructor will lead the class through a workflow for working on a project.

You will work in teams to follow along.

| Role  | Position | Name | Purpose |
| ----- | -------- | ---- | ------- |
| Pilot | Middle   |      | Carry out each step on behalf of the team. |
| Recorder |       |      | Document details of steps as they are shown by your instructor, and capture key points from any discussion along the way. |
| Manager |        |      | Act as an extra set of eyes for the team. Coordinate with your instructor if your team is not ready to move on. Get help from other teams nearby. |

## Objectives

By the end of this activity, you team should have

* A workflow annotated with enough detail that anyone in your team can follow the workflow to work on a personal project.
* A understanding of the purpose each step.

## Workflow

1. Create a project on GitLab, and configure it so that no one can push to `master`.
2. Add issues to issue tracker as a to-do list and prioritize them.
3. Clone the project locally.
4. Pick an issue to work on.
5. Create a branch and a merge-request for that issue. (You can remove the WIP status, since only you can and will merge your merge-request.)
6. Work on that branch for that issue
   * Pushing changes as you go.
   * Write short but meaningful commit messages that will help remind you of what you did.
7. Either
   * Merge into `master` when you are done and satisfied that it works correctly.
      * Write a good commit message for the merge (use the small commit messages to help you remember what you did.)
      * Squash the commits.
      * Delete the branch.
   * Or give up on the merge-request and close it.
      * Delete the branch
8. Clean up your local repository
   * Pull master
   * Delete the local issue branch
   * Delete the remote issue branch (if it wasn't deleted when you merged or closed it)
   * Remove local references to the remote issue branch
   * Merge master into all other issue branches and push.
9. If the issue you were working on is done, close it.
10. Review and update your issues, adding new ones, closing invalid ones, re-prioritize, etc.
11. Return to step 4.

## Questions

*Feel free to add more questions as they are raised or as they are discussed.*

1. Why use an issue tracker with issues for a personal project?
2. Why use branches and merge-requests?
3. Why delete branches after merge?
4. What is squashing and why do it?

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
