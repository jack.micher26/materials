# Untrusted Contributor Workflow

## Overview

Your instructor will lead the class through a workflow for working on a project.

You will work in teams to follow along.

| Role  | Position | Name | Purpose |
| ----- | -------- | ---- | ------- |
| Pilot | Middle   |      | Carry out each step on behalf of the team. |
| Recorder |       |      | Document details of steps as they are shown by your instructor, and capture key points from any discussion along the way. |
| Manager |        |      | Act as an extra set of eyes for the team. Coordinate with your instructor if your team is not ready to move on. Get help from other teams nearby. |

## Objectives

By the end of this activity, you team should have

* A workflow annotated with enough detail that anyone in your team can follow the workflow to work on a personal project.
* A understanding of the purpose each step.

## Workflow

1. [One] Create a public project on GitLab.
2. [One] Create a development board with a `ToDo`, `Doing`, and `Done` columns.
3. Add issues to ToDo column.
4. If you are not the maintainer of the project, fork the project.
5. Clone ***your fork*** locally.
6. Create a remote in your local repository named `upstream` that points to the `upstream` clone URL.
7. Pick an issue, make sure no one else has started working on it, discuss it with the community and a maintainer to get "buy-in".
8. Create a branch and a `WIP` merge-request for that issue, and make sure that it is linked to from the original issue.
9. Work on that branch for that issue
      * Pushing changes as you go.
      * Write short but meaningful commit messages that will help remind you of what you did.
10. Either
      * If satisfied,
         * Write a good commit message in the description for the merge (use the small commit messages to help you remember what you did.)
         * Request a maintainer review and merge your merge request.
         * A maintainer reviews and either requests adjustments, closes the merge-request without merging, or merges the merge-request.
            * Squash commits
            * Delete the branch.
      * Or give up on the merge-request and close it.
         * Delete the branch
11. Clean up your local repository
      * Pull changes from upstream's master into your local master
      * Push your local master to your fork (origin).
      * Delete the local issue branch
      * Delete the remote issue branch in origin
      * Remove local references to the remote issue branch
      * Merge master into each outstanding issue branch, and push them to origin.
12. If the issue you were working on is done, suggest that it should be closed.
13. Return to step 5.

## Questions

1. Why should you discuss an issue with a maintainer before working on it?
2. Why, if you are not a maintainer, do you need to fork the project?
3. Why do you pull changes from upstream's master rather than origin?
4. Why don't you close the issue when you are done with it?

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
