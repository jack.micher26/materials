# Trusted Team Workflow

## Overview

Your instructor will lead the class through a workflow for working on a project.

You will work in teams to follow along.

| Role  | Position | Name | Purpose |
| ----- | -------- | ---- | ------- |
| Pilot | Middle   |      | Carry out each step on behalf of the team. |
| Recorder |       |      | Document details of steps as they are shown by your instructor, and capture key points from any discussion along the way. |
| Manager |        |      | Act as an extra set of eyes for the team. Coordinate with your instructor if your team is not ready to move on. Get help from other teams nearby. |

## Objectives

By the end of this activity, you team should have

* A workflow annotated with enough detail that anyone in your team can follow the workflow to work on a personal project.
* A understanding of the purpose each step.

## Activity Instructions

1. Have all of your team members work on a different issue, and get to the point of requesting a review from another team member (step 8).
2. Review and merge one of the merge-requests.
3. Help each other team member (one at a time) update their local repository and feature branch.
4. Select another to review and merge. And again help each other team member (one at a time) update their local repository and feature branch.
5. Keep selecting and working on issues using this workflow until your team can do it smoothly.

## Workflow

1. [One] Create a project on GitLab, and configure it so
      * No one can push to `master`
      * Maitainers and developers can merge to `master`
2. [One] Add other team members as `developers` on your project
3. [One] Create a development board with a `ToDo`, `Doing`, and `Done` columns.
4. Add issues to ToDo column.
5. [All] Clone the project locally.
6. Pick an issue to work on, and assign it to yourself, and move it to `Doing`. Make sure no one else has done the same before moving on. If someone has done the same, negotiate who's going to do it.
7. Create a branch and a `WIP` merge-request for that issue.
8. Work on that branch for that issue
      * Pushing changes as you go.
      * Write short but meaningful commit messages that will help remind you of what you did.
9. Either
      * If satisfied,
         * If there are new changes in master, first update your feature branch (see section below on "Updating your feature branch with changes in master")
         * Write a good commit message in the description for the merge (use the small commit messages to help you remember what you did.)
         * Remove `WIP` from your merge-request
         * Request another team member to review and merge your merge request.
         * Other, reviews and either requests adjustments, closes the merge-request without merging, or merges the merge-request.
            * Squash commits
            * Delete the branch.
      * Or give up on the merge-request and close it.
         * Delete the branch
10. Clean up your local repository
      * (all) Pull master
      * Delete the local issue branch
      * Delete the remote issue branch
      * Remove local references to the remote issue branch
      * (all) Merge master into each outstanding issue branch, and push them.
11. If the issue you were working on is done, move it to done and then close it.
12. Review and update your issues, adding new ones, closing invalid ones, re-prioritize, etc.
13. Return to step 6.

## Updating your feature branch with changes in master

1. Synchronize your local `master` with origin's.
2. Merge `master` into your feature branch, and test it.
3. Push your feature branch.

## Questions

1. Why have an issue board?
2. Why claim an issue before working on it?
3. Why move an issue to `Doing` before working on it?
4. Why move an issue to `Done` before closing it?
5. Why should someone else review and merge your merge-request?
6. How does this workflow help avoid merge conflicts?
7. If your team does a good job reviewing merge-requests before merging them, and always squashes the commits on a feature branch before merging, what should be true about the commits in `master`?
8. Why does everyone need to pull master and merge it into their issue branches every time someone merges a merge-request?

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
