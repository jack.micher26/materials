# Personal Workflow Illustrated

## Overview

* We use the project's issue tracker on GitLab to keep a to-do list.

* `master` always and only contains finished work. Each commit on `master` is an atomic, logical, conceptual change. Theoretically, any commit on `master` could be checked out into the working tree and it would be meaningful and work.

* We always work on a feature branch cut from the most recent version of `master`. This allows us to keep `master` clean as described above, and allows us to easily abandon the work without disrupting `master`.

* We publish our feature branch early and push our work to it often. That way, if something happens to our local machine, we have a current backup of our work.

* We start a merge-request early. This merge-request will ultimately be used to merge our feature branch into master. This merge-request gives us a good place to keep notes during development, and is where we will compose our final good commit message.

* When we are done, we merge on GitLab using the merge-request. We have GitLab squash all of our "in-between" commits into a single logical commit with a good commit message. This helps us keep `master` clean. Also, this is the time to reflect on our work and compose a good commit message. This is a great time to do so as we have completed the work and know we want to keep it; so it's worth documenting.

* Once merged on GitLab, we clean up by closing issues on GitLab and synchronizing our local repository in preparation for continued work.

## Clone project locally

```bash
git clone url
cd project-root
```

```mermaid
graph RL
   subgraph origin
      master'[master] --> 0((0))
   end
   subgraph local
      *master --> 0'((0))
      origin/master --> 0'((0))
   end
```

## Create and publish feature branch and merge-request

```bash
git branch feature
git checkout feature
git push -u origin feature
```

On GitLab, create merge request.

```mermaid
graph RL
   subgraph origin
      master'[master] --> 0'((0))
      feature'[feature] --> 0'((0))
   end
   subgraph local
      master --> 0((0))
      *feature --> 0((0))
      origin/master --> 0((0))
      origin/feature --> 0((0))
   end
```

## Do some work

```bash
edit
git add .
git commit -m "..."
git push
```

```mermaid
graph RL
   subgraph origin
      master'[master] --> 0'((0))
      1'((1)) --> 0'((0))
      feature'[feature] --> 1'((1))
   end
   subgraph local
      master --> 0((0))
      1((1)) --> 0((0))
      *feature --> 1((1))
      origin/master --> 0((0))
      origin/feature --> 1((1))
   end
```

## Do some more work

```bash
edit
git add .
git commit -m "..."
git push
```

```mermaid
graph RL
   subgraph origin
      master'[master] --> 0'((0))
      1'((1)) --> 0'((0))
      2'((2)) --> 1'((1))
      feature'[feature] --> 2'((2))
   end
   subgraph local
      master --> 0((0))
      1((1)) --> 0((0))
      2((2)) --> 1((1))
      *feature --> 2((2))
      origin/master --> 0((0))
      origin/feature --> 2((2))
   end
```

## Merge merge-request

On GitLab, write a good commit message, merge merge-request, squashing and deleting branch.

```mermaid
graph RL
   subgraph origin
      1'((1)) --> 0'((0))
      2'((2)) --> 1'((1))
      1-2'((1-2)) --> 0'((0))
      m'((m)) --> 1-2'((1-2))
      m'((m)) --> 0'((0))
      master'[master] --> m'((m))
   end
   subgraph local
      master --> 0((0))
      1((1)) --> 0((0))
      2((2)) --> 1((1))
      *feature --> 2((2))
      origin/master --> 0((0))
      origin/feature --> 2((2))
   end
```

## Synchronize local master

```bash
git checkout master
git pull
```

```mermaid
graph RL
   subgraph origin
      1'((1)) --> 0'((0))
      2'((2)) --> 1'((1))
      1-2'((1-2)) --> 0'((0))
      m'((m)) --> 1-2'((1-2))
      m'((m)) --> 0'((0))
      master'[master] --> m'((m))
   end
   subgraph local
      1((1)) --> 0((0))
      2((2)) --> 1((1))
      1-2((1-2)) --> 0((0))
      m((m)) --> 1-2((1-2))
      m((m)) --> 0((0))
      feature --> 2((2))
      *master --> m((m))
      origin/master --> m((m))
      origin/feature --> 2((2))
   end
```

## Remove feature branch locally

```bash
git pull --prune
git branch -D feature
```

```mermaid
graph RL
   subgraph origin
      1'((1)) --> 0'((0))
      2'((2)) --> 1'((1))
      1-2'((1-2)) --> 0'((0))
      m'((m)) --> 1-2'((1-2))
      m'((m)) --> 0'((0))
      master'[master] --> m'((m))
   end
   subgraph local
      1((1)) --> 0((0))
      2((2)) --> 1((1))
      1-2((1-2)) --> 0((0))
      m((m)) --> 1-2((1-2))
      m((m)) --> 0((0))
      *master --> m((m))
      origin/master --> m((m))
   end
```

## Garbage collection

Garbage collection over time (1-2 weeks).

```mermaid
graph RL
   subgraph origin
      1-2'((1-2)) --> 0'((0))
      m'((m)) --> 1-2'((1-2))
      m'((m)) --> 0'((0))
      master'[master] --> m'((m))
   end
   subgraph local
      1-2((1-2)) --> 0((0))
      m((m)) --> 1-2((1-2))
      m((m)) --> 0((0))
      *master --> m((m))
      origin/master --> m((m))
   end
```

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
