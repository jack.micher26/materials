# Recovering from Common Mistakes

## Oops, commits on the wrong branch

If you have already created a feature branch, you need to merge master into your feature-branch, and then reset master to the same commit as origin/master.

```bash
git checkout feature-branch            # prepare to merge master into feature-branch
git merge master                       # merge master into feature-branch
git branch -f master origin/master     # move master to the same commit as origin/master
```

After doing the above, feature-branch will be your active branch and will contain all your work. Master will be in synch with the last known location of origin/master.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
