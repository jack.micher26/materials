# Exam 2 - Study Guide

## Git

* Branching
  * Creating a new branch, starting from a particular branch, and checking it out to work on it.
    ```
    git checkout STARTING_BRANCH
    git branch FEATURE_BRANCH
    git checkout FEATURE_BRANCH
    ```
  * Deleting a local branch.
    ```
    git branch -d BRANCH_TO_DELETE
    ```
  * Deleting a remote branch on origin.
    ```
    git push origin --delete BRANCH_TO_DELETE
    ```
  * Deleting local references to now deleted remote tracking branches.
    ```
    git pull --prune
    ```
* Merging
  ```
  git checkout BRANCH_TO_MERGE_INTO
  git merge BRANCH_WITH_CHANGES_TO_MERGE
  ```
* Conflicts
  * Lexical - detected by git, but requires manual resolution.
    1. Use `git show` to identify conflicted files.
    2. Edit conflicted files, find conflict annotations, rewrite those sections removing annotations.
    3. Use `git add` to mark files resolved.
    4. Use `git commit` to finish the merger or `git merge --abort` to abort.
  * Logical - only detectable by testing.
  * Note a merge may have both lexical and logical conflicts.
* Remotes
  * `git remote add upstream CLONE_URL_TO_OTHER_REPO` to create one named upstream
  * You get origin for free when you clone.
  * `git remote -v` to view the ones you have
* Untrusted Contributor Workflow: Contributing without developer status
  * Setup
    * Fork (on GitLab)
    * Clone: `git clone CLONE_URL_OF_YOUR_FORK`
    * Set upstream remote: `git remote add upstream CLONE_URL_OF_UPSTREAM`
  * Work on an issue
    * Claim issue (or comment on the issue)
    * Synchronize local and fork master with upstream
      ```
      get checkout master
      git pull upstream master
      git push origin master
      ```
    * Create feature branch
      ```
      git checkout master
      git branch FEATURE_BRANCH
      git checkout FEATURE_BRANCH
      ```
    * Publish feature branch
      ```
      git checkout FEATURE_BRANCH
      git push -u origin FEATURE_BRANCH
      ```
    * Create WIP merge request (on GitLab)
    * Make, stage, commit, and push changes
      ```
      git checkout FEATURE_BRANCH
      ### make some changes, and then ...
      git add .
      git commit
      git push origin FEATURE_BRANCH
      ```
    * Remove WIP from merge request and request a review (on GitLab)
    * Update feature branch with changes in upstream's master
      ```
      git checkout master
      git pull upstream master
      git push origin master
      git checkout FEATURE_BRANCH
      git merge master
      ### test and resolve any conflicts; stage and commit them; and then ...
      git push origin FEATURE_BRANCH
      ```
  * Clean up
    * Synchronize local and fork master with upstream
      ```
      git checkout master
      git pull upstream master
      git push origin master
      ```
    * Delete feature branch in local and remote, and references to remote feature branch.
      ```
      git checkout master
      git branch -D FEATURE_BRANCH
      git push origin --delete FEATURE_BRANCH
      git pull --prune
      ```
* Trusted-Team Workflow: Contributing with developer status
  * Setup
    * Clone: `git clone CLONE_URL`
  * Work on an issue
    * Claim issue
    * Synchronize local master with origin's
      ```
      get checkout master
      git pull origin master
      ```
    * Create feature branch
      ```
      git checkout master
      git branch FEATURE_BRANCH
      git checkout FEATURE_BRANCH
      ```
    * Publish feature branch
      ```
      git checkout FEATURE_BRANCH
      git push -u origin FEATURE_BRANCH
      ```
    * Create WIP merge request (on GitLab)
    * Make, stage, commit, and push changes
      ```
      git checkout FEATURE_BRANCH
      ### make some changes, and then ...
      git add .
      git commit
      git push origin FEATURE_BRANCH
      ```
    * Remove WIP from merge request and request a review (on GitLab)
    * Update feature branch with changes in master
      ```
      git checkout master
      git pull origin master
      git checkout FEATURE_BRANCH
      git merge master
      ### test and resolve any conflicts; stage and commit them; and then ...
      git push origin FEATURE_BRANCH
      ```
  * Clean up
    * Synchronize local master
      ```
      git checkout master
      git pull origin master
      ```
    * Delete feature branch in local and remote, and references to remote feature branch.
      ```
      git checkout master
      git branch -D FEATURE_BRANCH
      git push origin --delete FEATURE_BRANCH
      git pull --prune
      ```

## Unit Testing with JUnit 5 and Gradle

* Running tests with Gradle: `./gradlew test --info`
* Where tests are placed: `src/test/java/PACKAGE/...`
* Where code are placed: `src/main/java/PACKAGE/...`
* Both source and tests should be in the same java package: `package what.ever;`
* Annotations
  * `@Test`
  * `@BeforeEach`
  * `@BeforeAll`
  * `@AfterEach`
  * `@AfterAll`
* Each test method is ran in a separate instance of the test class; therefore, tests do not share instance variables.
* `assertEquals(EXPECTED, ACTUAL)`
* `assertTrue(ARBITRARY_CONDITION)`

## Unit Testing - Principles, Practices, and Patterns

### Ch1 Questions

1. What is the goal of unit testing?
2. What does code coverage metrics tell us about tests?
3. What does the testability of code tell us about the code?

### Ch2 Questions

1. What is the definition of a unit test from the London/mockist) point of view?
2. What is the definition of a unit test from the Detroit/classical point of view?
3. What is a shared dependency? Give an example of one.
4. What is a private dependency? Give an example of one.
5. What is an out-of-process dependency? Give an example of one.
6. Explain how a database (which is usually a shared, out-of-process dependency)
  can be made to be a private, out-of-process dependency.
7. Which dependencies are replaced with test-doubles in the classical school of unit testing?
8. Which dependencies are replaced with test-doubles in the London school of unit testing?

### Ch3 Questions

1. What is the AAA pattern and how does it relate to GWT (Given-When-Then)?
2. When should you use GWT and when should you use AAA?
3. How many of each of the AAA sections should a unit test typically have?
4. Which AAA section is typically the largest?
5. How big should the act section typically be?
6. How many asserts statements in the assert section should a unit test typically have?
7. How should unit tests be named?

### Ch4 Questions

1. What are the four pillars of a good unit test?
2. What is a regression?
3. In general, how do we write tests that maximize protection against regressions?
4. What is refactoring?
5. What is meant by "resistance to refactoring"?
6. What is a false positive?
7. What is a false negative?
8. "Protection against regressions" helps address which types of errors:
  false positives or false negatives? Explain.
9. "Resistance to refactoring" helps address which types of errors:
  false positives or false negatives? Explain.
10. What contributes to a test's maintainability?
11. Which two pillars will you "max-out", that is they are non-negotiable?
12. Which two pillars must you strike a balance between?
13. Explain how white-box and black-box testing should be used with unit
  testing?

### Ch5 Questions

1. How is a mock different than a stub?
2. Why do using mocks make your tests more fragile and less resistant to refactoring.
3. Explain how to distinguish between "observable behavior" and "implementation detail".

### Ch6 Questions

1. What is output-based testing, and why is it preferred over other styles for unit tests.
2. What is state-based testing, and why is it worse than output-based for unit tests.
3. What is communication-based testing, and why is it the worst of the three
  styles for use in unit testing. When must you use it?

### Ch7 Questions

1. What are the characteristics of each of the following categories of code:
    * Trivial code
    * Domain model, algorithms
    * Controllers
    * Overcomplicated code
2. What kind of tests are used to focus on domain models and algorithms?
3. What kind of tests are used to test trivial code?
4. Why will tests that test overcomplicated code be less valuable? Consider each of the 4 pillars in your answer.
5. What kind of tests are used to test controllers?

### Ch8 Questions

1. What quadrant of code should integration tests target?
2. What is the longest happy-path?
3. Besides the longest happy-path, what other integration tests should you add?
4. What should be mocked in an integration test?
5. What should not be mocked in an integration test?
6. What is the purpose of, when would you use, end-to-end testing?
7. Why are integration tests better at protecting against regressions than unit tests?
8. Why are integration tests less resistant to refactoring than unit tests?
9. Why are integration tests likely slower than unit tests?
10. Why are integration tests harder to maintain than unit tests?
