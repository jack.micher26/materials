# Homework 5

## Goals

Gain experience with Clean Code.

## Part 1 - Select Code for Review

Find some non-trivial code that you wrote in a previous course (data structures?) that you are willing to let others read and review.

- It doesn't have to be pretty. In fact, it's better if it is a bit rough.
- Ideally it runs and "works", but may have bugs.
- The language it's written in doesn't matter; as long as it is a standard high-level, 3rd generation programming language --- for example: Java, C, C++, Python, Javascript, etc.
- Preferably code that you wrote. If you didn't write it, each file must have the author(s) in comments at the top.
- Preferably more than one file.
- Preferably with at least one method that is more than 10 lines long.

Comment on this issue and briefly identify the program you intend to share. Provide some rough statistics about it:

- What does it do?
- What language is it in?
- How many files does it have?
- Roughly, how many lines of code does it have (counting whitespace and comments)?

***If you can't do the above, let me know ASAP.***

## Part 2 - Post code

1. Create a project in <https://gitlab.com/wne-csit/cs-220/spring-2020/class/homework5>
   Use your __last name__ as the project name.
2. Clone it.
3. Copy the code for the program you chose into your clone.
4. Stage it, commit it, and push it. (If you can't push it, delete the protected branch status on master within GitLab in your project's settings.)

## Part 3 - Review 1 other and yourself

Everyone will review themselves and one other person. The person you will review (besides yourself) will be listed below.

TBD

Each review will review the others code in terms of chapters 2, 3, and 4 of Clean Code. Use the template below. Try to identify three types of issues from each chapter.

### Review

* Code author:
* Reviewer:
* Date:

#### Chapter 2

Incident 1
* File(s):
* Line(s):
* Related section of Clean Code:
* Description of the problem:
* Suggestions for fixing the problem:

#### Chapter 3

...

#### Chapter 4

...


### Submission

Place your review in a file named your-lastname.md in the root of their project.

Place your review of your own code in a file named your-lastname.md in the root of your project.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
