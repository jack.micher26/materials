# Homework 3

*Version CS-220-Spring-2020-1.0*

## Goals

Practice...

* Working with feature branches
* Working with forks
* Working with issues and merge requests
* Writing good commit messages
* Working with remotes (especially an upstream remote)
* Synchronizing local `master` with upstream's `master`
* Updating feature branch with changes in upstream
* Resolving conflicts
* Working with a maintainer to get a merge-request merged

## Instructions

* Project URL: <https://gitlab.com/wne-csit/cs-220/spring-2020/class/yomb>
* Misspellings: 163,255

The text in the above project has several misspellings. Each misspelling is an
anagram of the original word. Help find and correct them.

1. Use the project's issue tracker to report 10 misspellings.
   To receive credit, the following must be true.
  * You must be logged in, so that the issue records you as its reporter.
  * The report must provide the line number of the misspelled word and
    the misspelled word.
  * Only one misspelling may be reported per issue. If an issue contains multiple
    reports, the entire issue will be closed as invalid and you will not receive
    credit for any report.
  * Only the first person to report a misspelled word will receive
    credit for reporting it. Others will be closed as a duplicate.
2. Get 3 merge requests merged by the maintainer. To receive credit, the
    following must be true.
  * Use a fork.
  * Use a clone.
  * Use a feature branch for each fix.
  * Each feature branch must be created from master (not from another feature
    branch).
  * Git must recognize you as the contributor of the commit (so be sure to
    configure git properly).
  * Commit messages must comply with <https://chris.beams.io/posts/git-commit/>
  * The body of commit messages must include the issue number that it closes.
    For example "Closes #16" will close issue number 16 when the commit is
    merged.
  * You must get the maintainer to merge your merge-request.
    Notify them using @NAME. Resolve any issue they raise.
    In particular, you may be asked to update your feature branch with
    new changes in master and resolve any conflicts.
    (You will be asked to do this at least once for one of your merge-requests.)

---

<!--
## Instructor notes

The data file is created by running https://github.com/StoneyJackson/scramblewords.py on an long text from https://www.gutenberg.org/ (e.g., Moby Dick). For example:

```
scramblewords.py --probability .25 --max-word-length 12 --min-word-length 7 < pg2489.txt > pg2489_mistakes.txt
```
-->

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
