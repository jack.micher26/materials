# Homework 6

## Goals

Gain experience with TDD.

See <https://gitlab.com/wne-csit/cs-220/homework6>

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
