# Homework 4

*Version CS-220-Spring-2020-1.0*

## Goals

Practice creating automated tests.

## Instructions

See https://gitlab.com/wne-csit/cs-220/homework4

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
