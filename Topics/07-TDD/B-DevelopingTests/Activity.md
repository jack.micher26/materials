# Developing Tests

## Roles

* Manager:
* Recorder:
* Skeptic:
* Quality Control:

## Start with a problem statement

Running Example: Sort non-decreasing

```
Write a sort method that takes a list and returns a new version of the
list with its elements in non-decreasing order.
```

Your Problem: To FizzBuzz

```
Write a method that takes a positive integer and returns a string. That string
either contains the given number as a decimal encoded string, "Fizz" if that number
is divisible by 3, "Buzz" if that number is divisible by 5, or "FizzBuzz" if that
number is divisible by both 3 and 5.
```

## 1. Write a list of facts about a correct solution to the problem.

* State facts in complete sentences.
* State facts as concisely as possible.
* Ensure that each fact only addresses a single fact.
* Define a correct solution completely so that an incorrect solution would not
  be accepted as a correct solution based on your list of characteristics.
* Minimize the number of facts in your list.


Running Example: Sort non-decreasing

```
1. The number of elements in the input and output are the same.
2. Element frequencies in the input and output lists are the same.
3. Elements in the output must appear in non-decreasing order.
4. The input list and its elements are not modified.
```

Your Problem: To FizzBuzz

```
YOUR STATEMENTS HERE
```

## 2. Write examples using Given-When-Then (GWT) format

For each fact, write one or more examples using
[GWT format](https://martinfowler.com/bliki/GivenWhenThen.html).


Running Example: Sort non-decreasing

```
Title: Sort non-decreasing

Scenario 1: The number of elements in the input and output are the same.

  GIVEN I have a list with 4 elements,
  WHEN I call sort_nondecreasing passing the list,
  THEN The returned list also has 4 elements.

Scenario 2: Element frequencies in the input and output lists are the same.

  GIVEN A the list [2, 2, 3, 3, 3, 1],
  WHEN I call sort_nondecreasing passing the list,
  THEN 1 appears once, 2 appears twice, 3 appears three times in the returned list.

Scenario 3: Elements in the output must appear in non-decreasing order.

  GIVEN A list,
  WHEN I call sort_nondecreasing passing the list,
  THEN The i_th element is less than or equal to the i+1_th element.

Scenario 4: The input list and its elements are not modified.

  GIVEN The list [3, 2, 1]
  WHEN I call sort_nondecreasing passing the list,
  THEN Then list's elements are still [3, 2, 1].
```

Your Problem: To FizzBuzz

```
YOUR STATEMENTS HERE
```



---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
