# JUnit Basics

## Roles

* Driver: (Code, share your screen!)
* Manager:
* Recorder: (Read questions and record answers in team pad)
* Researcher: (Share your screen; find answers in documentation)


## 1. Demo Starter Project

Demo and Tour of junit5-starter repository:
https://gitlab.com/wne-csit-jackson/cs220/spring2021/junit5-starter

See the README.md file in that project for the basics of Gradle and the
structure of the project.


## 2. Setup

1. Clone this project https://gitlab.com/wne-csit-jackson/cs220/spring2021/junit5-starter, and prepare your development environment to work in your clone.
2. Switch to the `sort-fizzbuzz-activity` branch.
3. Run gradle check continuously with extra information displayed.
   There are three scripts to help you `gradeld` for linux or MacOS,
   `gradled.bat` for Windows Command Prompt (cmd), and `gradled.ps1`
   for Windows PowerShell. These scripts use Docker, so be sure it's
   installed and running. If you get permission errors under
   PowerShell you probably need to configure PowerShell to allow
   it to run scripts.
   See https://adamtheautomator.com/run-powershell-script/ .

    Linux or MacOS

        ./gradled --continuous check --info

    Windows Command Prompt (cmd.exe)

        gradled.bat --continuous check --info

    PowerShell

        .\gradled.ps1 --continuous check --info


## 3. Sort Example

Read the following files:

* `src/test/java/cs220/sort/SortTest_Annotated.java`
* `src/test/java/cs220/sort/SortTest.java`
* `src/main/java/cs220/sort/Sort.java`

Answer the following questions:

1. What do you think is the purpose of `@Test`? Test you
   hypothesis. Very briefly explain what you did, your result,
   and your conclusion. (Hint: use the output of JUnit.)

6. Where to the names of the test methods come from?

5. Does each part of GIVEN, WHEN, and THEN always translate to
   a single instruction?

2. What assert methods are used in SortTest_Annotated.java?

3. Give a link to the documentation for each assert method in the
   answer to the last question.

4. Name 3 other assert methods that come with JUnit.

5. Figure out how to tell JUnit to skip a test. Try it, to make
   sure you can. Briefly explain how.

6. What are a couple differences between SortTest_Annotated.java
   and Sort.java?

7. What outstanding questions does your team have?


## 4. Complete FizzBuzz


Following the sort example, translate your fizzbuzz GWT tests into
JUnit tests and Java implementation. See the following starter
files in the same project.

* src/test/java/cs220/fizzbuzz/FizzBuzzTest.java
* src/main/java/cs220/fizzbuzz/FizzBuzz.java

Skip scenarios that involve checking for exceptions... for now.

## 5. Individual Work

Have each team member reproduce at least some of the above work.
Ensure they know how to work with their development environment,
run and monitor the tests, write basic tests, and write code.
