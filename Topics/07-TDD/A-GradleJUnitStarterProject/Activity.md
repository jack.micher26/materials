# Gradle and JUnit Starter Project

## Roles

* Manager
* Recorder
* Troubleshooter
* Efficiency Expert

## Demo

Your instructor will demonstrate and discuss the contents of
https://gitlab.com/wne-csit-jackson/cs220/spring2021/junit5-starter .

Take notes and ask questions.

## Try it

In your teams.

1. Clone https://gitlab.com/wne-csit-jackson/cs220/spring2021/junit5-starter
2. Try: ./gradled --continuous check --info
3. Find and modify A.java and or ATest.java and observe gradle's output

If you run into a problem, see if your team can help you fix it. If your
team is stuck, ask your instructor.

Note any questions or problems that your team still has.
