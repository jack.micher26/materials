# Fork-Branch-Workflow: Clean Up

## 1. Roles

* Manager:
* Reader:
* Driver:
* Recorder:

Working as a team

- Have your manager track time, ensure all have a voice and participate, and ensure the team is working together effectively.
- Have your reader read out each section.
- Have your driver work through each step.
- Have your recorder record commands, concepts, and questions.

## 2. Overview

Last time, we created a merge-request and were awaiting Bat to review and merge our work. Here is the state of the system while we wait for Bat's review.


```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> feature
    feature --> 4
    4 -> 3
    3 -> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_main --> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
f_feature .> u_main : merge request
```

## 3. Ant Merges

Let's suppose Bat accepts and merges our merge-request. Then our system looks like this.

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> feature
    feature --> 4
    4 -> 3
    3 -> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "3" as u_3
    circle "4" as u_4
    u_2 -> u_1
    u_3 -> u_2
    u_4 -> u_3
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_main --> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
```

Questions:

1. Does upstream contain the new commits?
2. Does main in our clone and fork point to the same commit as main in upstream?
3. Do all the repositories have the same branches?
4. Is the find-apples branch still needed?
5. Is your clone and fork in sync with upstream?

## 4. Clean up plan

To get your fork and clone in sync with upstream you need to

* Refresh main with changes in upstream.
* Delete the unnecessary find-apples branch.

Order matters, since you don't want to delete your branch until you know main contains its changes. So let's start by refreshing main.


## 5. Refresh main

1. You refreshed main in your clone and fork just before you create your feature branch when you started the process of offering a change. Write the steps for refreshing main below.
2. Execute these steps.
3. Confirm they worked as expected.

If all went well, your master branches should point to the same commit as upstream, as shown in the diagram below.

```plantuml
@startuml
database clone {
    card "HEAD" as HEAD
    card "main" as main
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> main
    main --> 4
    feature --> 4
    4 -> 3
    3 -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "3" as u_3
    circle "4" as u_4
    u_2 -> u_1
    u_3 -> u_2
    u_4 -> u_3
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_4
    f_main --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
```

## 6. Delete the merged branch

There are actually three branches that need to be deleted:

* find-apples in your clone
* find-apples in your fork
* and origin/find-apples in your CLONE that tracks the location of find-apples in your fork (origin).

> :information_source: **Note**
>
> You've probably seen `origin/find-apples` before when you ran
>
> ```bash
> git log --all --oneline --graph
> ```
>
> You can also get a list of all branches, including remote branches by providing `-a` to `git branch`.
>
> ```bash
> git branch -a
> ```

So the current state of your system before deleting any branches, and including the remote tracking branch origin/find-apples, looks something like this.

```plantuml
@startuml
database clone {
    card "HEAD" as HEAD
    card "main" as main
    card "find-apples" as feature
    card "origin/find-apples" as r_feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> main
    main --> 4
    feature --> 4
    r_feature --> 4
    4 -> 3
    3 -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "3" as u_3
    circle "4" as u_4
    u_2 -> u_1
    u_3 -> u_2
    u_4 -> u_3
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_4
    f_main --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
```



### 6.1. Delete find-apples in your clone

```bash
git branch -d find-apples
```

> :information_source: **Note**
>
> `git branch -d BRANCH_NAME`  will only delete the branch if it has been merged into main. So it is a fairly safe operation.

> :warning: **Warning**
>
> If you ever need to delete a branch that has not bee merged into main, you can use a capital D instead: `git branch -D BRANCH_NAME`. Be sure you don't need branch or the commits unique to that branch as they will eventually be garbage collected by Git, and you will lose them.

Your system should now look something like this.

```plantuml
@startuml
database clone {
    card "HEAD" as HEAD
    card "main" as main
    card "origin/find-apples" as r_feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> main
    main --> 4
    r_feature --> 4
    4 -> 3
    3 -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "3" as u_3
    circle "4" as u_4
    u_2 -> u_1
    u_3 -> u_2
    u_4 -> u_3
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_4
    f_main --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
```


### 6.2. Delete find-apples in your fork

```bash
git push origin --delete find-apples
```

> :information_source: **Note**
>
> Perhaps surprisingly, deleting a branch in the remote uses the `push` subcommand instead of the `branch` subcommand!


```plantuml
@startuml
database clone {
    card "HEAD" as HEAD
    card "main" as main
    card "origin/find-apples" as r_feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> main
    main --> 4
    r_feature --> 4
    4 -> 3
    3 -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "3" as u_3
    circle "4" as u_4
    u_2 -> u_1
    u_3 -> u_2
    u_4 -> u_3
    u_main -> u_4
}
database fork {
    card "main" as f_main
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_main --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
```

### 6.3. Delete origin/find-apples in your clone


```bash
git remote prune origin
```

> :information_source: **Note**
>
> You don't really delete a remote tracking branch reference. Instead you tell git to compare its remote-tracking branches with the actual branches in a remote and then **`prune`** those remote-tracking branches that no longer exist in the remote. That's why it's part of a `pull` operation.


And now your system should look like this.

```plantuml
@startuml
database clone {
    card "HEAD" as HEAD
    card "main" as main
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> main
    main --> 4
    4 -> 3
    3 -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "3" as u_3
    circle "4" as u_4
    u_2 -> u_1
    u_3 -> u_2
    u_4 -> u_3
    u_main -> u_4
}
database fork {
    card "main" as f_main
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_main --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
```

## Summary

Now your fork and clone are now in sync with upstream.

1. Write a short summary of commands you need to run to clean up after your merge-request was merged.

2. List questions that your team has.

---
Copyright 2021, Stoney Jackson <dr.stoney@gmail.com>
Licensed under CC-BY-SA 4.0 International.
