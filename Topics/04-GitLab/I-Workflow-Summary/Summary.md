# Fork-Branch-Workflow Summary

Below each step, write the git commands to complete the step (or how to complete the step), and write other important notes and reminders.

## Setup

When first starting to work on a project...

1. Fork.
2. Clone.
3. Create the upstream remote.

## Offer Changes

When you want to offer a new feature, or a bug fix, or an improvement...

1. Refresh main.
2. Create a feature branch.
3. Work on the feature branch.
4. Publish the feature branch to origin (fork).

## Continue Work

When asked to make changes...

1. Work on the feature branch.
2. Push changes to origin (fork).

## Sync with Upstream

When asked to sync stale feature branch with changes in upstream...

1. Refresh main.
2. Merge main into feature branch, handling lexical and logical conflicts.
3. Push changes to origin (Fork).

## Clean Up

When MR is accepted and merged...

1. Refresh main.
2. Delete merged branches (local, remote, remote-tracking).

## Tear Down

When you no longer plan to work on the project (or you want to start over)...

1. Delete your clone.
2. Delete your fork.
