# Workflow

## Round 1

In this first round, you'll **setup**, **offer a change**, it will be merged, and you'll **clean up**.

Individually... (but help team members if they get stuck)

1. Setup to work on your team's project (find your team project here: https://gitlab.com/wne-csit-jackson/cs220/spring2021/class).

2. Offer a change that contains a single file whose name is your @username, but DO NOT include the @ in the filename.

3. In a comment on your MR, ask a team member to review your MR. Mention them using their GitLab @username.

4. Your teammate will inspect your MR and merge it if it looks reasonable.

5. Clean up after your request is merged.

As a team...

6. Confirm your commit is in main in your fork, your clone, and in upstream.

7. Confirm that your merged feature branch has been deleted.

## Round 2

In this round you'll offer another change, you will make an additional change, your MR will be merged, and you will clean up.

1. Offer a new change, adding the title of your favorite book or movie to your file.

2. Request one of your team members to inspect your MR by mentioning them in a comment on the MR.

3. Your teammate will leave a comment asking you to "Please include your favorite food."

4. Update your MR by adding your favorite food or dessert to your file.

5. Again, through your MR, ask your teammate to review your MR.

6. Your teammate will inspect the MR and if it is correct (it should have two commits -- one with a favorite food and one with a favorite book or movie), merge it.

7. Clean up and confirm your commits are in all repositories main and your feature branch has been deleted.

## Round 3

In this round the reviewer will request you to update your changes with new changes in upstream.

1. Offer a new change: add your favorite holiday to your file.

2. Let your instructor know when all of your team has created a new MR, and wait until they tell you to proceed before continuing. Your instructor will add a commit to upstream.

3. Use your MR to request a teammate to review your MR.

4. Your teammate will notice that there have been new commits in upstream's main since this MR was opened (where on the MR do you see this?), and they will ask you to synchronize your feature branch with these new changes.

5. Synchronize your feature branch with changes in upstream.

6. Request a teammate to review your MR again.

7. Your teammate will merge your MR if they see that it has been updated.

8. Clean up and confirm success.


## Round 4

Same as round 3, but this time you're going to have to resolve a merge conflict.

1. Offer a new change: create a file named AUTHORS.txt (be exact here). Put your GitLab @username on the first line of this file.

2. Use your MR to request a teammate to review your MR.

3. Your teammate will notice that the MR cannot be merged, and will ask you to synchronize your feature branch with these new changes.

4. Synchronize your feature branch with changes in upstream, resolving the conflict(s) as needed.

5. Request a teammate to review your MR again. NOTE, you and your teammate may need to repeat steps 3 and 4 until the MR is mergeable.

6. Your teammate will merge your MR if they see that it has been updated.

7. Clean up and confirm success.
