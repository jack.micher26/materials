# Fork-Branch-Workflow: Setup

## Roles

* Manager:
* Driver:
* Recorder:
* Researcher:

## 1. Context

Bat has created a project named FruitFinder and has made it public on GitLab. You would like to help improve FruitFinder, and plan to contribute many changes to it.

```plantuml
@startuml
cloud "GitLab" {
    folder Bat {
        database "FruitFinder" as upstream
    }
}
@enduml
```

For this activity, use this project: https://gitlab.com/wne-csit-jackson/cs220/spring2021/fruitfinder


## 2. Setup

Before you can contribute changes, there is some setup that needs to be done. You'll need to:

1. Fork Bat's FruitFinder.
2. Clone your fork.
3. Create an upstream remote.

This section describes each of these steps and their purpose.

### 2.1. Fork

You cannot modify Bat's copy of FruitFinder, because you don't own it. The first thing you need to do is get a copy of FruitFinder that you can modify.

You ***fork*** it.

Forking FruitFinder creates a copy of it under your namespace on GitLab.

```plantuml
@startuml
cloud "GitLab" {
    folder Bat {
        database "FruitFinder" as upstream
    }
    folder You {
        database "FruitFinder" as origin
    }
}
upstream -> origin :fork
@enduml
```

When you fork FruitFinder, GitLab also creates a ***fork relationship*** that points from your copy to Bat's on GitLab. This relationship makes it easy to return to Bat's copy of FruitFinder from your copy, and it will make it easy for you to offer changes to Bat later. For now, just be aware that GitLab creates and maintains this relationship for you when you fork a project.

```plantuml
@startuml
cloud "GitLab" {
    folder Bat {
        database "FruitFinder" as upstream
    }
    folder You {
        database "FruitFinder" as origin
    }
}
upstream <. origin: fork relationship
@enduml
```

> **Try it**
>
> 1. Find direction for forking project on GitLab. Paste a link to them here.
> 2. Fork the project into your namespace on GitLab.
> 3. When you are done, you will be looking at your fork. Find two indications that this is your fork and not the original. List them here.

### 2.2. Clone

Although you can modify your copy of FruitFinder directly on GitLab, you are not able to access all of your development tools that you need to do a good job. Those tools are installed on your personal computer. You need to get a copy of FruitFinder on your personal computer to take advantage of your development tools.

You ***clone*** your fork.

> **Careful**
>
> You were very careful to clone your fork and ***NOT*** Bat's copy of FruitFinder. GitLab will allow you to clone a public project that you don't own. But you won't be able to push your changes to it later.

Cloning your fork of FruitFinder makes a copy of it on your local machine.

```plantuml
@startuml
cloud "GitLab" {
    folder Bat {
        database "FruitFinder" as upstream
    }
    folder You {
        database "FruitFinder" as origin
    }
}
folder "Your Computer" {
    database "FruitFinder" as clone
}
origin --> clone: clone
@enduml
```

When you cloned your fork of FruitFinder, Git created a ***remote*** that points back to your fork from your local clone. Git names this remote ***origin***. Later, you'll use origin to push change from your clone up to your fork.

```plantuml
@startuml
cloud "GitLab" {
    folder Bat {
        database "FruitFinder" as upstream
    }
    folder You {
        database "FruitFinder" as origin
    }
}
folder "Your Computer" {
    database "FruitFinder" as clone
}
origin <.. clone: origin
upstream <. origin: fork relationship
@enduml
```


> **Try it**
>
> 1. Find directions for cloning a GitLab project. Paste a link to them here.
> 2. Clone ***your fork*** of FruitFinder. ***NOT*** the original upstream project.
> 3. Use `git remote -v` to confirm that the *origin* remote points to ***your fork***. Paste the output below.


### 2.3. Create Upstream Remote

As Bat and others update FruitFinder, new changes will begin to appear in Bat's copy. You will need to a way to update your copies with those changes.

GitLab does provide a "mirroring" mechanism. But you want a technique that will work regardless if you are using GitLab, GitHub, or some other repository hosting service.

So you plan to pull changes from Bat's FruitFinder into your clone, and then push them to your fork. This would require you to type in the URL to Bat's FruitFinder each time you update.

To make your life easier, you decide to create a remote named ***upstream*** that points from your clone to Bat's FruitFinder on GitLab. You call it "upstream" because Bat's FruitFinder is the official copy from which new changes flow to clients and developers alike.

> **Careful**
>
> You were very careful to point upstream to the clone URL for Bat's FruitFinder, and ***NOT*** your fork.


```plantuml
@startuml
cloud "GitLab" {
    folder Bat {
        database "FruitFinder" as upstream
    }
    folder You {
        database "FruitFinder" as origin
    }
}
folder "Your Computer" {
    database "FruitFinder" as clone
}
origin <.. clone: origin
upstream <. origin: fork relationship
clone ..> upstream: upstream
@enduml
```

> **Try it**
>
> 1. Navigate to the original, ***upstream*** project on GitLab. **NOT** your fork.
> 2. Copy the clone URL for the project.
> 3. In your clone, use the following command to add an upstream remote.
>       ```
>       git remote add upstream PASTE-URL
>       ```
> 4. Use `git remote -v` to confirm that *origin* and *upstream* point to the correct repositories. Paste the output here.

### 2.4. Setup Summary

You have completed your setup. To summarize, you did the following:

1. Forked the project
2. Cloned your fork
3. Created an upstream remote

> **Review**
>
> 1. List the git commands you used in this activity and briefly explain the purpose of each.
> 2. Write any questions that you have at this point.

---
Copyright 2021, Stoney Jackson <dr.stoney@gmail.com>
Licensed under CC-BY-SA 4.0 international
