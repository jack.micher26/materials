# Part 4: Logical Conflicts

## Roles

* Manager
* Driver
* Recorder
* Skeptic


## Instructions


1. Part4Project.zip contains a git repository. Unzip it. Explore the repository in Part4Project as you did in Part1. Summarize your findings.

2. Ensure that the code in each branch compiles. (e.g., `rm *.class; javac *.java`)

3. Merge non-master branch into `master`.

4. Was git able to merge the branches?

5. Does the merged code compile? Do you foresee any problems?

6. From a developer point of view was the merger ***really*** successful?

7. Define a "logical merge conflict".

8. In a larger project, do you think you will easily be able to find such problems by inspection?

9. How might help you be able to identify such problems?

10. Fill in the blank: After you merge you should always __________.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
