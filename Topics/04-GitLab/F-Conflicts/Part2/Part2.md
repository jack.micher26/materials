# Part 2: Lexical Conflicts


## Roles

* Manager
* Driver
* Recorder
* Skeptic


## Model 1: Orientation (5 min)

DO NOT RUN THESE COMMANDS.

```bash
$ git branch
feature-bar
* master

$ git merge feature-bar
Auto-merging A.java
CONFLICT (content): Merge conflict in A.java
Automatic merge failed; fix conflicts and then commit the result.
```

### Model 1 Questions

1. Which branch is the active branch?

2. Which branch is merging into the active branch?

3. Which branch should be updated as a result of the merge?

4. Did the merge succeed?

5. What file has a conflict?

6. What are we supposed to do now?

7. Do you think a new commit has been created?

## Model 2: Conflicted Status (5 min)

DO NOT RUN THESE COMMANDS.

```bash
$ git status
On branch master
You have unmerged paths.
(fix conflicts and run "git commit")
(use "git merge --abort" to abort the merge)

Changes to be committed:

    modified:   B.java

Unmerged paths:
(use "git add <file>..." to mark resolution)

    both modified:   A.java
```

### Model 2 Questions

1. What command do we use to learn more about which files have conflicts?

2. Does B.java have conflicts? What tells you that?

3. Does A.java have conflicts? What tells you that?

4. Is the merge complete? What tells you that?

5. If you want to give up on the merge, what do you do?

6. What do you need to do to resolve the conflict and complete the merge?

## Model 3: A Conflict

DO NOT RUN THESE COMMANDS.

```java
$ cat A.java
public class A {

    public void foo() {
<<<<<<< HEAD
        B b = new B();
        b.bar();
=======
        System.out.println("Hi");
>>>>>>> feature-bar
    }

}
```

1. Is `<<<<<<< HEAD` standard Java syntax?

2. Is `=======` standard Java syntax?

3. Is `>>>>>>> feature-bar` standard Java syntax?

4. What reasons might you have that the above syntax has something to do with git?

5. What conflicted code do you think comes from the `feature-bar` branch? How do you know?

6. What conflicted code comes from the `master` branch (remember it's the branch we are merging ***into***)? How do you know?

7. Git does not know how to program in Java (or any language). It is now your job to decide what the new version of the file should look like. There are infinite possible answers. Based on the conflict, update the code below to resolve the conflict. Be prepared to explain your decisions.

    ```java
    public class A {

        public void foo() {
    <<<<<<< HEAD
            B b = new B();
            b.bar();
    =======
            System.out.println("Hi");
    >>>>>>> feature-bar
        }

    }
    ```

8. Assuming you make the above changes and save the file, what commands do you issue to finish the merge? (Hint: review Model 2.)

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
