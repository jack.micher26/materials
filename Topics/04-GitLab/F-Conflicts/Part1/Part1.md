# Conflicts Part 1: No Conflicts

## Roles

* Manager
* Driver
* Recorder
* Skeptic


## Instructions

1. Part1Project.zip contains a git repository. Unzip it.

2. Use `git branch` to list the branches in this repository.
    * What branches exist?
    * Which branch is the active branch (i.e., which branch is checked out into the working tree? i.e., which branch are you on?)?

3. Use `ls` and `cd` as needed to browse around figure out what files and directories exist. List them here.

4. Use `cat` to display file contents.

5. Use `git log --all --graph --online` to explore the structure of commits. Summarize your understanding of the graph structure below. Consider drawing a diagram for yourself.

6. What is the ID of the most recent commit that both branches have in common?

7. Use `git diff COMMIT_ID` (replace COMMIT_ID with the ID of a commit) to view the changes on the current branch since the most recent commit that the branches have in common.
   1. What file was changed?
   2. Was something added or removed?
   3. What was added or removed?

8. Use `git checkout` to checkout the other branch, and repeat steps 3-7 on this branch. Summarize your results here.

9.  Use `git diff BRANCH`, where BRANCH is the name of the branch you are not on, to view the difference between the two branches. Summarize the differences.

10. You plan to merge the non-`master` branch into `master`. Predict the contents of the files in `master` after the merge.

11. Merge non-`master` branch into `master`.

12. Inspect the contents of `master`. Is it what you expected? If not, how were you surprised?

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
