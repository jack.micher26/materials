# GitLab - Quick Start

## Roles

* Manager:
* Driver:
* Recorder:
* Researcher:

## Instructions

Complete this first as a team, helping a single driver through these steps. Then have each of your team members complete these steps individually, helping each other as you go.

As you work through this, if something goes wrong, consult the tips at the end of this document, use Google, ask others on the class Discord channel. Part of being a programmer is problem solving. And the best way to get better at problem solving is practicing.

Make notes of problems and questions as you go, so we can discuss them.

1. Create a free GitLab account. See privacy note below.
2. Sign into GitLab.
3. Create a new project:
   1. Start with a blank project
   2. Name it My First Project
   3. Set the visibility to public
   4. Initialize it with a README file (there is a checkbox)
4. Clone your new project's repository to your local system:
   1. Copy the HTTPS clone URL from your project.
   2. Then in a terminal (e.g., git-bash)
        ```bash
        git clone PASTE-URL
        cd my-first-project
        code .
        ```
5. Edit the README locally and add your favorite book, movie, or animal.
6. Stage, commit, and push your change:
    ```bash
    git add .
    git commit -m "docs(README): declare favorite WHATEVER"
    git push      # this will prompt you for your GitLab username and password
    ```
7. Confirm those changes are now in your project on GitLab.
8. Make, stage, commit, and push a couple more changes to make sure you've got the procedure down and things are working smoothly.
9. Delete your local clone:
    ```bash
    cd ..
    rm -rf my-first-project
    ```
10.  Clone your project's repository again and confirm that it has all the commits you made (`git log`).

## Tips

* :exclamation: **Privacy:** The username and the email you choose to use with git and GitLab may become public. Choose wisely. If you want to remain anonymous, create a non-identifying email, and select an equally non-identifying username.

* Your instructor may require that you tell them your GitLab account name so they can give you permissions in the GitLab group for this class and so they can correctly attribute your work to you.

* :bulb: GitLab uses email for different purposes. It uses an email to send you notifications. It uses an email to allow GitLab and other GitLab users to contact you (without giving them this email). It can supply a public email. You may provide the same or different emails for each of these purposes, and all are used to identify commits that belong to you.

  Navigate to your Emails in your profile settings: click your profile in the upper right, select settings, then select emails from the left menu.

  Make sure that the email you gave to git is one of the emails in this list.

* :boom: Keep getting an authorization error when you try to `git push` and it is not prompting you for your username and password anymore? This happens because a password manager remembers what you typed the first time and keep supplying what you typed each time, even if it is wrong. You need to use your systems password manager to find and update your username or password. On Windows, it's the Credential Manager. On Mac, it's probably Keychain.
    * [Windows Credential Manager](https://support.microsoft.com/en-us/windows/accessing-credential-manager-1b5c916a-6a16-889f-8581-fc16e8165ac0#:~:text=Windows%2010,select%20Credential%20Manager%20Control%20panel.)
    * [Mac Keychain](https://support.apple.com/guide/keychain-access/what-is-keychain-access-kyca1083/mac)

* :boom: When you type your password no stars appear?! This is normal on the command line. When you are asked to type a password, terminals turn off the "echo". That is, they don't repeat your keystrokes to the screen. So, you just have to type your password in confidently and then press enter!

* :boom: If something goes really wrong and you don't know how to correct it, you could start over using a slightly different name for the project, or create different account, etc. Make a note of your old account, project, etc., so we can clean it up later.

---
Copyright 2021 Stoney Jackson <dr.stoney@gmail.com>. Licensed under CC-BY-SA 4.0 international.
