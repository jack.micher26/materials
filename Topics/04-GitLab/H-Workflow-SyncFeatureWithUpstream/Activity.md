# Fork-Branch-Workflow: Sync Feature with Upstream

## 1. Roles

* Manager:
* Reader:
* Driver:
* Recorder:

Working as a team

- Have your manager track time, ensure all have a voice and participate, and ensure the team is working together effectively.
- Have your reader read out each section.
- Have your driver work through each step.
- Have your recorder record commands, concepts, and questions.

## 2. Overview

There are times when Bat may ask you to update your feature branch with new changes in upstream's main branch. This situation is depicted in the diagram below.

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    HEAD --> feature
    feature --> 3
    3 -> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "4" as u_4
    u_2 -> u_1
    u_4 -> u_2
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    f_feature --> f_3
    f_3 -> f_2
    f_main --> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
f_feature .> u_main : merge request
```

Questions:

1. Which commit is in upstream that is not in your clone or your fork?
2. How do you suppose this commit got into main?
3. Relative to your work, in rough terms...
   1. When was this new commit created?
   2. When was this new commit merged into upstream?
4. Why does Ant want you to update your feature branch with the new changes? What is Ant worried about?


## 3. Updating your feature branch

Below is a high level overview of what you need to accomplish. Write a sequence of git commands that you need to perform for each step.

1. Refresh main.
2. Merge main into your feature branch.
3. Push your updated feature branch to your fork.

After step 1 your system should look like this.

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    HEAD --> feature
    feature --> 3
    3 -> 2
    main --> 4
    4 -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "4" as u_4
    u_2 -> u_1
    u_4 -> u_2
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_3
    f_3 -> f_2
    f_main --> f_4
    f_4 -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
f_feature .> u_main : merge request
```

After step 2...

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    circle "5" as 5
    HEAD --> feature
    feature --> 5
    3 -> 2
    main --> 4
    4 -> 2
    2 -> 1
    5 -> 4
    5 -> 3
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "4" as u_4
    u_2 -> u_1
    u_4 -> u_2
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_3
    f_3 -> f_2
    f_main --> f_4
    f_4 -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
f_feature .> u_main : merge request
```

After step 3...

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    circle "5" as 5
    HEAD --> feature
    feature --> 5
    3 -> 2
    main --> 4
    4 -> 2
    2 -> 1
    5 -> 4
    5 -> 3
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    circle "4" as u_4
    u_2 -> u_1
    u_4 -> u_2
    u_main -> u_4
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    circle "5" as f_5
    f_feature --> f_5
    f_3 -> f_2
    f_main --> f_4
    f_4 -> f_2
    f_2 -> f_1
    f_5 -> f_4
    f_5 -> f_3
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
f_feature .> u_main : merge request
```


Questions:

1. What kind of commit is commit 5?
2. What could go wrong when you merge main into the feature branch (step 2)?
3. Why don't you merge the feature branch into main?
4. What should you do before pushing your changes in step 3?

---
Copyright 2021, Stoney Jackson <dr.stoney@gmail.com>
Licensed under CC-BY-SA 4.0 International.
