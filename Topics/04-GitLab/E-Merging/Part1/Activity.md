# Merging Part 1

## Roles

* Manager
* Driver
* Recorder
* Skeptic

## Instructions

Time: 20m

1. part1.zip contains a git repository. Unzip it and open a command-line positioned in the root of the part1 folder.

2. Use `git branch` to list the branches in this repository. What branches exist and which is the active branch?

3. What are the contents of the part1 folder?

4. Use `git log --all --graph --oneling` to the commit graph. Summarize your understanding of the graph structure below.

5. How many commits are there?

6. Run the following command, note its output, and display the commit graph. What has changed and why?

    ```bash
    git merge main
    ```

7. Switch to the branch `foo`, then run the following command, note its output, and display the commit graph. What has changed and why?

   ```bash
   git merge main
   ```

8. Switch to the branch `main`, then run the following command, note its output, and display the commit graph. What has changed and why?

    ```bash
    git merge foo
    ```

9. What files are in part1?

10. How many commits are there now in the commit graph? Compare that to your answer in 5.

11. Look at the output to step 8. What type of merge was performed?

12. Does that type of merge result in new commit?

13. Switch to branch `foo`. What files are in part1?

14. Which branch was modified by step 8?

15. Assume you are working in some other project, and all of the branches named below exist. Which branch will be modified by the following sequence?

    ```
    git switch branch1
    git merge branch2
    ```

16. Complete the following statement. To merge the contents of branch A into branch B, you must first switch to...

17. Use the following command to delete the `foo` branch. You will probably get an error. Read it and figure out what you need to do.

    ```
    git branch -d foo
    ```

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
