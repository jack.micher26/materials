# Merging Part 2

## Roles

* Manager
* Driver
* Recorder
* Skeptic

## Instructions

Time: 10m

1. part2.zip contains a git repository. Unzip it and open a command-line positioned in the root of the part2 folder.

2. What branches exist?

3. List each branch and the files it contains.

4. Describe the commit graph including the number of commits.

5. Merge `foo` into `main` (i.e., `main` should be modified, not `foo`).

6. List each branch and the files it contains.

7. Describe the commit graph including the number of commits.

8. What kind of merge was performed (see output from 5)?

9. What kind of merge creates a merge commit?

10. Which branch contains all the commits?

11. Which branch is theoretically obsolete, because all its commits are subsumed by the other branch?

12. Delete the obsolete branch.

---

13. Delete the part2 folder, unzip part2.zip again, and this time merge `main` into `foo`.

---
Copyright © 2020 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
