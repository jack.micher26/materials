# Offering Changes - With Pictures!

## 1. Roles

...

## 2. Overview

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    circle "1" as 1
    HEAD -> main
    main -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    circle "1" as f_1
    f_main -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
@enduml
```

## 3. Switch to `main`

```
git switch main
```

## 4. Refresh `main`


```
git pull upstream main
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    circle "1" as 1
    circle "2" as 2
    HEAD -> main
    main -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    circle "1" as f_1
    f_main -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
@enduml
```

```bash
git push origin main
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    circle "1" as 1
    circle "2" as 2
    HEAD -> main
    main -> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    circle "1" as f_1
    circle "2" as f_2
    f_main -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
@enduml
```

## 5. Create a feature branch

```bash
git switch -c find-apples
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    HEAD --> feature
    feature --> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    circle "1" as f_1
    circle "2" as f_2
    f_main -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
@enduml
```

## 6. Work on feature branch

```bash
git switch find-apples
edit ...
git add .
git commit -m "feat(apples): where to find apples"
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    HEAD --> feature
    feature --> 3
    3 -> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    circle "1" as f_1
    circle "2" as f_2
    f_main -> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
@enduml
```

## 7. Publish feature branch

```bash
git push -u origin find-apples
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    HEAD --> feature
    feature --> 3
    3 -> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    f_feature --> f_3
    f_3 -> f_2
    f_main --> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
@enduml
```

## 8. Create merge request

Follow URL printed by previous command and submit the request.

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    HEAD --> feature
    feature --> 3
    3 -> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    f_feature --> f_3
    f_3 -> f_2
    f_main --> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
f_feature .> u_main : merge request
@enduml
```

## 9. Update feature branch (and merge request)

```bash
git switch find-apples
edit ...
git add .
git commit -m "feat(peaches): where to find peaches"
git push
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as feature
    circle "1" as 1
    circle "2" as 2
    circle "3" as 3
    circle "4" as 4
    HEAD --> feature
    feature --> 4
    4 -> 3
    3 -> 2
    main --> 2
    2 -> 1
}
database upstream {
    card "main" as u_main
    circle "1" as u_1
    circle "2" as u_2
    u_2 -> u_1
    u_main -> u_2
}
database fork {
    card "main" as f_main
    card "find-apples" as f_feature
    circle "1" as f_1
    circle "2" as f_2
    circle "3" as f_3
    circle "4" as f_4
    f_feature --> f_4
    f_4 -> f_3
    f_3 -> f_2
    f_main --> f_2
    f_2 -> f_1
}
fork .[hidden]> upstream : fork relationship
fork <.[hidden]. clone : origin
upstream <.[hidden]. clone : upstream
f_feature .> u_main : merge request
@enduml
```

## 10. Summary

...

## 11. Practice

...

## :boom: What if you make commits to the wrong branch?

Assumptions:

1. You've made some commits to `main` (f1 and f2), but haven't pushed them.
2. You haven't created your feature branch yet.

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "origin/main" as om
    circle "1" as 1
    circle "f1" as 2
    circle "f2" as 3
    HEAD --> main
    main --> 3
    3 -> 2
    2 -> 1
    om --> 1
}
```

```bash
git branch find-apples
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as f
    card "origin/main" as om
    circle "1" as 1
    circle "f1" as 2
    circle "f2" as 3
    HEAD --> main
    f --> 3
    main --> 3
    3 -> 2
    2 -> 1
    om --> 1
}
```

```bash
git reset origin/main
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as f
    card "origin/main" as om
    circle "1" as 1
    circle "f1" as 2
    circle "f2" as 3
    HEAD --> main
    f --> 3
    main --> 1
    3 -> 2
    2 -> 1
    om --> 1
}
```

```bash
git switch find-apples
```

```plantuml
@startuml
database clone {
    card "main" as main
    card "HEAD" as HEAD
    card "find-apples" as f
    card "origin/main" as om
    circle "1" as 1
    circle "f1" as 2
    circle "f2" as 3
    HEAD --> f
    f --> 3
    main --> 1
    3 -> 2
    2 -> 1
    om --> 1
}
```


---
Copyright 2021, Stoney Jackson <dr.stoney@gmail.com>
Licensed under CC-BY-SA 4.0 International.
