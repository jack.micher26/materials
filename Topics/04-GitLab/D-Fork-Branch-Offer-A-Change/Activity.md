# Fork-Branch-Workflow: Offering Changes

## 1. Roles

* Manager:
* Reader:
* Driver:
* Recorder:

Working as a team

- Have your manager track time, ensure all have a voice and participate, and ensure the team is working together effectively.
- Have your reader read out each section.
- Have your driver work through each step.
- Have your recorder record commands, concepts, and questions.

## 2. Overview

You have setup your fork and clone of FruitFinder. You've read up on branches. And now you are now ready to start offering changes to Bat.

## 3. Switch to `main`

You have a great idea for how to find apples. You want to base your work on a stable copy of FruitFinder. So, you switch to `main`.

```bash
git switch main
```

## 4. Refresh `main`

But you don't want just any stable copy, you want the most recent stable copy. New commits may have been added to main since you last worked with it.

```bash
git pull upstream main
```

So you pull any changes from upstream into your clone.

```
git pull upstream main
```

While you are updating things, you decide to update your main branch in your fork by pushing your now up-to-date main branch to your fork.

```bash
git push origin main
```

## 5. Create a feature branch

Now that you have the most recent, stable, copy of the project, you are ready to get to work.

***But wait!*** If you start making commits to `main`, you would add commits that have not been vetted and approved by Bat. That would mean that `main` would no longer be the official, stable copy of the project.

So, you create a new branch based on `main` to hold your new idea for finding apples, and switch to it at the same time.

```bash
git switch -c find-apples
```

> **Keys to Success in Fork-Branch-Workflow**
>
> 1. Never commit to `main`.
> 2. Always refresh `main` before starting new work.
> 3. Always create feature branches from `main`.
> 4. Create a new feature branch for each contribution you want to make.
>
> Exception to 3: If you are fixing a bug in a specific branch, commit, or release, create your feature branch based on that branch, commit, or release.

## 6. Work on feature branch

***Now*** you are ready to start working. So you get working.

In the statements below, replace GITLAB_USERNAME with your username on GitLab.

> **Tip**
>
> Get in the habit of checking that you are on the right branch before you start making commits. If your prompt shows you which branch you are on, quickly glance at it. If not, you could `git switch` to the correct branch. If you are already on the branch, Git will just say so; no harm done.

```bash
git switch find-apples
echo "I know where to find apples." >> YOUR_NAME.txt
git add .
git commit -m "feat(apples): where to find apples"
```

## 7. Publish your "feature" branch

You are ready to offer your change to Bat.

But you can't push your change to Bat's copy of FruitFinder (upstream), because you don't own it. However, if you can make your changes available to Bat, then Bat can merge your changes into its copy.

So, you use your fork to publish your changes so Bat can access them.

Publish your new branch to your fork.

```bash
git push -u origin find-apples
```

That `-u` tells git to remember that find-apples in your clone maps to the find-apples in your fork.

## 8. Submit a merge-request (MR)

That last command printed a URL for creating a new MR. It will let you place a request to Bat to merge changes in your new branch into its repository.

Navigate to the URL and create the MR, accepting the defaults. Notice that the MR is created on Bat's project, not on your fork.

A MR starts a discussion between you and Bat. Bat will review your changes and then do one of the following:

1. Close the MR without merging it (rejecting).
2. Merge the MR as is (accepting).
3. Request that you make some adjustments.

## 9. Make adjustments as needed

If you are asked to make adjustments, add commits to the same branch and push them to your fork, which will automatically update the MR. That's because the MR is tracking your branch.

For example, make the following adjustment.

```bash
git switch find-apples
echo "I also know where to find peaches." >> YOUR_NAME.txt
git add .
git commit -m "feat(peaches): where to find peaches"
git push
```

> **Note**
>
> The simple `git push` is possible because you specified `-u` to `git push -u origin find-apples`. If you didn't, you would need to be more explicit now: `git push origin find-apples`, which is error prone.

Now confirm that your MR includes the new commit.

## 10. Summary

To offer changes in a Fork-Branch-Workflow...

1. Synchronize main
2. Create a feature branch
3. Work on the feature branch
4. Publish the feature branch
5. Submit a merge-request

> **Tip**
>
> Occasionally, you may need to create your feature branch based on a commit other than the most recent commit of main. For example, if a bug is found in a previous release, you would first checkout that release, and then create your feature branch based on that commit.

## 11. Practice

You decide it would be better to have separate files for fruits, and to store all your directions to fruits in a folder whose name is your GITLAB_USERNAME.

./GITLAB_USERNAME/apples.txt
./GILTAB_USERNAME/peaches.txt

Remember to start your new idea in a new feature branch starting from the most recent copy of `main`.

Once you have a new MR, return to your previous MR and close it. You like your new idea better.

## 12. :boom: What if you make commits to the wrong branch?

Assumptions:

1. You've made some commits to `main`, but haven't pushed them.
2. You haven't created your feature branch yet.

First, we need to create a feature branch that has the new commits.

```bash
git branch find-apples
```

We didn't switch to the new branch because we now need to move `main` to where it was before the new commits. Since you haven't pushed `main` since making the new commits, we can use `origin/main` because it points to the commit that `main` points to in `origin` (your fork). So we move `main`, which is the active branch, to `origin/main` using `reset`.

```bash
git reset origin/main
```

Now you can switch to your feature branch and get back to work.

```bash
git switch find-apples
```

## 13. Summary

1. List commands and concepts your team learned in this activity.
2. List questions your team has.

---
Copyright 2021, Stoney Jackson <dr.stoney@gmail.com>
Licensed under CC-BY-SA 4.0 International.
