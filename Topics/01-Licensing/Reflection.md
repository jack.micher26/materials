## Reading - Copyright and Licensing

* 42m Video: *What you need to know about open source licensing* by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>
