# Homework - Open Source Licenses

* YOUR NAME
* THE DATE
* CS220-Sp2021

## Objectives

* Edit Markdown
* Distinguish between permissive and CopyLeft Licenses
* Use other sources to understand licenses such as TLDRLegal
* Identify the role of Contributor License Agreements (CLAs)
* Identify the nature of unlicensed software

## Instructions

1. Download the raw source of this file.
2. Read the following position article about Copyleft and Permissive open-source licenses. https://www.datamation.com/open-source/open-source-debate-copyleft-vs-permissive-licenses/#:~:text=Both%20copyleft%20and%20permissive%20licenses,software%20can%20do%20these%20things.
3. Edit this source of this file in your favorite plain text editor (e.g., VS Codium).
4. Write your answers below each question.
5. Save your work.
6. Submit your edited file to the designated Kodiak Assignment post.

Answer the following questions. Feel free to consult other sources to help you answer these questions.

1. Which type of license ensures that all derivatives of the work will always have the same license?
2. Which type of license allows derivatives of the work to apply a different license to their new version?
3. Which type provides the most freedoms to recipients?
4. Which ensures that all recipients have the same rights on all derivations?
5. Which is easier for a company to work with because they do not have to regularly audit their commits to ensure they are in compliance with licenses?
6. Which does not require a contributor to supply a copyright notice and a statement of how their contribution is licensed?
7. MIT License (Expat), is a classic permissive license. GPLv3 (not LGPLv3) is a classic CopyLeft license. Look both up on [TLDRLegal](https://tldrlegal.com/). What makes the GPLv3 license a CopyLeft license?
8. What is Linux licensed under? What type of license is it?
9. Read the [Developer Certificate of Origin](https://developercertificate.org/). Linux requires its contributors to sign-off on this certificate for each contribution. What could go wrong if they didn't?
10. Read this [Contributor License Agreement](https://gist.github.com/pjcozzi/4d1ab2166519de7ba41b). Why must it mention the MIT license?
11. If you find code online that does not have a license, what can you do with it? Do you think that was the intent of the author? What can you do about it?

---
#### &copy; 2019 Stoney Jackson and Karl R. Wurst

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
