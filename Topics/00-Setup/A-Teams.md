# Form Teams Activity

Time: 15m

## Instructions

1. If you are physically in class, your instructor will assign you a team number.
2. In your team channel read and discuss the following. Note any questions or
   insights your team has at the bottom of this document.

## Teams

Learning to work effectively in teams is one of the objectives of this course,
because teamwork is an essential skill in the modern software development workplace.
Also, learning in teams has been shown to be an extremely effective way to learn.
So in this course, you'll be working in small teams a lot.

## Roles and Responsibilities

When working in a team, it's good to have roles and responsibilities as it helps
the team work more effectively together.

In this class there are several roles that we'll use, some more commonly than others.
Below are some of the roles you'll encounter and their responsibilities.

- Manager - Manages time. Ensures everyone has a voice and is able to participate.
  Encourages others by periodically recognizing their achievements verbally.
- Recorder - Records the teams answers and notes from discussions and minim lectures.
- Spokesperson - Speaks for the team during full class discussions.
- Quality control - Ensures that the team's answers are good by questioning reasoning,
  rationale, and facts. Also known as the Skeptic.
- Efficiency expert - Ensures that the team is functioning well together,
  and makes notes about how the team might work more effectively in the future.
- Driver - Manipulates code and tools that are being studied on behalf of the team.
- Navigator - Reads the questions, stays aware of the big picture and what's next.
- Researcher - Responsible for finding relevant documentation online relevant to the task at hand.

Note that everyone is responsible for helping to work through activities and solve problems.

Each session/activity, your team will be asked to assign a set of roles to members.
Everyone will have at least one role, sometimes you may have more than one role.

Be sure to rotate roles each sessions so that everyone serves in different roles over time.

## Teams on Discord

Most team members should be sharing their screen when working in their team.
Here is a list of what is on the screen of different role when sharing screens.

- Recorder - The activity opened in an editable format.
- Pilot - The development environment.
- Other team members - Documentation and search results to help answer questions.

## Team members in Person

If two or more of you are physically colocated, separate yourselves and force yourselves
to use Discord. This will help you develop your skills to work in teams online, and will
prepare your team to do so if necessary because one or more of you choose to or must
attend remotely.

## Working on an Activity

All team members should work through each question together. Do not "divide and conquer"
the questions unless you have permissions to do so. The purpose is to use the team to
develop a shared understanding of the material under study. The process of doing this
will help all members develop a deeper understanding of the material while at the same
time improving everyone's interpersonal, communication, and teamwork skills.

## Questions, Notes, and Insights
