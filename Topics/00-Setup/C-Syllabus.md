# Syllabus Activity

Time: 20m

## Roles

Use the same roles as in the previous activity, but rotate the roles.
(We won't normally rotate roles every session, but we are practicing.)

1. Manager - NAME
2. Recorder - NAME
3. Spokesperson - NAME
4. Efficiency Expert - NAME (If you only have 3 members assign this role to the spokesperson)

## Instructions/Questions

1. Where is the Syllabus located?
2. How is it organized?
3. List the major sections of the syllabus below, then review the contents of each within your team.
   Note any questions under the relevant section. Spokesperson should be prepared to raise these
   issues during report out.
