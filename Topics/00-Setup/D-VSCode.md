## VS Code

Time: 15m

## Roles

- Driver: INITIALS (pick someone who does not have VS Code installed; or the person with the least experience using VS Code)
- Manager: INITIALS
- Recorder/Spokesperson: INITIALS
- Researcher: INITIALS (assign manager this role if you have a team of 3)

## Instructions

Driver: Share your screen. Carry out the steps below.
Recorder: Share your screen. Record action taken for completing each step.
Researcher: Share your screen. Help driver figure out what to do using your favorite search engine.
Manager: Help coordinate the team and manage time.

Use your operating system to do the following

1. Install VS Code.
2. Create a directory named "VSCodeTest" on your desktop.

Use VS Code to do the following

3. Open "VSCodeTest" using VS Code.
4. Create a file named "test.md" in "VSCodeTest".
5. Create a directory named "subdirectory".
6. Create a file named "another.md" in "subdirectory".
7. Move "test.md" into "subdirectory".
8. Copy/paste the contents of your team's pad into "test.md".
9. Save "test.md".
10. Preview the file (CTRL+SHIFT+V or COMMAND+SHIFT+V on Mac) and close the preview.
11. Rename "test.md" to "notes.md".
12. Delete "another.md".
13. Open multiple files at the same time.
14. Rearrange the editors so they are side-by-side.
15. Find a plugin for Markdown and install it. Figure out how it works.
16. Exit VS Code.

## Tips, Questions, and Insights

Record here tips, questions, or insights that you have.
