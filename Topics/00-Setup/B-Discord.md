# Discord Activity

Time: 20m

## Roles

1. Manager - NAME
2. Recorder - NAME
3. Spokesperson - NAME
4. Efficiency Expert - NAME (If you only have 3 members assign this role to the spokesperson)

## Instructions

1. Form teams of size 3-4.
2. Join your teams audio/video channel and text channel in Discord.
3. Have each member mute and unmute themselves.
4. Have each member change the volume of at least one of the other members.
5. Have each member of your team disconnect and reconnect to the audio/video channel.
6. Have each member of your team share their screens.
7. Have each member of your team switch their view between each members screen.
8. Have each member stop sharing their screens.
9. Have each member who can, share their video (web-cam; don't forget to smile and wave).
10. Have each member turn off their video without disconnecting from the channel.
11. Have each member type something in the team's text channel.
12. Have each member @ someone in the chat channel and talk about what that does.
13. Have each member react to a chat message and discuss why you might do that instead of writing another chat message.
14. Identify 2-3 tips for using Discord.
