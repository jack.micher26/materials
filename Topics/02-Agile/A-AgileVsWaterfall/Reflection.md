## Agile vs Waterfall - Reflection

* 2300 word article, 20m read: *Agile vs Waterfall: Which Method is More Successful?*. Published on December 4, 2014, Updated on February 15, 2020 by Michael Sweeney. <https://clearcode.cc/blog/agile-vs-waterfall-method/>
