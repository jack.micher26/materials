# Agile vs Waterfall


## Roles

* Manager: INITIALS
* Recorder: INITIALS
* Spokesperson: INITIALS
* Quality Control: INITIALS


## Model 1

Time: 15m

See Sweeney's article "Agile vs Waterfall: Which Method is More Successful" https://clearcode.cc/blog/agile-vs-waterfall-method/

Use the above article to answer the following questions.

1. In section 1, what phases/stages does waterfall have that agile does not? What do you think that means?

2. Explain the "Boat" diagram.

3. The Mona Lisa is often used to illustrate incremental and iterative development. Search for these and summarize your understanding of these concepts and how they relate to agile development.

(Class discussion 15m)

## Model 2

Time: 5m

>! NOTE: The pie charts in this activity use Mermaid. If you are viewing the source or your previewer renders it as a code block, you should still be able to extract the information you need. If you want to see the pretty pie charts, try viewing them in GitLab.)

Project success rates by process methodology from Ambysoft's 2013 Project Success Rates Survey http://www.ambysoft.com/surveys/success2013.html :


```mermaid
pie
    title Projects using an Agile Process
    "Success" : 64
    "Challenged" : 28
    "Failed" : 8
```

```mermaid
pie
    title Projects using Waterfall
    "Success" : 49
    "Challenged" : 33
    "Failed" : 18
```

Use model 1 to answer the questions below.

1. Which process methodology had the larger success rate?

2. Which process methodology had the larger fail rate?

3. Which process methodology has the larger challenged rate?

4. What do you think is meant by "challenged" as opposed to failed?

(Class discussion 5m)

## Model 3

Time: 5m

Project success rates by process methodology from the Standish Group's 2015 CHAOS Report https://www.infoq.com/articles/standish-chaos-2015/ .


```mermaid
pie
    title Projects using an Agile Process
    "Success" : 42
    "Challenged" : 49
    "Failed" : 9
```

```mermaid
pie
    title Projects using Waterfall
    "Success" : 14
    "Challenged" : 57
    "Failed" : 29
```

Use model 2 to answer the questions below.

1. Which process methodology had the larger success rate?

2. Which process methodology had the larger fail rate?

3. Which process methodology has the larger challenged rate?

4. Does the results from the 2015 CHAOS Report (model 2) confirm or reject those from Ambysoft's 2013 Project Success Rates Survey (model 1)?

5. If you were planning to start a new project, which process methodology would you likely want to learn more about and possibly use?

(Class discussion 5m)


## Other articles on Waterfall vs Agile

*   [https://clearcode.cc/blog/agile-vs-waterfall-method/](https://clearcode.cc/blog/agile-vs-waterfall-method/)
*   [https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/](https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/)
*   [https://www.infoq.com/articles/standish-chaos-2015](https://www.infoq.com/articles/standish-chaos-2015)


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
