# Scrum - Homework

1. Find a diagram that depicts the Scrum framework. Include the diagram as an image (e.g., png, gif, or jpeg). Describe it here and identify what parts of Scrum are not shown in the diagram.
2. Find a video that explains in more detail how scrum works. Provide a link to the video.
3. Complete the following assessment with a score of 85% or higher (you can retake it as many times as you like): [Scrum Open Assessment](https://www.scrum.org/open-assessments/scrum-open) . Print the result page as a PDF and submit the PDF.
