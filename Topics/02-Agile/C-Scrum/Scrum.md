# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: INITIALS
* Recorder: INITIALS
* Spokesperson: INITIALS
* Researcher: INITIALS


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition

Time: 5m

1. True or false; you can apply Scrum as described in The Scrum Guide.
2. True or false; you should not deviate from Scrum as described in The Scrum Guide.
3. True or false; Scrum is a completely defined process for software development.


## Model 2: Scrum Theory/Pillars

Time: 5m

1. What are the three pillars of Scrum?
2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?
3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?
4. What is necessary for "adaptation" to be possible?


## Model 3: Scrum Values

Time: 5m

1. What values does Scrum add to the Agile values?
2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.


## Model 4: Scrum Team

Time: 10m

1. What roles are there in the Scrum Team?
2. How big should a Scrum Team be?
3. Which of these teams is cross-functional?
   1. A database team that is able to design and implement the database.
   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert.
4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog.
   2. Clearly communicates items in the Product Backlog.
   3. Ensures Scrum events take place and are positive, productive, and time-boxed.
   4. Orders/prioritizes the items in the Product Backlog.
   5. Removes barriers between stakeholders and the Scrum Team.
   6. Holds the team accountable as professionals.
   7. Adhering to the Definition of Done.
   8. Adapting the plan each day toward the Sprint Goal.
   9. Clearly communicates the Product Goal.

## Model 5: Scrum Events

* Time: 20m
* Class discussion: 20m

Scrum Events
1. What are the major Scrum events?
2. What is the fundamental purpose of all Scrum events?

Sprints
3. How long is a typical Sprint?
4. True or false; the length of a Sprint is determined by the amount of work that needs to be done?
5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint?
6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent the Team from meeting the Spring Goal on time. Which of the following is allowed by Scrum?
   1. Renegotiate the end of the current Sprint?
   2. Renegotiate the quality of the work being done in the current Sprint?
   3. Renegotiate the scope of the work of the current Sprint?
7. When may a Sprint be canceled and by whom?

Sprint Planning
1. Purpose?
2. Length?
3. Frequency?
4. When during the Sprint?
5. Who are involved?
6. What is the input?
7. What is the output?

Daily Scrum
1. Purpose?
2. Length?
3. Frequency?
4. When during the Sprint?
5. Who are involved?
6. What is the input?
7. What is the output?

Sprint Review
1. Purpose?
2. Length?
3. Frequency?
4. When during the Sprint?
5. Who are involved?
6. What is the input?
7. What is the output?

Sprint Retrospective
1. Purpose?
2. Length?
3. Frequency?
4. When during the Sprint?
5. Who are involved?
6. What is the input?
7. What is the output?

## Model 6: Scrum Artifacts

General

Product Backlog
1. What is it and what does it contain?
2. Who is responsible for sizing an item in the backlog?
3. Who is responsible for ordering the items in the backlog (highest priority first)?
4. What is the Product Goal and its purpose?

Sprint Backlog
1. What is in the Sprint Backlog?
2. What is the Sprint Goal and its purpose?

Increment
1. What is it?
2. How many may be produced as part of a sprint?
3. What is the Definition of Done?
4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?


<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
