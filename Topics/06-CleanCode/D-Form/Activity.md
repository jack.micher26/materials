# Form

## Roles

Be sure to rotate roles!

* Manager:
* Recorder:
* Skeptic:
* Spokesperson:

## Model 1

Watch the "Form" Clean Code video by Robert Martin through O'Reilly
Learning via ACM Learning.

> **NOTE**
>
> * **START** at 11m to skip the science lesson.
> * **END** at 53m 34s "Data Structures"
> * That's about 42m

There is a book by the same name and by the same author -- Clean Code by
Robert Martin -- and is also available in O'Reilly Learning.


## Questions

As a team discuss and answer the following questions.

1. Does Uncle Bob like a "coding standard"?
2. What does Uncle Bob believe should be the "coding standard document"?
3. What are some of the problems with comments (list all that you can)?
4. When are comments necessary (list all that you can)?
5. What are (or leads to) bad comments (list all that you can)?
6. How many lines should a typical file be?
7. What are the rules for blank lines?
8. What is the general rule for organizing things vertically
    (i.e. what should be close together, what should be far apart)?
9. What are the rules for line length?
10. What are Uncle Bob's opinions about indentation and brace placement?
11. What the most important rule about indentation and brace placement?
12. What does Uncle Bob think about getter's and setter's and why?
