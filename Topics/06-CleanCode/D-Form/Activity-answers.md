# Form - Part 1

## Roles

Be sure to rotate roles!

* Manager:
* Recorder:
* Skeptic:
* Spokesperson:

## Model 1

Watch the "Form" Clean Code video by Robert Martin through O'Reilly
Learning via ACM Learning.

> **NOTE**
>
> * **START** at 11m to skip the science lesson.
> * **END** at 53m 34s "Data Structures"
> * That's about 42m

There is a book by the same name and by the same author -- Clean Code by
Robert Martin -- and is also available in O'Reilly Learning.


## Questions

As a team discuss and answer the following questions.

1. Does Uncle Bob like a "coding standard"?
   1. Yes.
2. What does Uncle Bob believe should be the "coding standard document"?
   1. The code itself.
3. What are some of the problems with comments (list all that you can)?
   1. The clutter up code.
   2. They could be a crutch to excuse poor code... that is you should focus on the code.
   3. If there is a discrepancy between comments and code, the code is what is real.
   4. A comment is a failure to express oneself (to humans) in code.
   5. First try hard to express yourself clearly in code... and only when you can't, then comment.
   6. Comments often eventually lie. (Comments are often non-local)
4. When are comments necessary (list all that you can)?
   1. Legal comments (put them at the top, automate it)
   2. Informative comments (describing an embedded language)
   3. Explanation of intent when you cannot express yourself otherwise
   4. Warning of consequences
   5. TODO ... NOT!
   6. Public API documentation
5. What are (or leads to) bad comments (list all that you can)?
   1. Mumbling (talking about random stuff)
   2. Redundant explanation -- DRY (Don't Repeat Yourself)
   3. Mandated redundancy (more of a cause, than a bad comment itself)
   4. Wrong or misleading comments... if you see it, fix it or delete it.
   5. Journal comments... source control
   6. Noise comments... stating the obvious... same as redundant explanation
   7. Big banner comments
   8. Closing brace comments... but we have good IDEs and our functions are small
   9. Attributions... Version control
   10. HTML in comments... It's more important that the comment reads in the code
   11. Non-local information... Rots because when something else gets modified...
   12. Commented out code... delete it
6. How many lines should a typical file be?
   1. 500 max, 50-60 average, 30-100 most files
   2. 50-60 lines average, most are less than 100-200 lines, max 500 lines
   3. Smaller is better
7. What are the rules for blank lines?
   1. Separate methods from each other and variables, and between variables of different visibility
   2. Separate major constructs/topics, variable declarations and instructions, instructions and return
   3. Use consistent spacing
8. What is the general rule for organizing things vertically
    (i.e. what should be close together, what should be far apart)?
    1. Things that are related to each other should be vertically closer together
9.  What are the rules for line length?
    1.  Never have to scroll right to see it
    2.  40-50 average and < 100-120
10. What are Uncle Bob's opinions about indentation and brace placement?
    1.  Spaces
    2.  2 space
    3.  K&R style
11. What the most important rule about indentation and brace placement?
    1.  Be consistent and use whatever the team agrees to
12. What does Uncle Bob think about getter's and setter's and why?
    1.  A class with lots of getters, the more it encourages "ask, don't tell"
    2.  Getter's and setter's typically use a single variable in the class,
        which means they are not very cohesive. So, keeping getters and setters
        to a minimum improves cohesiveness.
    3.  Getter's expose implementation, so think careful about naming them
        and try to be more general. This will improve your design and make
        polymorphism possible.
