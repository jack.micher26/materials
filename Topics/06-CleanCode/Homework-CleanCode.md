# Homework 5

## Goals

Gain experience with Clean Code.

## Part 1 - Select Code

Find some non-trivial code that you wrote in a previous course (data structures?).

- It doesn't have to be pretty. In fact, it's better if it is a bit rough.
- Ideally it runs and "works", but may have bugs.
- The language it's written in doesn't matter; as long as it is a standard
   high-level, 3rd generation programming language --- for example: Java, C,
   C++, Python, Javascript, etc.
- Preferably code that you wrote. If you didn't write it, each file must have
   the author(s) in comments at the top.
- Preferably more than one file.
- Preferably with at least one method that is more than 10 lines long.

***If you can't do the above, let me know ASAP.***

## Part 2 - Prep

1. Clone hw5 project from within your homework group under
   <https://gitlab.com/wne-csit-jackson/cs220/spring2021/homework>
   1. Do not fork it.
   2. Do not create a feature branch.
   3. Commit your work directly to main.
2. Copy your code into `./before/` and commit it.
3. Copy your code into `./after/` and commit it.

## Part 3 - Review your code

1. Change your code based on the following videos from Clean Code:
   1. Names++
   2. Functions
   3. Function Structure
   4. Form
2. Write a report that explains how you addressed one principle from each
   of the above videos.
3. Place your write up in `./report.md`.


---
Copyright © 2021 Stoney Jackson. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
