# Functions

## Roles

* Manager:
* Recorder:
* Skeptic:
* Troubleshooter:

The Troubleshooter will help team members with technical challenges and help
to find workarounds to reach the actual goal. For example, if someone cannot
access O'Reilly Online, see if you can help them gain access, or find a
workaround that will allow them to watch the video for now.

## Model 1

Watch the "Functions" Clean Code video by Robert Martin through O'Reilly
Learning via ACM Learning. You may start at 6m 3s (the first 6m is a science
lesson that is interesting, but not relevant to the topic of the chapter).

There is a book by the same name and by the same author -- Clean Code by
Robert Martin -- and is also available in O'Reilly Learning.
The above video corresponds to chapter 3 of this text.

> **NOTE**
>
> You can start at 9m 20s. What comes before is an extremely brief introduction
> and a science lesson.

## Questions

As a team discuss and answer the following questions.

1. How big should a function be?

2. If you follow this rule and you have a `try` or an `if` or a `loop`, what
    must the body of the `try`, `if`, or `loop` be?

    Example:
    ```
    very_descriptive_function_name() {
        if (some_condition())
            // What kind of statement is this likely to be?
        else
            // Same question.
    }

3. Follow up... if you follow Uncle Bob's suggestion, what will be the typical
    maiximum level of nesting of code? Say the body of the funtion is indented
    to level 0. Each additional level would add 1 to this level. So the code
    in the else branch of the if statement above is at level 1.


4. Can you think of a time when Uncle Bob might be OK to have a function whose
    indentation reaches level 2?


5. What provides "sign posts" so that new programmers can find their way around
    code?


6. If you have a long function, what should your refactor it into to get
    started?


7. How did Uncle Bob get rid of the switch statement?


8. Why doesn't Bob's code have very many comments?


9. What questions or insights does your team have?
