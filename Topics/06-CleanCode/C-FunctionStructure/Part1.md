# Function Structure - Part 1

## Roles

* Manager:
* Recorder:
* Skeptic:
* Spokesperson:

## Model 1

Watch the "Function Structure" Clean Code video by Robert Martin through O'Reilly
Learning via ACM Learning.

There is a book by the same name and by the same author -- Clean Code by
Robert Martin -- and is also available in O'Reilly Learning.


> **NOTE**
>
> * **START** at 9m
>
> * **STOP** at 51m ... when you see *Tell Don't Ask*.
>

## Questions

As a team discuss and answer the following questions.

1. The best function has how many arguments?
2. What's Uncle Bob's limit the maximum number of arguments?
3. What are some ways that you can reduce the number of arguments?
4. What's wrong with boolean parameters?
5. What's wrong with output arguments?
6. How are null parameters related to boolean parameters?

> Notice the Uncle Bob relies on unit tests. We'll learn how to write unit
> tests soon.

7. How should methods/functions be organized in a file?
8. What is the typical relationship between switch-statements and dependencies?
9. What can you typically use in place of a switch-statement?
10. Where is it acceptable to have a switch statement?
11. What is CQS (command query separation)?
