# Function Structure - Part 2

## Roles

Be sure to rotate roles!

* Manager:
* Recorder:
* Skeptic:
* Spokesperson:

## Model 1

Watch the "Function Structure" Clean Code video by Robert Martin through O'Reilly
Learning via ACM Learning.

> **NOTE**
>
> * **START** at 51m, *Tell Don't Ask*
>

There is a book by the same name and by the same author -- Clean Code by
Robert Martin -- and is also available in O'Reilly Learning.


## Questions

As a team discuss and answer the following questions.

1. What is "Tell, don't ask?"
2. What is a train wreck? What law do the violate?
3. What is the "Law of Demeter?"
4. Finish this though: "Breaks and continues don't violate structured programming but..."
5. How does Uncle Bob feel about the following code and why?

    ```python
    def getIndexOfValueInArray(value, array):
        for i in range(len(array)):
            if array[i] == value:
                return i
    ```
6. Stack: To talk about error handling, Uncle Bob begins an implementation
    of a Stack. He writing unit tests at the same time he is implementing the
    Stack. Write any questions you have here.
7. Does Uncle Bob think it is better to return an error value from a function
    or raise an exception?

> **Example**
>
> Here is an example of some code that uses error handling.
>
> ```python
> def main():
>   error_code = foo()
>   if error_code > 0:
>     handle_error(error_code)
>   error_code = bar()
>   if error_code > 0:
>     handle_error(error_code)
>
> def foo():
>   error_code = baz()
>   if error_code > 0:
>      return error_code  # Bubble up errors to handle consistently.
>   ...
>
> def bar():
>   ...
>
> def baz():
>   ...
>
> def handle_error(error):
>   error_msgs = [ "Missing File", "Bad Argument", "Dereferrenced Null", ... ]
>   log(error_msgs[error])
>   sys.exit(error)
> ```
>
> Here is that same program using exceptions.
>
> ```python
> def main():
>   try:
>     foo()
>     bar()
>   except Exception as e:
>     handle_error(e)
>
> def foo():
>   baz()   # exceptions are automaticly bubbled up
>   ...
>
> def bar():
>   ...
>
> def baz():
>   ...
>
> def handle_error(error):
>   log(error)
>   sys.exit(1)
> ```
>
> Which world would you rather program in?

8. Should you use Java's checked exceptions?
9. Why doesn't Uncle Bob usually pass a message in an exception?
10. What does Uncle Bob have stack.top() return when called on an empty stack?
11. What does Uncle Bob have stack.find(x) return when x is not found?
12. Based on the previous 2 questions, when should you return null and when
    should you raise an exception?
13. What's the maximum number of try-catch blocks per function?
