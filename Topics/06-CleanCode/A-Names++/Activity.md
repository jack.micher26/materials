# Names++

## Roles

* Manager:
* Recorder:
* Skeptic:
* Troubleshooter:

The Troubleshooter will help team members with technical challenges and help
to find workarounds to reach the actual goal. For example, if someone cannot
access O'Reilly Online, see if you can help them gain access, or find a
workaround that will allow them to watch the video for now.

## Model 1

Watch the "Names++" Clean Code video by Robert Martin through O'Reilly Learning
via ACM Learning. You may start at 6m 3s (the first 6m is a science lesson that
is interesting, but not relevant to the topic of the chapter).

There is a book by the same name and by the same author -- Clean Code by
Robert Martin -- and is also available in O'Reilly Learning.
The above video corresponds to chapter 2 of this text.

## Questions

As a team discuss and answer the following questions.

1. What general major rules/guidance did the author mention?
2. Generally what part of speech should a class name be?
3. Generally what part of speech should a method name be?
4. Generally what part of speech should a variable name be?
5. What part of speech should a boolean variable be named?
6. What is the general rule for the name of a method that returns a boolean
    values?
7. Generally what part of speech should enum values be named?
8. What is the relationship between the length of a variable name and its scope?
9. What is the relationship between the length of a method name and its scope?
10. What is the relationship between the length of a class name and its scope?
11. What other questions or insights does your team have?
