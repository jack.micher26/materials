# .gitignore and .gitattributes Activity

## Learning Outcomes

By the end of this activity, participants will be able to...

- Explain the purpose of .gitignore.
- Use .gitignore to ignore different groups of files by extension.
- Justify types of files that should and should not be stored in a repository.
- Explain the problem of line-endings and how .gitattributes can help.

## Process Skills Practiced

- Formulating and testing hypotheses.

## Team Roles

* Manager:
* Recorder:
* Spokesperson:
* Efficiency Expert:

## Model 1: Files to NOT commit

Time: 10m

Read/skim the following article or watch the video at the following page:

* *What not to save into a Git repository*, 30th Nov 2018, <https://zellwk.com/blog/what-not-to-save-into-a-git-repo/>

### Questions

1. List the types of files that should not be committed into a repository.
2. Can you think of a time when you may want to commit a generated file into a repository?
3. What about data files? Should they be committed into a repository? Try to come up with an example to support your position.

## Model 2

Time: 5m

```
$ pwd
/home/hjackson/p1
$ ls -a
.   ..  .git  A.java    B.java
$ javac *.java
$ ls -a
.   ..  .git  A.class   A.java    B.class   B.java
$ git status
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        A.class
        A.java
        B.class
        B.java

nothing added to commit but untracked files present (use "git add" to track)
```

1. What will be committed by the following command sequence?
    ```
    git add .
    git commit -m "feat(AB): add A and B"
    ```

2. What's wrong with running this command sequence now?


## Model 3

Time: 5m

```
$ edit .gitignore
$ cat .gitignore

# Ignore Java class files.
*.class

$ git status
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        .gitignore
        A.java
        B.java

nothing added to commit but untracked files present (use "git add" to track)
```

1. What new file was created above? What is its contents?

2. What will be committed by the following command sequence?
    ```
    git add .
    git commit -m "feat(AB): add A and B"
    ```

3. Is it safe to run this sequence now?

4. Consider the contents of .gitignore file.
     1. What does the `#` do?
     2. What does the `*` do?
     3. What is the meaning of `*.class`?

## Model 4

Time: 5m

```
$ ls -a
.          ..         .git       .gitignore A.class    A.java     B.class    B.java     C.py       D.py
$ python C.py
Hi
$ ls -a
.          ..         .git       .gitignore A.class    A.java     B.class    B.java     C.py       D.py       D.pyc
$ git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        C.py
        D.py
        D.pyc

nothing added to commit but untracked files present (use "git add" to track)
```

1. What files will be committed by the following sequence?
    ```
    git add .
    git commit -m "feat(CD): add C and D"
    ```

2. Is the sequence safe to run now? Why or why not?

3. What do you think you should do to fix the problem?

## Model 5

Time: 5m

Visit https://github.com/github/gitignore

### Questions

1. In what ways are the .gitignore files organized?

2. Find and view a .gitignore file for a Python-based project. Pick a few rules that you think are interesting/surprising/confusing and list them here.

3. Find and view a .gitignore file for a Java-based project. Pick a few rules that you think are interesting/surprising/confusing and list them here.

4. Your team will be starting a new project that will have use both Java and Python. How will you create your .gitignore file?

## Model 6

Time: 10m

Different operating systems different character(s) for line endings for text files.

1. Do some quick research, what line ending character(s) is(are) used by the following operating systems...
    * Windows?
    * Linux?
    * MacOS?
2. Consider a team of developers working on the same project using different operating systems. What problems might this cause with version control?
3. It's possible to configure your local git to handle line-endings properly so that the above problem doesn't occur. What's the problem with relying on each developer to configure git properly to handle line endings?
4. Skim https://help.github.com/en/github/using-git/configuring-git-to-handle-line-endings . How does adding a .gitattributes address our problem?
5. .gitattributes can be complicated to write, how does the following site help? Identify files from https://github.com/alexkaratarakis/gitattributes that you would want to use for our Java and Python project.
6. Use the generated linked to on the previous project to generate an appropriate gitattributes file. (Don't paste it here, it will be long, I'll believe you.)

---
Copyright © 2021 Karl R. Wurst and Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
