# Homework - Git

You are in a capstone class, and are going to be working in a small team. Your team will be creating a mobile frontend for placing orders to a Bear Necessities Market as part of the LibreFoodPantry community. Specifically your team will be implementing an app that will run on Android. At least one of your team members is running macOS on their personal machine, and at least another is running Windows. Also, at least one team member regularly uses use Vim and at least one prefers to use VisualStudioCode. Since you'll be contributing to the LibreFoodPantry community, your project will be licensed under GPLv3.0.

## Task

Name your teams project. Create a new project folder that is under Git version control. Configure the project so that files that are commonly generated during the development of an Android app, including by the development environments and operating systems your team is using, are not accidentally committed to the repository. Also, configure your repository to make sure that line endings of source files don't get messed up between macOS and Windows (tip: Android apps are written in Java). Also, please place a `LICENSE` file in the root of the project with the contents of the plaintext copy of the GPLv3. Last, please place a `README.md` in the root that has the title of the project.

Useful resources:

* [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
* [Collection of gitignore files](https://github.com/github/gitignore)
* [Collection of gitattributes files](https://github.com/alexkaratarakis/gitattributes)

## Constraints

1. You need at least 5 commits. The order of the commits do not matter.
2. Each commit should do one thing.
3. Use conventional commit messages.

Resources

* [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Submission

> :bulb: If you cannot see hidden files in your file browser (like `.git/`), configure your operating system to show hidden files.
> * [macOS](https://support.apple.com/guide/mac-help/show-or-hide-filename-extensions-on-mac-mchlp2304/mac#:~:text=In%20the%20Finder%20on%20your,%E2%80%9CShow%20all%20filename%20extensions.%E2%80%9D)
> * [Windows](https://support.microsoft.com/en-us/windows/show-hidden-files-0320fe58-0117-fd59-6851-9b7f9840fdb2)

> :bulb: If you cannot see file extensions on files in your file browser (like `.zip` at the end of `my-project.zip`), configure your operating system to show file extensions.
> * [macOS](https://support.apple.com/guide/mac-help/show-or-hide-filename-extensions-on-mac-mchlp2304/mac#:~:text=In%20the%20Finder%20on%20your,%E2%80%9CShow%20all%20filename%20extensions.%E2%80%9D)
> * [Windows](https://support.winzip.com/hc/en-us/articles/115011457948-How-to-configure-Windows-to-show-file-extensions-and-hidden-files)

> :bulb: If you don't know how to zip or unzip files on your operating system...
> * [macOS](https://support.apple.com/guide/mac-help/zip-and-unzip-files-and-folders-on-mac-mchlp2528/mac)
> * [Windows](https://support.microsoft.com/en-us/windows/zip-and-unzip-files-8d28fa72-f2f9-712f-67df-f80cf89fd4e5)


1. Zip the folder that is the root of your project. This is the folder that contains the `.git` folder. So if `my-project/.git/` exists, zip up `my-project`. This should give you a file named `my-project.zip`.
2. Submit the zip to the designated Assignment post on Kodiak.

## Check your work

* Check your work by downloading the zip you submitted, and extracting it to a ***different location*** that the original. Exam its contents, `git log`, etc.
* An invalid submission is a quick way to get a 0.

> :information_source: Why extract to a different location? First to make sure you don't destroy your work. Second, if your zip was empty, you may be fooled into incorrectly believing that your existing work is what was extracted from the zip, when in fact the zip is empty.

---

Copyright 2021, Stoney Jackson <dr.stoney@gmail.com>. Licensed under CC-BY-SA 4.0.
