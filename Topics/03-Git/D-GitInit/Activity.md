# git init

Here you'll learn to create local repositories.

## Roles

* Manager: INITIALS
* Recorder: INITIALS
* Driver: INITIALS
* Navigator: INITIALS

## Model 0 - Setup

Time: 5m

1. Open a linux-based terminal
2. Create a temporary directory to hold the work of this activity

    ```bash
    mkdir gitinit
    ```
3. Change into that directory

    ```bash
    cd gitinit
    ```

## Model 1 - git init

Time: 5m

Run the commands below, but before and after each command below, run `ls -a`.

```bash
mkdir my-new-project
cd my-new-project
git init .
```

1. What does `mkdir my-new-project` do? Is it a git command?
2. What does `cd my-new-project` do? Is it a git command?
3. What does `git init .` do?
4. What happened to your prompt after `git init .`? Why do you think that happened?
5. Try running `git init .` again. What was the result?

## Model 2 - .git/

Time: 5m

Continue from the previous Model (your terminal should be in my-new-project).
Run the following commands, but run `ls -a` before and after each command.

```
rm -rf .git
```

1. What did the command above do?
2. What happened to your prompt?
3. Try running `git init .` again and run `ls -a` after. What was the result?
4. What do you think the `.git/` contains or represents?
5. If you want to know if git is being used to manage a project, what would you look for?
6. Run `ls` (without `-a`). What do you see? Why do you think that is?

## Model 3

Time: 5m

If you are still in `my-new-project` return to its parent `gitinit`.

```bash
cd ..
```

Now run the following commands running `ls -a` before and after each.

```
mkdir existing-work
cd existing-work
touch one.txt
touch two.txt
git init .
```

1. What did each command above do?
2. Was git able to initialize in a directory with existing content?
3. Assume /some/directory contains some existing project, but is not yet being managed by git. Give a set of commands to start using git with that project.

## Model 4

Time: 5m

If you are still in `existing-work` return to its parent `gitinit`.

```bash
cd ..
```

Now run the following commands running `ls -a` before and after each.

```
git init another-new-project
cd another-new-project
```

1. What did the first command do?
2. What did the second command do?
3. Why is the second command important?
4. Compare and contrast the commands and their effects in this Model with those in Model 1?

## Model 5

Time: 5m

1. Summarize what you learned from this activity.
