# Git - Reflection

## 1. Watch Introduction to Git

2.4m Video: *What is Git? Explained in 2 Minutes!* by Programming with Mosh, Sep 21, 2020 <https://youtu.be/2ReR1YJrNOM> .


## 2. Install Git and a Linux-like Shell

>! Note: if you do not have one of the Operating Systems below, you'll need to use a lab computer to complete the assignment. See the "Lab Computer" section below.

### 2.1. Windows

Only complete this section if your personal computer runs Windows.

On Windows, installing Git for Windows will give you both Git and Git-Bash (a Linux-based terminal).

1. Download and install [Git for Windows](https://git-scm.com/download/win), accepting all defaults.
2. Right-click on your desktop and select `Open Git-Bash here` (or something similar).
3. Run `git --version` in the Git-Bash terminal.
4. Take a screenshot of the result.
5. Upload the screenshot to the reflection quiz.

### 2.2. MacOS

Only complete this section if your personal computer runs MacOS.

MacOS comes with Terminal.app which runs the Zsh shell, which is a Linux-based shell. That will work fine for this course.

You'll still need to install git. You could download and install git manually from https://git-scm.com/ . However, as a developer you'll regularly be installing free open-source tools. This is more easily done using a package manager on the command-line. So the instructions below have you install Homebrew, a popular package-manager, and then use it to install git.

1. On Mac, start a terminal by pressing COMMAND+SPACEBAR and then type Terminal.app and press enter.
2. Install [Homebrew](https://brew.sh/)
3. Use Homebrew to install Git as follows:
    ```
    brew install git
    ```
4. Run `git --version` in the terminal by typing it in and pressing enter.
5. Take a screenshot of the result.
6. Upload the screenshot to the reflection quiz.

### 2.3. Linux

If you are running Linux, we assume you have a working understanding of how to use it. If you need help, please contact your instructor.

1. Open a terminal.
2. Install git using your Linux distributions package manager. For example, on Debian...
    ```
    sudo apt-get install git
    ```
3. Now run `git --version`
4. Take a screenshot.
5. Upload the screenshot.

### 2.4. Lab Computer

Git and Git-Bash should already be installed. So follow the Windows section above, skipping the install step.
