# Saving Changes

We will use git to keep track of changes made to the files in your
project.

Content Learning Objectives
---------------------------

*After completing this activity, students should be able to:*

-   Determine whether files are changed or unchanged.

-   Determine whether files are in the git working area or the git
    staging area.

-   Move changed files to the git staging area.

-   Commit files in the git staging area.

-   View the log of commits.


## Process Skill Goals

*During the activity, students should make progress toward:*

-   Leveraging prior knowledge and experience of other students.
    (Teamwork)


## Team Roles


* Manager: INITIALS
* Recorder: INITIALS
* Driver: INITIALS
* Spokesperson: INITIALS


## Model 1: A file can be in one of three states in Git

```plantuml
@startuml
[*] -> Working: create
Working -> Staged: git add
Staged -> Committed: git commit
Committed -> Working: modify
@enduml
```

**Working:** The file has changes that will not be committed in the next **`git commit`** command.

**Staged:** The file has changes that will be committed in the next **`git commit`** command.

**Committed:** The file has no uncommitted changes.

### Questions (1 min)

1.  What operation will move a file from the Working area to the Staged area?

2.  What operation will move a file from the Staged to Committed?


## Model 2: Before Starting to Work on your Program to add Feature Y.

You are working on your computer. You are about to start making changes
to add Feature Y to the program. You enter the following commands (each command comes right after the prompt `$`) and get the following results:

```bash
$ ls
A.java B.java C.java

$ git log
commit db967767f9ab675dba21e08c5d20627b34cbe133 (HEAD -> main, origin/main, origin/HEAD)
Author: Karl R. Wurst <karl@w-sts.com>
Date:   Fri Jan 18 14:25:44 2019 -0500

    Add Feature X

$ git status
On branch main
Nothing to commit, working tree clean

$
```

### Questions (8 min)

1.  How many files are there in this program? What are their names?

2.  What was the purpose of the last committed change to the project?

3.  Who made the last set of changes to the program?

4.  When did they finish making those changes?

5.  What does **`git log`** tell you about your program?

6.  What does **`git status`** tell you about your program?

7.  For each file in the program, place it in one of the three states below.

    * Working:
    * Staged:
    * Committed:


## Model 3: Starting to Work on your Program to add Feature Y.

To add Feature Y to your program, you determine that you need to make
changes to the file **B.java**.

> ℹ️ Info: `edit` is not a real Linux command. When you see this command, it means "Create and/or make changes to the specified file using your favorite editor."

```bash
$ edit B.java

$ git status
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)

        modified: B.java

no changes added to commit (use "git add" and/or "git commit -a")

$
```

### Questions (4 min)

1.  For each file in the program, place it in one of the three states below.

    * Working:
    * Staged:
    * Committed:


2.  Assuming that you are done with making changes to the file B.java, what command would move it into the Staging state? Looking only at the output in Model 3, how do you know?


## Model 4: Done with One File, But with More to Do

You want to put **B.java** into a state to indicate that you are done making changes to it. But you still need to make changes to other files before you have finished Feature Y.

```bash
$ git add B.java

$ git status
On branch main

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

          modified: B.java

$
```

### Questions (2 min)

1.  For each file in the program, place it in one of the three states below.

    * Working:
    * Staged:
    * Committed:


## Model 5: More Work for Feature Y

To finish adding Feature Y, you realize you need to make a change to the file A.java, and you need to create a new file D.java.

```bash
$ edit A.java

$ edit D.java

$ git status
On branch main

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

        modified: B.java

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)

        modified: A.java

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        D.java

$
```

### Questions (5 min)

1.  For each file in the program, place it in one of the three states below.

    * Working:
    * Staged:
    * Committed:

2.  You have finished making your changes for Feature Y. You want to have the files **A.java, B.java,** and **D.java** all to be in the Staged state. What command(s) would you give to make this the case?


## Model 6: Done Working on Feature Y

You have finished making all your changes to implement Feature Y, and have Staged all the new and changed files. Let's commit them.

```bash
$ git status
On branch main

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

        modified: A.java
        modified: B.java
        new file: D.java

$ git commit -m "Add Feature Y"
[main 63b66cf] Add Feature Y
3 files changed, 7 insertions(+), 5 deletions(-)
Create mode 100644 D.java

$ git status
On branch main
Nothing to commit, working tree clean

$ git log
commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4
Author: Karl R. Wurst <karl@w-sts.com>
Date:   Fri Jan 18 16:02:12 2019 -0500

      Add Feature Y

commit db967767f9ab675dba21e08c5d20627b34cbe133 (HEAD -> main, origin/main, origin/HEAD)
Author: Karl R. Wurst <karl@w-sts.com>
Date: Fri Jan 18 14:25:44 2019 -0500

      Add Feature X
```

### Questions (9 min)

1.  What is the purpose of the first **`git status`** command?

2.  What does **`git commit`** do?

3.  What is the purpose of **`-m "Add Feature Y"`** in the **`git commit`** command?

4.  What is the purpose of the second **`git status`** command?

5.  What does the **`git log`** command demonstrate?

6.  In what order does **`git log`** shows commits?

7.  What is the purpose of the
    `commit 63b66cf74632ab3f2ebabb333e71d13938d7f2d4`
    part of the log for? Where else do you see that number?

8.  Why do you think there is a stage? Why not go straight to commit?

## Model 7: Practice

Time: 40m

When working on a project, you will likely repeat the following over and over again.

```bash
edit ...                        # Make a change
git add .                       # Stage the change
git commit -m "good message"    # Commit the change
```

Sometimes if you've made many changes and you want to be sure they all belong together before you commit


Manager, one at a time, have each of your team members work through the following exercise while the other team members assist. Be sure they share their screen. Have your recorder make notes of any insights your team discovers along the way.

This will take on average about 10 minutes per person. Probably longer for the first, and less for the last.

1. Create a new directory to hold a new project.
2. Initialize your new directory for use with git.

Use `git log` and `git status` before and after each of the following to confirm your understanding of what is going on.

3. Create a file, stage it, and commit it.
4. Create another file, stage it, and commit it.
5. Modify both files, stage ONE of them, and commit it.
6. Stage the other file and commit it.
7. Delete a file, stage the change, and commit it.
8. Rename a file, stage the change, and commit it.

---
Copyright © 2021 Karl R. Wurst and Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
