# Commit Messages Activity

## Roles

- Manager:
- Recorder:
- Efficiency Expert:
- Spokesperson:

## Model 1

Time: 20m

Chris Beams' [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/): https://chris.beams.io/posts/git-commit/

### Instructions

1. ***DO NOT*** read the entire article right now. You may want to read the full article at another time.
2. Skim the *Introduction*.
3. Read the list of "The seven rules of a great Git commit message"
4. Review the example template of a commit message (just below the 7 rules).

### Questions

1. Which rule or rules does the following commit message violate:
	```
	I moved foo() and bar() from class A to B

	After I moved these methods, I reran the tests. All
	tests passed. So we're good.
	```

2. Rewrite the commit message above. You may make any assumption about the context.


3. Browse through the commit messages of the flake8 project on GitLab (link given below).

	https://gitlab.com/pycqa/flake8/-/commits/master

   Find examples of the following, and paste a link to the commit (right click on the commit title/subject and copy the link, and paste it below):

   1. A commit message with no body.
   2. A commit message with a small body.
   3. A commit message with a large body.
   4. A commit that changes very little.
   5. A commit that changes a lot.

5. From the above select the commit with the best commit message. Paste a link to the commit below (the same link you gave above) and paste the full commit message below. Be prepared to explain your selection.


6. From the above select the worst commit message and paste it below. Paste a link to the commit below (the same link you gave above) and paste the full commit message below. Be prepared to explain your selection.


7. Below list questions that came up while completing the activity.


8. Below list key insights your team discovered during this activity.

## Model 2

Time: 15m

Conventional Commits <https://www.conventionalcommits.org/>

### Instructions

1. ***DO NOT*** read the entire specification now. You may want to later.
2. Read the "Quick summary" and its examples.

### Questions


1. Angular and Oh My Zsh are two projects that use conventional commits. Browse through their commit histories (linked below) and find examples of the following. Paste a link to the commit (right click on the commit title/subject and copy the link, and paste it below):

	* <https://github.com/ohmyzsh/ohmyzsh/commits/master>
	* <https://github.com/angular/angular.js/commits/master>

	Find examples of the following types of commits:

	1. A commit that add a new feature.
	2. A commit that fixes a bug.
	3. A commit that reorganizes the code but does not fix a bug or add a new feature.
	4. A commit that modifies documentation.
	5. A commit that has a breaking change.


2. Suppose I asked you to find similar commits in flake8, do you think it would be easier or harder to find such commits?


## Model 3

Time: 10m

Imagine a commit that a) fixed the whitespace and newlines across the files, b) added a new notification feature, c) fixed a but in the logging system, and d) optimized the rendering algorithm.

1. Suppose someone later discovered a bug in the optimization from the rendering algorithm and wanted to immediately revert the change in production (`git revert THE_COMMIT`) until someone can debug the algorithm and create a fix. What's the problem with such a revert?

2. Write a commit message for the commit above.

3. What was difficult about the previous task? Or what is the least useful part of your commit message?

4. If you know you have to write a short meaningful commit subject, how does that help you create better commits?

5. How does using conventional commits help you figure out what should be in a single commit?

---
Copyright © 2019 Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
