# Reflection

* 4m Video - *Write git commit messages like a PRO with Conventional Commits* by Coding with Justin, Sep 9, 2020. <https://youtu.be/OJqUWvmf4gg>
* 6m Video - *Please, write Git commit messages properly. Here are some good examples.* by Cameron McKenzie, Jul 22, 2020. <https://youtu.be/Kz2iyskwLRA>
