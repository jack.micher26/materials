# Images and Containers

## 1. Scenario

You need to use two versions of Java for two different projects:
Java 11 and 15.

This is challenging because Java and its ecosystem of tools often rely on
systemwide environment variables like PATH and JAVA_HOME to determine what
version of Java to use. There are IDE and Java specific solutions to this
problem.

We'll use Docker to solve this problem, which will work for any language
and does not rely on a specific IDE.


## 2. Choose a Terminal/Shell

On MacOS, use Terminal or iTerm with any Linux-based shell
(e.g., Bash, Zsh, etc.)

On Windows, do NOT use Git Bash. Either use PowerShell or
Command Prompt. I recommend PowerShell because its syntax
is more similar to Linux that is Command Prompt.

If you choose to use Command Prompt, you'll need to replace `${PWD}`
with `%cd%` in the commands below. Also, you will need to substitute
equivalent DOS commands for Linuxs commands
(e.g., use `dir` instead of `ls`).


## 2. TL;DR Solution

1. Download Hi.java to a location where you can easily find it
    from the command line.

2. Open a terminal and navigate to the folder that contains Hi.java.

3. Compile Hi.java.

    ```bash
    docker run -it --rm -v "${PWD}:/w" -w /w adoptopenjdk:11-jdk javac Hi.java
    ```

    > **Windows' Command Prompt Reminder**
    >
    > Substitute `${PWD}` with `%cd%`.

    You'll see some downloading and then it just stops.

    Confirm that you now have a Hi.class file in your current directory.

    ```bash
    ls Hi.class
    ```

4. Run Hi.

    ```bash
    docker run -it --rm -v "${PWD}:/w" -w /w  adoptopenjdk:11-jdk java Hi
    ```

    Notice that this time nothing was downloaded. Why do you think that is?

5. Repeat the above using adoptopenjdk:15-jdk instead of adoptopenjdk:11-jdk
    to compile and run Hi.java using Java 15.

6. List the images in your local Docker environment.

    ```bash
    docker images
    ```

    How many images do you have?

    How big are they?

7. Clean out your entire Docker environment

    ```bash
    docker system prune --all --volumes
    ```

## 3. Going Deeper

### 3.1. Pulling Images

In a Linux-like shell, confirm that your local Docker environment does not
    have any images.

```bash
docker images
```

Download the image you need to compile and run a program written for Java 11.

```bash
docker pull adoptopenjdk:11-jdk
```


List the images in your local Docker environment and confirm you have an
    image.

```bash
docker images
```

Paste the output here.


### 3.2. Running an Image

```bash
docker run adoptopenjdk:11-jdk
```

Note that it flashes something about jshell and exits. That's because jshell
is Java's interactive shell. But by default, when we run a Docker image, it
runs it non-iteractively. So when you ran the the image, the interactive jshell
immediately exited because there was no interactive shell attached to the
process.

> **Info**
>
> Notice that `docker run` did not download anything this time. That's because
> there is a local image associated with `adoptopenjdk:11-jdk`, so `docker run`
> uses it. In fact, if `docker run` finds the image locally, it doesn't check
> if there is a remote image associated with that same tag. It just uses the
> local image.
>
> ***TL;DR - `docker run` only pulls an image if no image exists locally by that
> tag.***
>
> `docker pull` will always look for a remote image by the given tag. If there
> is no local image associated with the tag or if the local image is different
> from the remote image by the same tag, it downloads the
> remote image and associates it with the tag locally &mdash; effectively
> replacing the local image with the remote.
>
> ***TL;DR - `docker pull` ensures you have the same image locally as in the
> registry for the same tag.***

### 3.3. Running an Image Interactively

To run a shell interactively we specify to flags to Docker.

* `--tty` or `-t` which attaches your terminal to the container allowing you
    to see its output.
* `--interactive` or `-i` which allows you to interact with the the container.

Notice both have a long form that starts with two dashes, and a short form
that starts with a single dash. Short forms can be combined into a single
option using a single dash. For example, instead of specifying `-t` and `-i`
we can combine them into one `-it`.

**TL;DR - Use `-it` to run one-off commands interactively.**

```bash
docker run -it adoptopenjdk:11-jdk
```

You are now in a Java shell, jshell. Try defining a variable or two and some
expressions.

    int x = 3;
    String s = "hi";
    s + x;

Exit jshell by typing `/exit`

You were just interacting with jshell that was running inside of a container
created from the adoptopenjdk:11-jdk image.

### 3.4. Running Multiple Containers

A container is an instance of an image, and you can have multiple,
independent containers of the same image running at the same time.
Let's try.

Start a jshell session running in a container created from the
adoptopenjdk:11-jdk image.

```bash
docker run -it adoptopenjdk:11-jdk
```

Open another terminal and start another container created from the
same image.

```bash
docker run -it adoptopenjdk:11-jdk
```

Open yet another terminal, and use the following command to list the
containers running in your local Docker environment.

```bash
docker ps
```

> **Info**
>
> `ps` is a common Linux command that lists a system's running processes.
> Similarly `docker ps` lists the containers currently running in Docker.

Switch between the terminals and interact with the jshells to confirm that
they are independent.

Exit one of the jshells and list the runnning Docker processes. Is it gone?

Stop the other one and confirm that it is no longer running.

### 3.5. Stopping Containers

So far you have stopped your interactive container by telling jshell you're
done, which then means jshell stops running, and then the container halts.

But what happens if a container stops responding but is still running? Or
what if you aren't in an interactive shell and you need to stop the container?
Let's learn how to stop our containers from outside the container.

Start a jshell but without the `--interactive` option.

```bash
docker run -t adoptopenjdk:11-jdk
```

Try interacting with the shell, and notice that jshell does not respond.

Open another terminal and list the contianers. Note the ID or name of the
container. Now stop it as follows,

```bash
docker stop ID
```

Switch to the other terminal and confirm that the container has stopped.

### 3.6. Removing Containers

Use the following command to list all containers, running or not, in the
local Docker environment.

```bash
docker ps -a
```

How many containers do you have? Why so many? Why do containers linger around
after they stop running?

Remove one or two containers (but not all) by running the following command
for each container, replacing ID with the ID (or name) of the container.

```bash
docker rm ID
```

List all the containers again and confirm that they are gone.

That's a bit tedious if you have a lot of containers to remove out.
Here is another way that works in a linux like shell.

```bash
docker rm $(docker ps -aq)
```

> **NOTE**
>
> Command substitution `$(...)` works on Linux-like shells. It *might* work
> on PowerShell. I'm not sure. It probably won't work under Command Prompt.
> If the above doesn't work, you'll have to use the long form shown before.


This works as follows. First the command inside of `$(...)` runs.
`docker ps -aq` lists all the containers, but the `-q` (quiet) causes it to
only list their IDs. Then these IDs are given to the outer command
`docker rm`, which removes all the containers with the given IDs.

You can use this same trick to stop all containers.

```bash
docker stop $(docker ps -q)
```

So a one-line way to stop and remove all containers is

```bash
docker stop $(docker ps -q) ; docker rm $(docker ps -aq)
```

### 3.7. Automatically Removing Containers

When we use Docker to run a one-off command, we often don't want the
container to stick around after the command is done. And removing it
manually is tedious.

When we use `docker run` to run an image, we can pass it `--rm`
(note their are two dashes because this is a single long form option)
which tells docker We can ask Docker to remove the container after it
stops running.

```bash
docker run --rm adoptopenjdk:11-jdk
```

We didn't run this interactively, so this should just flash jshell and exit.
List all the containers and confirm that no containers were left behind.

Combining this with what we had before, we can run adoptopenjdk:11-jdk
interactively and have Docker clean up the container after it stops
as follows:

```bash
docker run -it --rm adoptopenjdk:11-jdk
```

Now exit jshell with `/exit`, and confirm that no container lingers.

### 3.8. Running a Different Command in a Container.

Our original goal was not to interact with jshell, but was to compile and
run our Java programs.

Images contain a default command to run when a container is started. For
adoptopenjdk:11-jdk, the default command is jshell. Images can be configured
to allow use to specify a different command to run with the container starts.
We do this by appending the command (and its arguments) to the end of our
`docker run` command. Let's run `javac --version` in a container and confirm
we have the right version of the compiler.

```bash
docker run -it --rm adoptopenjdk:11-jdk javac --version
```

The image also contains the `java` command which we'll use to run our
compiled Java programs. Let's confirm we can run it too by checking
its version.

```bash
docker run -it --rm adoptopenjdk:11-jdk java --version
```

### 3.9. Bind Mounting Files and Directories into a Container

OK, let's compile our `Hi.java` program again. ***First***,
delete `Hi.class` if it's still around. That way we'll know if we're
successful or not.

***Now***, let's compile `Hi.java`.

```bash
docker run -it --rm adoptopenjdk:11-jdk javac Hi.java
```

Ack, an error?! The first line of that error reads something
like `error: file not found: Hi.java`.

The problem is that containers are designed to be isolated from other
containers and the host machine. That prevents them from interfering
with each other. Part of this isolation is having independent
filesystems. So host OS's filesystem is not available to containers
by default.

This is a good thing. It means that a strange container
that you are trying for the first time can't delete all the files
on your system!

So if we want to poke a hole in this isolation, we have to do so
explicitly. In this case, we want to give the container access to
the file we are compiling, Hi.java. More, we know that javac is
going to generate Hi.class and place it in our current working
directory, so it really needs access to our current working directory.

We can tell `docker run` to bind mount a host directory into a container
using the `--volume` or `-v` option. Along with this option we need to
specify which folder we want to mount from our host filesystem, and the
path inside the container where we want to access it, and you separate
them with a `:`. So, `-v HOST:CONAINTER` where HOST is a path on the host
machine, and CONTAINER is a path inside the container.

We want to mount the current working directory.

* In PowerShell and Linux-like
    shells, we can use the `PWD` environment variable. To access the value in
    and environment variables, we place it inside `${}`: so `${PWD}`. Since
    path names may have spaces in them, we will always access this variable
    inside double quotes to prevent the command-line from splitting the path
    based on spaces.
* In Command Prompt, we can use %cd% to get the current working directoy.


Now, where are we going to mount `${PWD}` inside the container. We will
make up a directory `/w`. That is a directory named `w` off the root of
the filesystem. This will make it easy for us to access.

OK... let's give it a try...

```bash
docker run -it --rm -v "${PWD}:/w" adoptopenjdk:11-jdk javac Hi.java
```

Ack, same error!

The problem now is that our current working directory inside the container
may not be '/w' (where our files are). Let's see where our current working
directory is inside the container.

```bash
docker run -it --rm -v "${PWD}:/w" adoptopenjdk:11-jdk pwd
```

I got `/`. Did you? That's the root directory. But we want to be in `/w`.
Guess what, Docker gives us an option for that too. By passing `--workdir`
or `-w` for short, we can specify which directory should be the working
directory when Docker starts the container. So let's add `-w /w` to our
command and give it another try.

```bash
docker run -it --rm -v "${PWD}:/w" -w /w adoptopenjdk:11-jdk javac Hi.java
```

Success? Check, do you have a Hi.class? Wuhoo!

Now run your program. Remember, in Java we don't specify the `.class` of
the class we want to run.

```bash
docker run -it --rm -v "${PWD}:/w" -w /w adoptopenjdk:11-jdk java Hi
```

Did you see "Hi"? Wuhoo!

Delete Hi.class, and try compiling and running Hi.java using Java 15.

Check, do you have any stopped containers hanging around?

### 3.10. Removing Images

List the images in your local Docker environment.

```bash
docker images
```

Notice how much space they are consuming. If you don't think you'll need
those images anytime soon, you could reasonably remove them.
Remove one of them using the following command.


```bash
docker rmi ID
```

If you want to remove all your images and you have a lot of them that will
get tedious. You can use a similar trick as you did with containers.

```bash
docker rmi $(docker images -q)
```

### 3.11. Removing Other Resources

Besides images and containers, Docker also manages a set of networks
for communication and volumes for storage. If you want to clean out
all resources not used by a running container, use the following.

```bash
docker system prune --all --volumes
```
