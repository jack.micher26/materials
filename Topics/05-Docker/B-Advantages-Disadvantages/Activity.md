# Docker - Advantages and Disadvantages

## Roles

* Manager:
* Recorder:
* Skeptic:
* Efficiency Expert:

## Objectives

* Identify the advantages and disadvantages of Docker

## Model 1 - Overview

**Method A:**
```plantuml
    rectangle "Server 2" {
        rectangle "App2" as app2a
        rectangle "Deps2" as deps2a
        rectangle "OS2" as os2a
        rectangle "HW2" as hw2a
        app2a --> deps2a
        deps2a --> os2a
        os2a --> hw2a
    }
    rectangle "Server 1" {
        rectangle "App1" as app1a
        rectangle "Deps1" as deps1a
        rectangle "OS1" as os1a
        rectangle "HW1" as hw1a
        app1a --> deps1a
        deps1a --> os1a
        os1a --> hw1a
    }
```

**Method B**
```plantuml
    rectangle "Server" as serverb  {
        rectangle "App1, App2, ..." as app2b
        rectangle "Deps2, Deps2, ..." as deps2b
        rectangle "OS" as osb
        rectangle "HW" as hwb
        app2b --> deps2b
        deps2b --> osb
        osb --> hwb
    }
```

**Method C**
```plantuml
    rectangle  "Server" as serverc {
        rectangle "VM2" {
            rectangle "App2" as app2c
            rectangle "Deps2" as deps2c
            rectangle "OS2" as os2c
        }
        rectangle "VM1" {
            rectangle "App1" as app1c
            rectangle "Deps1" as deps1c
            rectangle "OS1" as os1c
        }
        rectangle "Hypervisor" as hyperv
        rectangle "HW" as hwc
        app2c --> deps2c
        deps2c --> os2c
        app1c --> deps1c
        deps1c --> os1c
        VM1 ----> hyperv
        VM2 ----> hyperv
        hyperv --> hwc
    }
```

Method D
```plantuml
    rectangle "Server" as serverd {
        rectangle "Container2" {
            rectangle "App2" as app2d
            rectangle "Deps2" as deps2d
        }
        rectangle "Container1" {
            rectangle "App1" as app1d
            rectangle "Deps1" as deps1d
        }
        rectangle "Docker" as dockerd
        rectangle "OS" as osd
        rectangle "HW" as hwd
        app2d --> deps2d
        Container1 ---> osd
        app1d --> deps1d
        Container2 ---> osd
        dockerd <-> osd
        osd --> hwd
    }
```

- App = Application
- Deps = Application dependencies (e.g., Python, Java, 3rd party libraries, environment variables, files, etc.)
- OS = Operating System
- HW = Hardware Architecture (chipset, etc.)
- Server = Physical Machine
- VM = Virtual Machine

1. Model 1 depicts four methods of deployment of applications. Write a title and a description for each method.

    * Method A (this one is completed as an example)
      * Title: App-per-machine
      * Description: Each app runs on a different machine with possibly different dependencies, operating systems, and hardware.
    * Method B
      * Title:
      * Description:
    * Method C
      * Title:
      * Description:
    * Method D
      * Title:
      * Description:


2. For each characteristic below, rank each method from *most* to *least*. Be prepared to share your reasoning.

    1. Efficiently uses hardware and operating system resources.
    (This one is completed as an example.)


        - B: makes best use of HW and OS resources as apps and dependencies can share.
        - D: shares hardware and software, but there may be a bit more overhead with docker.
        - C: shares the hardware. But each app has its own OS. That's going to take up a lot memory, which is a waste if apps use the same kind of OS.
        - A: If apps are small, then HW and OS will be underutilized.


    2. Is difficult to install/deploy due to incompatibilities between requirements of applications and their dependencies. (reorder as needed)

        - A:
        - B:
        - C:
        - D:

    3. Is fast to deploy an application and all its dependencies. Include the operating system and hardware as a dependency if it is not shared with other applications.

        - A:
        - B:
        - C:
        - D:

    3. Requires applications and their dependencies to be built for the same operating system?

        - A:
        - B:
        - C:
        - D:

    4. Demands a lot of hardware resources from each machine.

        - A:
        - B:
        - C:
        - D:

    5. Allows applications and their dependencies to interfere with each other.

        - A:
        - B:
        - C:
        - D:



---

&copy; 2021 Stoney Jackson <dr.stoney@gmail.com> and Karl Wurst

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
