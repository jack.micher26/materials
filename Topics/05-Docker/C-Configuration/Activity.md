# Docker Desktop Configuration

## 1. Roles

* Manager:
* Reader:
* Recorder:


## 2. General Instructions

* Have the Reader read each step below.
* Have everyone follow the step.
* Try to help those who have a technical challenge.
* Have the Recorder record any questions and insights in the space below.

## 3. Recorder notes

(Note your team's questions and insights.)

## 4. Instructions

1. Start Docker Desktop if it is not already running. The Docker icon should be in a status bar (a.k.a. system tray), and the containers on the whale's back should be static (not changing).
   If it is animated, Docker Desktop is still starting up.

   ![Docker Icon](Images/DockerIcon.png)

2. Open the dashboard for Docker Desktop.

   ![Open Dashboard](Images/OpenDashboard.png)

3. Open Docker Desktop's settings.

   ![Open Settings](Images/OpenSettings.png)

4. Consider disabling starting Docker when you log in.

    ![General](Images/General.png)

    > **Tip**
    >
    > TL;DR - You may not want Docker to start when you log in.
    >
    > Docker containers are typically designed to run in Linux. Although there are
    > Windows-based containers, since Docker was original designed for Linux, most
    > containers run on Linux.
    >
    > So how does Docker run a Linux-based container on a non-Linux operating
    > system? It runs a Linux virtual machine, and deploys containers into this
    > virtual machine. That's why when you installed Docker Desktop on Windows, it
    > required WSL (Windows Support for Linux) to be installed, and it needed
    > virtualization support to be enabled in your BIOS and in Windows.
    >
    > Virtual machines need a sizable amount of memory and disk space to be allocated
    > to them. Running Docker Desktop may take a fair amount of time to start,
    > and will consume a fair amount of system resources while it is running. So,
    > you may not want Docker Desktop running all the time, and you may not want it
    > to start when you log in.
    >
    > For more information about how Docker works, checkout this 15m read:
    > Anatomy of Docker by Uday Hiwarale, 2018 [https://itnext.io/getting-started-with-docker-1-b4dc83e64389](https://itnext.io/getting-started-with-docker-1-b4dc83e64389)

5. View resources and adjust them as needed. Generally Docker needs 2GB of RAM. If you find Docker is not behaving properly, it may be that you've given it too little resources. If you find the rest of your system is painfully slow, it may be that you have given Docker too many resources.

    ![Resources](Images/Resources.png)


6. View file sharing. You may need to return here if Docker complains when you
   try to mount a local directory into a container that isn't shared with the
   virtual machine.

    ![File Sharing](Images/FileSharing.png)

7. When you start building systems that are composed of multiple containers,
   you may start looking into orchestrators. Docker Compose and Docker Swarm
   come with Docker Desktop, and are immediately available for use. But notice
   that Kubernetes is also available, but must be enabled. Kubernetes is a
   popular orchestrator that has much of the market share (at the time of
   writing). When you begin to explore orchestrators, and if you want to explore
   Kubernetes, return here and enable it.

    ![Kubernetes](Images/Kubernetes.png)

8. Make sure everyone on your team knows how to stop docker.
