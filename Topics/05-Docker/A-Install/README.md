# Installing Docker

For this section of the course, you will need access to a computer that has Docker installed.

Docker requires a fair amount of memory and hardware virtualization support. 

At the end of summer 2020, I created a set of install instructions for macOS and Windows 10 Home. At that time Docker just began supporting Windows 10 Home, and the official install instructions for it were scattered and hard to follow. However, a lot has changed since then.

I recommend ***first*** trying to follow the official install instructions, found here:

* Official instructions: https://docs.docker.com/get-docker/

If you get stuck or have problems with those, consult with me and/or the instructions I created in 2020. They are here:

* [MacOS](./MacOS.md)
* [Windows-10-Home](./Windows-10-Home.md)
