## Required Materials

### Non-Free

- [ACM Student Membership ~$19](https://www.acm.org/membership/membership-options).
  This gives you access to O'Reilly Online, which contains numerous texts and videos
  that I will be selecting from in this class.

- A Laptop with a supported version of Windows, MacOS, or Linux (but not
  Chromebook). You do have access to computer labs that should have the
  software we need in this class. But a Laptop would likely make your life
  easier.

- A headset for participating in voice conferences without disturbing those around you.

### Free

- Discord
- Git for Windows
- GitLab account
- Docker
- VSCodium (or VSCode)
