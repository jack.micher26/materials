## Communication

We use a number of different tools for communication. To help navigate these,
this document provides a brief overview of the tools we'll use and for what purpose.

### Kodiak

* Announcements
* Assignments
  * Officially assign homework
  * Submit some homework. (Some will be submitted to GitLab instead.)
* Quizzes
  * Exams
  * Reflections
* Grades
  * Grades
  * Feedback
* Content


### Google Doc

* Meeting agenda/minutes
  * Announcements
  * Questions from Checkin Quizzes
  * Session details
  * Short cut links to resources.


### GitLab

* Syllabus
* Schedule
* Policies
* Activities
* Homework source
* Homework submission


### Discord

* Synchronous class sessions
* Office hours
* Transient, synchronous communication


### Ether Pad

* Team Activities and Notes


### Email

* Asynchronous private communication
