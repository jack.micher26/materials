## Tentative Schedule

An item is due at 9am on the day that is indicated in the table below (unless otherwise indicated).

Week | Date | Day | Activity | Reflection | Homework
:--: | :--: | :--: | :-----: | :--------: | :------:
1  | 1/25 | M | 00.A-Teams <br> 00.B-Discord | |
1  | 1/26 | T | 00.C-Syllabus <br> 00.D-VSCodium | |
1  | 1/27 | W | 00.E-Markdown <br> 01-Licensing | 01-Licensing (1pm) |
1  | 1/28 | R | 01-Licensing <br> 02.A-AgileVsWaterfall (model 1) | |
   |      |   | | |
2  | 2/1  | M | SNOW | 02.A-AgileVsWaterfall <br> 02.B-Agile <br> 02.B-Scrum | |
2  | 2/2  | T | SNOW | |
2  | 2/3  | W | 02.A-AgileVsWaterfall (models 2-3) <br> 02.B-Agile | |
2  | 2/4  | R | 02.C-Scrum (models 1-4) | |
2  | 2/5  | F | (no class) | | 01-Licensing
   |      |   | | |
3  | 2/8  | M | 02.C-Scrum (models 5-6) | 03.A-Paths |
3  | 2/9  | T | 03.A-Paths <br> 03.B-LinuxCommands (models 1-2) | |
3  | 2/10 | W | 03.B-LinuxCommands (models 3-5) | |
3  | 2/11 | R | 03.C-GitConfig <br> 03.D-GitInit | |
3  | 2/12 | F | (no class) | | 02-Scrum
   |      |   | | |
4  | 2/15 | M | 03.E-Changes | |
4  | 2/16 | T | 03.E-Changes (Practice) <br> 03.F-Undo (Model 1) | |
4  | 2/17 | W | NO CLASS | |
4  | 2/18 | R | 03.F-Undo | |
4  | 2/19 | F | (no class) | |
   |      |   | | |
5  | 2/22 | M | 03.G-CommitMessages | |
5  | 2/23 | T | 03.H-GitignoreGitattributes | |
5  | 2/24 | W | NO CLASS | |
5  | 2/25 | R | Exam 1 - Review | |
5  | 2/26 | F | (no class) | | 03-Git
   |      |   | | |
6  | 3/1 | M | Exam 1 | |
6  | 3/2 | T | | |
6  | 3/3 | W | NO CLASS | |
6  | 3/4 | R | | |
6  | 3/5 | F | (no class) | |


---

## Last Time

Avoid working ahead as topics, activities, assignments, and deadlines may change.

Week | Date | Day | Activity | Reflection | Homework
:--: | :--: | :--: | :--: | :--: | :--:
2 | 2/1 | M | Scrum, File System Navigation and Git Setup | Agile vs Waterfall | |
2 | 2/2 | T | Saving with Git | |
2 | 2/3 | W | Undoing with Git | |
2 | 2/4 | R | Create a project on GitLab, cloning with Git, and pushing with Git
2 | 2/5 | F | (no class) | | 01-CopyrightAndLicensing
  |       |   |
3 | 2/8 | M | *Launch first homework* and help debug git/gitlab connection issue
3 | 2/9 | T | Review Git and GitLab so far
3 | 2/10 | W | .gitignore and .gitattributes
3 | 2/11 | R | Commit Messages
  |       |   |
4 | 2/15 | M | Branching, Merging, Conflicts (BMC)
4 | 2/16 | T | **Due HW1: Git Warmup, Licensing** <br> BMC
4 | 2/17 | W | NO CLASS
4 | 2/18 | R | BMC
4 | 2/19 | F | **Due HW2: Git, Licensing, Agile/Scrum** (no class on Friday)
  |       |   |
5 | 2/22 | M | **Exam 1: Everything before Branching**
5 | 2/23 | T | BMC
5 | 2/24 | W | Remotes (Clone, Fetch, Pull, and Push)
5 | 2/25 | R | Remotes Practice
  |       |   |
6 | 3/1 | M | Personal Workflow
6 | 3/2 | T | Trusted-Team Workflow
6 | 3/3 | W | Trusted-Team Workflow (Moby activity)
6 | 3/4 | R | Untrusted-Contributor-Workflow
  |       |   |
7 | 3/8 | M | Docker
7 | 3/9 | T | Docker
7 | 3/10 | W | Docker
7 | 3/11 | R | Docker
  |       |   |
8 | 3/15 | M | Testing (demo: binary addition; launch HW3)
8 | 3/16 | T | Testing (JUnit Experiments)
8 | 3/17 | W | Testing (Testing Terms)
8 | 3/18 | R | Testing (Go Fast Forever; Developing Tests)
  |       |   |
9 | 3/22 | M | Testing (Ch1 Unit Testing; Developing Tests cont'd)
9 | 3/23 | T | Testing (Ch2 Unit Testing; Developing Tests cont'd; Launch HW4)
9 | 3/24 | W | Testing (Ch3 Unit Testing; **Due HW3: Contribution Workflow**)
9 | 3/25 | R | Testing (Ch4-5 Unit Testing)
  |       |   |
10 | 3/29 | M | Testing (Ch6 Unit Testing), Homework 4 Q\&A
10 | 3/30 | T | Testing (Ch7 Unit Testing)
10 | 3/31 | W | Testing (Ch8 Unit Testing)
10 | 4/1 | R | NO CLASS
   |       |   |
11 | 4/5 | M | Exam review **Due HW4: Testing**
11 | 4/6 | T | Ch1 Clean Code
11 | 4/7 | W | Ch2 Clean Code
11 | 4/8 | R | Ch3 Clean Code
   |       |   |
12 | 4/12 | M | **Exam**
12 | 4/13 | T | Ch2&3 clean up
12 | 4/14 | W | Ch4 Clean Code
12 | 4/15 | R | Ch4 Clean Code
   |       |   |
13 | 4/19 | M | TDD through Ch 3
13 | 4/20 | T | TDD through Ch 9
13 | 4/21 | W | TDD
13 | 4/22 | R | TDD
   |       |   |
14 | 4/26 | M | TDD
14 | 4/27 | T | TDD
14 | 4/28 | W | TDD
14 | 4/29 | R | Review
   |       |   |
15 | 5/3 | M | FINALS
15 | 5/4 | T | FINALS
15 | 5/5 | W | FINALS
15 | 5/6 | R | FINALS
