## COVID-19

When in the computer classroom, please observe the following policies:

* Masks are required and must cover your mouth and nose.
* Consumption of food and drink is prohibited.
* Maintain 6-foot physical distancing.
* Wipe down the computer and the space you will be using when you first enter the classroom.
