### Extenuating Circumstances

If a circumstance beyond your control causes you to violate the above policies,
you may request special considerations for your circumstance. Make a request as
follows:

*   Complete the Extenuating Circumstance "quiz" on Kodiak.
*   Submit hard copies of evidence to your instructor.

Appeals must be submitted as soon as possible once you are aware of the
extenuating circumstances. Your instructor will determine the acceptability of
an appeal, along with the specific accommodations that will be made.

_If you have any problems with the above instructions, please discuss your
situation with your instructor._
